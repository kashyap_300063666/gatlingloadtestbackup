# Usage Example

## Running as an independent jar

     $ ./gradlew clean build fatJar
     $ java -Dusers.count=1 -Dfilter.scenarios="active term" -jar inbound-performance-test-all.jar -s simulations.partners.terms.PartnerTermsSimulation -df . -rf .

## Running as gradle task

     $ ./gradlew clean build loadTest -Dusers.count=1000 -Dfilter.scenarios="active term" -Dload.test.class=simulations.partners.terms.PartnerTermsSimulation


#### Note: For more configuration properties, please refer **src/test/resources/gating.properties**
