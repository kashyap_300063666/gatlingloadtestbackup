package simulations.loyalty

import io.gatling.http.Predef.http
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.http.Predef.status
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class LoyaltySimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "cartpayments"
  /* Name of File to be downloaded */
  val fileName = "loyaltyLoginIDs.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.LOYALTY.toString)
  private val csvFeeder = csv(filePath).circular

  private val accountBalanceHeaders = Map("cache-control" -> "no-cache",
    "client" -> "pps",
    "version" -> "1.0")

  val loyaltyBaseUrl = System.getProperty("baseUrl", defaultUrl)
  val httpProtocol = http
    .baseUrl(loyaltyBaseUrl)
    .headers(accountBalanceHeaders)

  private val balance =
    scenario("loyalty balance")
        .feed(csvFeeder)
        .exec(http("Get user's loyalty balance")
        .get("/lp/loyalty/loyaltyPoints/v2/accountBalance")
        .headers(accountBalanceHeaders)
        .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
        .check(status.is(200))
        .check(jsonPath("$.login").is("${uidx}"))
      )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}
