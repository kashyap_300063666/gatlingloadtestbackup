package simulations.insider.styleplus

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class UserSimulation extends BaseSimulation {

  private val styleplusBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.STYLE_PLUS_BACKEND.toString)).disableWarmUp

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "commonplatform"
  /* Name of File to be downloaded */
  val fileName = "uidxlist.txt"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/uidxlist.txt")
  val uidxFileLocation: String = System.getProperty("uidxFileLocation", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  val r = scala.util.Random
  val linesUidx = readFile(filePath)

  // Create a dummy user with userid LoadTestUser before starting the simulation
  // also assign loyalty points to that user
  val userId = Map("userid" -> "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")

  // 1) /user/exists
  private val userExists =
    scenario("user exists").
      exec(http("userExists").
        get("/api/styleplus/v1/user/exists").headers(headers).headers(userId).
        check(status.is(200)).check(jsonPath("$.uidx").is("4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")))

  // 2)  /user/user-tier-detail
  private val userTierDetails =
    scenario("user tier details")
        .exec(http("userTierDetails").get("/api/styleplus/v1/user/user-tier-detail").headers(headers).
          headers(userId).check(status.is(200)))

  // 3)  /user/user-tier-progress-percent
  private val userTierPercentage =
    scenario("user tier percentage").
      exec(http("userTierPercentage").get("/api/styleplus/v1/user/user-tier-progress-percent").headers(headers).
        headers(userId).check(status.is(200)))

  // 4) /user/exists
  private val userExistsByFile =
    scenario("user-exists-by-file").
      exec(http("userExistsByFile").
        get("/api/styleplus/v1/user/exists").headers(headers).headers(Map("userid" -> linesUidx(r.nextInt(100000)))).
        check(status.is(200)))

  // 5)  /user/user-tier-detail
  private val userTierDetailsByFile =
    scenario("user-tier-details-by-file")
      .exec(http("userTierDetailsByFile").get("/api/styleplus/v1/user/user-tier-detail").headers(headers).
        headers(Map("userid" -> linesUidx(r.nextInt(100000)))).check(status.is(200)))

  // 6)  /user/user-tier-progress-percent
  private val userTierProgressPercentByFile =
    scenario("user-tier-progress-percent-by-file")
      .exec(http("userTierProgressPercentByFile").get("/api/styleplus/v1/user/user-tier-progress-percent").headers(headers).
        headers(Map("userid" -> linesUidx(r.nextInt(100000)))).check(status.is(200)))

  // 7)  /user/user-tier-progress
  private val userTierProgress =
    scenario("user-tier-progress")
      .exec(http("userTierProgress").get("/api/styleplus/v1/user/user-tier-progress").headers(headers).
        headers(userId).check(status.is(200)))

  // 8)  /user/user-tier-progress-by-file
  private val userTierProgressByFile =
    scenario("user-tier-progress-by-file")
      .exec(http("userTierProgressByFile").get("/api/styleplus/v1/user/user-tier-progress").headers(headers).
        headers(Map("userid" -> linesUidx(r.nextInt(100000)))).check(status.is(200)))

  // 9)  /user/user-detail
  private val userDetail =
    scenario("user-detail")
      .exec(http("userDetail").get("/api/styleplus/v1/user/user-detail").headers(headers).
        headers(userId).check(status.is(200)))

  // 10)  /user/user-detail
  private val userDetailByFile =
    scenario("user-detail-by-file")
      .exec(http("userDetailByFile").get("/api/styleplus/v1/user/user-detail").headers(headers).
        headers(Map("userid" -> linesUidx(r.nextInt(100000)))).check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(styleplusBaseUrl)
}