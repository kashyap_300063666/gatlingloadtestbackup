package simulations.insider.styleplus

import java.util.UUID.randomUUID

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class InsiderSimulation extends BaseSimulation{

  private val styleplusBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.STYLE_PLUS_BACKEND.toString)).disableWarmUp
  //private val uuid= randomUUID.toString;

  private val uuidFeeder = Iterator.continually(Map("uuid" -> randomUUID().toString))

  private val creditPoints =
    scenario("creditPoints")
      .feed(uuidFeeder)
      .exec(http("creditPoints")
          .post("/api/styleplus/v1/offers/v2/credit-points")
        .headers(headers)
        .body(StringBody(
          """{
    "uidx":"a4cac10e.8e41.4241.8bbb.20d81a7396e2fJZWmHVHlD",
    "source":"map",
    "points": 10,
    "message": "Credit points for testing",
    "expiryDate":"2019-06-06",
    "sourceKey": "${uuid}",
                    "sourceName":"test"
}""")).asJson
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(styleplusBaseUrl)
}
