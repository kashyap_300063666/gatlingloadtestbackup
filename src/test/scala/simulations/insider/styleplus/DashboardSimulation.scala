package simulations.insider.styleplus

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class DashboardSimulation extends BaseSimulation {

  private val styleplusBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.STYLE_PLUS_BACKEND.toString)).disableWarmUp

  // create dummy offers and activities against dummy user
  val userId = Map("userid" -> "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")

  // 1) /dashboard/offers
  private val dashboardoffers =
    scenario("dashboard offers")
      .exec(http("dashboardoffers").get("/api/styleplus/v1/dashboard/offers").headers(headers).
        headers(userId).check(status.is(200)))

  // 2) /dashboard/activities
  private val dashboardactivities =
    scenario("dashboard activities")
      .exec(http("dashboardactivities").get("/api/styleplus/v1/dashboard/activities").headers(headers).
        headers(userId).check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(styleplusBaseUrl)
}
