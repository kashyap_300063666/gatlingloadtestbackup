package simulations.insider.styleplus

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class OrderLineSimulation extends BaseSimulation {
  private val styleplusBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.STYLE_PLUS_BACKEND.toString)).disableWarmUp

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "commonplatform"
  /* Name of File to be downloaded */
  val fileName = "uidxlist.txt"

  val uidxFileLocation: String = System.getProperty("uidxFileLocation", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/uidxlist.txt")
  val r = scala.util.Random
  val linesUidx = readFile(filePath)

  // update dummy orders in confirmed, delivered status for dummy userid with created time less than last 90 days
  // create orderLine entries with dummy itemIds
  val userId = Map("userid" -> "c0fe630e.0b9e.4d21.95d4.0c06bb3c25beiv9fLkGEhM")

  // 1) /orderline/fetch-order
  private val fetchOrder =
    scenario("fetch orders")
      .exec(http("fetchOrder").post("/api/styleplus/v1/orderline/fetch-order").headers(headers).
        headers(userId).check(status.is(200)))

  // 2) /orderline/fetch-order-points
  private val fetchOrderPoints =
    scenario("fetch order points")
      .exec(http("fetchOrderPoints").post("/api/styleplus/v1/orderline/fetch-order-points").
        headers(headers).headers(userId).
        body(StringBody(
          """{"itemEntries":[{"styleId":1000000021,"skuId":12121212,"amount":1000.56},{"styleId":1000000022,"skuId":12121213,"amount":1500},{"styleId":1000000023,"skuId":12121214,"amount":677.2}]}""".stripMargin)).asJson.
        check(status.is(200)))

  // 3) /orderline/points-by-line-item
  private val pointsByLineItem =
    scenario("points by line item")
      .exec(http("pointsByLineItem").post("/api/styleplus/v1/orderline/points-by-line-item").
        headers(headers).headers(userId).
        body(StringBody("""{"lineItemIds": [4402355209, 4402427721, 4402427729, 4402432915]}""")).asJson.check(status.is(200)))

  // 4) /orderline/points-by-amount
  private val pointsByAmount =
    scenario("points by amount")
      .exec(http("pointsByAmount").post("/api/styleplus/v1/orderline/points-by-amount").
        headers(headers).headers(userId).
          body(StringBody("""{"styleId":12345,"amount": 5000}""")).asJson.check(status.is(200)))

  // 5) /orderLine/fetch-order-points
  private val fetchOrderPointsByFile =
    scenario("fetch-order-points-by-file")
      .exec(http("fetchOrderPointsByFile").post("/api/styleplus/v1/orderline/fetch-order-points").
        headers(headers).headers(Map("userid" -> linesUidx(r.nextInt(100000)))).
        body(StringBody(
          """{"itemEntries":[{"styleId":1000000021,"skuId":12121212,"amount":1000.56},{"styleId":1000000022,"skuId":12121213,"amount":1500},{"styleId":1000000023,"skuId":12121214,"amount":677.2}]}""".stripMargin)).asJson.
        check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(styleplusBaseUrl)
}
