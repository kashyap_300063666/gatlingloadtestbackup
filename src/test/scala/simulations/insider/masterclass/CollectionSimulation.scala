package simulations.insider.masterclass

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef.scenario
import simulations.BaseSimulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class CollectionSimulation extends BaseSimulation {
  private val masterclassBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MASTERCLASS.toString)).disableWarmUp

  // Create a dummy user before starting the simulation
  val userId = Map("userid" -> "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")
  val stylistId = "5de74baab85c951c9628a1ff"

  // 1) /collection/get-all
  private val getAll =
    scenario("get all").
      exec(http("getAll").
        get("/api/masterclass/v1/collection/get-all").headers(userId).
        check(status.is(200)))

  // 2) /collection/list-by-stylist
  private val listByStylist =
    scenario("list by stylist").
      exec(http("listByStylist").
        get("/api/masterclass/v1/collection/list-by-stylist?stylistId=" + stylistId).headers(userId).
        check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(masterclassBaseURL)
}
