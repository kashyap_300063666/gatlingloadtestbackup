package simulations.rms
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import simulations.BaseSimulation

class RmsGetReturnsSimulation extends BaseSimulation {

	private val feederUtil = new FeederUtil()
	/* Name of the team Name */
	val containerName = "oms"
	/* Name of File to be downloaded */
	val fileName = "returnOrderIds.csv"


	val httpConf: HttpProtocolBuilder = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.RMS.toString)).disableWarmUp

	//val csvFile: String = System.getProperty("returnOrderIds", "test-data/returnOrderIds.csv")
	val csvFile: String = System.getProperty("returnOrderIds", fileName)
	val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

	val returnOrderIdsFeeder: BatchableFeederBuilder[String]#F = csv(filePath).circular


	val rms: ScenarioBuilder = scenario("rms")
  	.feed(returnOrderIdsFeeder)
		.exec(http("rms")
		.get("/myntra-rms-service/rms/owner/2297/buyer/2297/storeReturn/search")
		.header("authorization", "Basic YTph")
		.header("x-myntra-client-id", "scmloadtest")
		.headers(headers)
		.queryParam("q","orderId.in:${orderId}")
		.check(status.is(200))
	)


setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec).protocols(httpConf)
}