Style Service LT
<br>
All Input props - 
<br>
All properties are optional
1. pdp.users - Number of parallel users for getPdpData. Default=1
2. pdpBulk.users - Number of parallel users for getPdpDataBulk. Default=1
3. projected.users - Number of parallel users for getProjectedEntries. Default=1
4. pdpBulk.skipDiscount - skipDiscountCall in getPdpDataBulk. Default=false
5. projected.skipDiscount - skipDiscountCall in getPdpDataBulk. Default=false
6. singleServerURL - single server url. By default uses style.myntra.com for prod and style.stage.myntra.com for stage
7. environment - stage or prod
8. use.constant_users - true or false
9. users.count - Not used in Style LT
10. duration.seconds - time for the run (in seconds)
11. grpc.port - grpc port to be used (8080 on stage, 6044 for single server, 80 for LT on entire backend)

Commands to Run - 

Local - Hit Stage

```java -Denvironment=stage -Duse.constant_users=true -Dduration.seconds=5 -Dgrpc.port=8080  -Dpdp.users=0 -DpdpBulk.users=1 -Dprojected.users=2 -jar build/libs/myntra-gatling-load-tests-all.jar -s simulations.search.style.StyleAllSimulation -df . -rf .```

<br>
Single Server - Local hit Local

```java -Duse.constant_users=true -Dduration.seconds=5 -Dgrpc.port=6044 -Dpdp.users=0 -DpdpBulk.users=1 -Dprojected.users=2 -DsingleServerURL=localhost -jar build/libs/myntra-gatling-load-tests-all.jar -s simulations.search.style.StyleAllSimulation -df . -rf .```

<br>

<h3>How to Run LT on Prod</h3>
- Goto <a href> scaleit.myntra.com</a> and login
- Goto "LoadTest" tab
- Click on "Run Existing Gatling Load Test"
-  Parameters - (Update various user counts, singleServerIP, duration as reqd. More parameters from list above can be added)  
<br>For Single Server ==> `{
    "pdp.users": 1,
    "pdpBulk.users": 1,
    "projected.users": 1,
    "singleServerURL": "10.162.177.237",
    "use.constant_users": true,
    "duration.seconds": 30,
    "grpc.port": 6044
  }`
  <br>For Prod ==> `{
                     "pdp.users": 100,
                     "pdpBulk.users": 100,
                     "projected.users": 100,
                     "use.constant_users": true,
                     "duration.seconds": 120,
                     "grpc.port": 80,
                     "environment": "prod"
                   }`
- In "Simulation" field, paste `simulations.search.style.StyleAllSimulation`
- Add relevant Run name, description, etc. No other field needs to be updated. You can add any value in duration field, as duration is picked from params.

<br>
Individual APIs

Just set relevant user counts to 0