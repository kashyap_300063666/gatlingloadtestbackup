package simulations.mfp

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation


class MfpPerformanceTest extends BaseSimulation{
  val planningIdFeeder = Array(
    Map("key" -> 42023, "value" -> 164),
    Map("key" -> 42026, "value" -> 120),
    Map ("key" -> 208383, "value" -> 182),
    Map ("key" -> 359400, "value" -> 116),
    Map ("key" -> 320941, "value" -> 222),
  ).circular

  val dateFeeder = Array("2019-05-20", "2019-05-21", "2019-05-22", "2019-05-23", "2019-05-24", "2019-05-25", "2019-05-26", "2019-05-17", "2019-05-18", "2019-05-19").map(e => Map("toDate" -> e))
    .circular

  val nameFeeder = Array("gatlingLoadTesting_createScenario11A", "gatlingLoadTesting_createScenario12A", "gatlingLoadTesting_createScenario13", "gatlingLoadTesting_createScenario14", "gatlingLoadTesting_createScenario15", "gatlingLoadTesting_createScenario16", "gatlingLoadTesting_createScenario17", "gatlingLoadTesting_createScenario18", "gatlingLoadTesting_createScenario19", "gatlingLoadTesting_createScenario20" +
    "gatlingLoadTesting_createScenario21", "gatlingLoadTesting_createScenario22", "gatlingLoadTesting_createScenario23", "gatlingLoadTesting_createScenario24", "gatlingLoadTesting_createScenario25", "gatlingLoadTesting_createScenario26", "gatlingLoadTesting_createScenario27", "gatlingLoadTesting_createScenario28", "gatlingLoadTesting_createScenario29", "gatlingLoadTesting_createScenario30").map(e => Map("scenarioname" -> e))
    .circular

  val eventDateFeeder = Array("2019-06-10", "2019-06-11", "2019-06-12", "2019-06-13", "2019-06-14", "2019-06-15", "2019-06-16", "2019-06-17", "2019-06-18", "2019-06-19").map(e => Map("eventDate" -> e))
    .circular

  val scenarioIdForApprovalFeeder = Array("6332","138","139","145","151","157","163","169","175","181").map(e => Map("scenarioId" -> e))
    .circular

  val monthFeeder = Array(
    Map("start" -> 4, "end" -> 4),
    Map("start" -> 5, "end" -> 5),
    Map("start" -> 6, "end" -> 6),
    Map("start" -> 7, "end" -> 7),
    Map("start" -> 8, "end" -> 8),
    Map("start" -> 9, "end" -> 9),
    Map("start" -> 10, "end" -> 10),
    Map("start" -> 11, "end" -> 11),
    Map("start" -> 12, "end" -> 12),
    Map("start" -> 1, "end" -> 1),
    Map("start" -> 2, "end" -> 2),
    Map("start" -> 3, "end" -> 3),
  ).circular

  val editScenarioIdWithMonthFeeder = Array(
    Map("key" -> 6342, "value" -> 4),
  ).circular

  //private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MFP_LOCAL.toString)
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MFP.toString)
  private val mfpBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  private val createScenario = scenario("mfp createScenario")
    .feed(monthFeeder)
    .feed(nameFeeder)
    .feed(planningIdFeeder)
    .exec(http("createScenario")
      .post("/myntra-mfp-service/financialPlan/scenario/createScenario")
      .headers(headers)
      .body(StringBody("""{"scenarioType":"IN_SEASON",
                     "roleUp":"MONTH",
                     "financialYear":212,
                     "roleUpStartValue":${start},
                     "roleUpEndValue":${end},
                     "channel":2,
                     "businessType":4,
                     "scenarioName":${scenarioname},
                     "planningAxis":${value},
                     "userId":${key}}""") ).asJson
      .check(status.is(200)))

  private val editScenarioAggDelta = scenario("mfp editScenarioAggDelta")
    .feed(editScenarioIdWithMonthFeeder)
    .exec(http("editScenarioAggDelta")
      .post("/myntra-mfp-service/financialPlan/scenario/editScenario")
      .queryParam("page", 1)
      .queryParam("pageLimit", 5)
      .headers(headers)
      .body(StringBody("""{
           "roleUp": "MONTH",
           "scenarioId":${key},
           "planningAxisId": 164,
           "strategyType": "BreakBack",
           "grandTotalHold": false,
           "breakBacktype": "DELTA",
           "freshness": "none",
           "denomination": "CR",
           "aggregateMetricChanges": [
              {
                  "metricName":"gmv",
                  "metricOldValue":"0",
                  "metricNewValue":"1",
                  "roleUpValue":${value},
                  "freshnessNewValue": 0,
                  "freshnessOldValue": 0,
                  "freshnessNewEdited":false,
                  "freshnessOldEdited":false,
                  "eventType":"MONTH"
             }
           ],
           "localMetricChanges": [
           ],
           "aggregateMetricHolds": [
           ],
           "localMetricHolds": [
           ]
         }""")).asJson
      .check(status.is(200)))

  private val viewScenario = scenario("mfp viewScenario")
    .feed(editScenarioIdWithMonthFeeder)
    .exec(http("viewScenario")
      .post("/myntra-mfp-service/financialPlan/scenario/viewScenario")
      .headers(headers)
      .body(StringBody("""{
                        "page":1,
                        "pageLimit":20,
                        "roleUpValueStart":${value},
                        "roleUpValueEnd":${value},
                        "roleUp":"MONTH",
                        "scenarioId":${key},
                        "planningAxisId":164,
                        "commercialType":"none",
                        "freshness":"none",
                        "compare":"none",
                        "compareStartDate":"",
                        "compareEndDate":"",
                        "doSort":false,
                        "doSearch":false,
                        "denomination":"CR",
                        "metricsOrderBy":{
                        },
                        "dimentionSearch":{

                        },
                        "userId":42023
                     }""")).asJson
      .check(status.is(200)))


  private val approveScenario = scenario("mfp approveScenario")
    .feed(scenarioIdForApprovalFeeder)
    .exec(http("approveScenario")
      .post("/myntra-mfp-service/financialPlan/workflow/submit")
      .headers(headers)
      .body(StringBody("""{
                            "comment": "approved",
                            "scenarioId": ${scenarioId},
                            "status": "APPROVE_EVENT",
                            "userId": 42023
                          }""")).asJson
      .check(status.is(200)))

  val mfpScenarios = List(createScenario, editScenarioAggDelta, viewScenario) filter (filterScenariosByName)

  setUp(mfpScenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(mfpBaseUrl)

}
