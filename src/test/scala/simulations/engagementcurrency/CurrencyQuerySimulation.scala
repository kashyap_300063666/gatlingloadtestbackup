package simulations.engagementcurrency

import java.util.UUID.randomUUID

import com.github.phisgr.gatling.grpc.Predef.grpc
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.engagement.currency.grpc.common.PageRequest
import com.myntra.engagement.currency.grpc.engagementQuery.{CurrencyQueryServiceGrpc, GetAccountHistoryRequest, GetAccountSummaryRequest}
import io.gatling.core.Predef.{scenario, _}
import io.gatling.core.session.Expression
import io.grpc.ManagedChannelBuilder
import simulations.BaseSimulation

class CurrencyQuerySimulation extends BaseSimulation{
  private val url = System.getProperty("url", BaseUrlConstructor.getBaseUrl(ServiceType.ENGAGEMENTCURRENCY.toString, ""))
  private val uidx = System.getProperty("uidx", "Hello")
  private val pageNum = System.getProperty("page", "1").toInt
  val grpcConf = grpc(ManagedChannelBuilder.forAddress(url, grpcPortNum).usePlaintext())

  println("Uidx is " + uidx)

  val accountSummaryRequest: Expression[GetAccountSummaryRequest] = GetAccountSummaryRequest(uidx=uidx, tenantId = 3)
  val accountHistoryReqeust: Expression[GetAccountHistoryRequest] = GetAccountHistoryRequest(uidx = uidx, tenantId = 3, pageRequest = Some(PageRequest(pageNum = pageNum, pageSize = 10)))
  // Get Account summary
  val getAccountSummary = scenario("Get_account_summary")
    .exec(grpc("GetAccountSummary")
      .rpc(CurrencyQueryServiceGrpc.METHOD_GET_ACCOUNT_SUMMARY).payload(accountSummaryRequest))


  // Get History
  val getAccountHistory = scenario("Get_account_history")
    .exec(grpc("GetAccountHistory")
      .rpc(CurrencyQueryServiceGrpc.METHOD_GET_ACCOUNT_HISTORY).payload(accountHistoryReqeust))


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)
}
