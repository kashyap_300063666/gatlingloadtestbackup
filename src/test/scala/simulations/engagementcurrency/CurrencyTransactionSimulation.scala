package simulations.engagementcurrency

import java.util.UUID.randomUUID

import com.github.phisgr.gatling.grpc.Predef.{grpc, _}
import com.github.phisgr.gatling.pb._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.engagement.currency.grpc.engagementTransaction.{CreditRequest, CurrencyTransactionServicesGrpc, DebitRequest}
import io.gatling.core.Predef.{scenario, _}
import io.gatling.core.session.Expression
import io.grpc.ManagedChannelBuilder
import simulations.BaseSimulation

class CurrencyTransactionSimulation extends BaseSimulation {
  private val url = System.getProperty("url", BaseUrlConstructor.getBaseUrl(ServiceType.ENGAGEMENTCURRENCY.toString, ""))
  println("Url is : " + url)
  val grpcConf = grpc(ManagedChannelBuilder.forAddress(url, grpcPortNum).usePlaintext())
  private val uidx = System.getProperty("uidx", "Hello")
  private val tenant = System.getProperty("tenant", "2").toInt
  private val source = System.getProperty("source", "4").toInt
  private val uuidFeeder = Iterator.continually(Map("uuid" -> randomUUID().toString))

  val dynamicCreditPayload: Expression[CreditRequest] = CreditRequest(uidx = uidx,
    tenantId = tenant,
    source = source,
    points = 50,
    ttlInDays = 10,
    metadataInJson = "{\"source\":\"abcd\", \"title\":\"title1\", \"rewardExpiry\":0}")
    .updateExpr(
      _.idempotenceId :~ $("uuid")
    )

  val dynamicDebitPayload: Expression[DebitRequest] = DebitRequest(uidx = uidx,
    tenantId = tenant,
    source = source,
    points = 2,
    metadataInJson = "{\"source\":\"abcd\", \"title\":\"title1\", \"rewardExpiry\":0}",
    description = "Describe")
    .updateExpr(
      _.idempotenceId :~ $("uuid")
    )

  // Get Account summary
  val credit = scenario("Credit")
    .feed(uuidFeeder)
    .exec(grpc("Credit")
      .rpc(CurrencyTransactionServicesGrpc.METHOD_CREDIT)
      .payload(dynamicCreditPayload)
    )


  // Get History
  val debit = scenario("Debit")
    .feed(uuidFeeder)
    .exec(grpc("Debit")
      .rpc(CurrencyTransactionServicesGrpc.METHOD_DEBIT).payload(dynamicDebitPayload))


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)
}
