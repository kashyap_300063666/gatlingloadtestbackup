package simulations.payments

import com.myntra.commons.{FormGenTokenAndLoginUser, ServiceType}
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

class   LoginPage {

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.PPSSERVICE.toString)
  private val paymentinstrumentDefaltUrl = BaseUrlConstructor.getBaseUrl(ServiceType.PAYMENTINSTRUMENTSERVICE.toString)
  protected val headers = Map("Authorization" -> "Basic U3JlZW5pdmFzdWx1fnNyZWVuaX46YWJjZA==", "accept" -> "application/json", "Content-Type" -> "application/json")
  val ppsBaseUrl = System.getProperty("ppsSingleServerUrl", defaultUrl)
  val paymentinstrumentBaseUrl = System.getProperty("paymentinstrumentUrl", paymentinstrumentDefaltUrl)
  val count : Int = 1599999

  val sessionSetUp: ChainBuilder =
    exec(session => {
      session.set("emailId", (Random.nextInt(count)+1).toString + "@myntra360.com").set("ppsBaseUrl", ppsBaseUrl).set("paymentinstrumentBaseUrl",paymentinstrumentBaseUrl)
    })
      .exec(http("knuthToken")
        .get("/auth/v1/token")
        .header("clientid", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
        .header("x-myntra-app", "appFamily=MyntraRetailAndroid")
        .header("origin", "myntra.com")
        .header("User-Agent", "MyntraRetailAndroid/4.2004.0 (Phone, 440dpi)")
        .check(status.is(200))
        .check(header("At").saveAs("at"))
      )
      .exec(http("login using gen token ")
        .post("/auth/v1/login")
        .header("at", session => session("at").as[String])
        .header("x-myntra-app", "appFamily=MyntraRetailAndroid")
        .header("origin", "myntra.com")
        .header("User-Agent", "MyntraRetailAndroid/4.2004.0 (Phone, 440dpi)")
        .body(StringBody("""{"userId":"${emailId}","accessKey":"${emailId}"}""")).asJson
        .check(status.is(200))
        .check(header("Rt").saveAs("rt"))
        .check(header("At").saveAs("at"))
        .check(header("Xid").saveAs("xid"))
        .check(header("Sxid").saveAs("sxid"))
        .check(jsonPath("$.uidx").saveAs("uidx"))
      )


//      exec( sessionVar =>
//      /* Set session data from redis to local session (which will be used inside scenario) */
//      sessionVar.set("at", convertedObject.get("at").getAsString)
//        .set("rt", convertedObject.get("rt").getAsString)
//        .set("sxid", convertedObject.get("sxid").getAsString)
//        .set("xid", convertedObject.get("xid").getAsString)
//        .set("uidx", convertedObject.get("uidx").getAsString)
//        .set("queryParam", "shirt")
//        .set("pageNos", 3)
//        .set("filterquery", "sort=low")
//    })

//  private val loginIdea =
//    scenario("Get User Token and login")
//      .exec(session => {
//        session.set("emailId", (Random.nextInt(count)+1).toString + "@myntra360.com")
//      })
//      .exec(http("knuthToken")
//        .get("/auth/v1/token")
//        .header("clientid", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
//        .header("x-myntra-app", "appFamily=MyntraRetailAndroid")
//        .check(status.is(200))
//        .check(header("At").saveAs("at"))
//        .check(header("Xid").saveAs("xid"))
//      )
//      .exec(http("login using gen token ")
//        .post("/auth/v1/login")
//        .header("at", "${at}")
//        .header("xid", "${xid}")
//        .header("x-myntra-app", "appFamily=MyntraRetailAndroid")
//        .body(StringBody("""{"userId":"${emailId}","accessKey":"${emailId}"}""")).asJson
//        .check(status.is(200))
//        .check(header("Rt").saveAs("rt"))
//        .check(header("Sxid").saveAs("sxid"))
//        .check(jsonPath("$.uidx").saveAs("uidx"))
//      )
//      .exec(sess => {
//        (sess.apply("emailId").as[String], FormGenTokenAndLoginUser.formJson(
//          sess.apply("at").as[String],
//          sess.apply("rt").as[String],
//          sess.apply("xid").as[String],
//          sess.apply("sxid").as[String],
//          sess.apply("uidx").as[String]))
//        sess
//      })

}
