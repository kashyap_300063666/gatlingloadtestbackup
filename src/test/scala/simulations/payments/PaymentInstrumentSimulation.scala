package simulations.payments


import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class PaymentInstrumentSimulation extends BaseSimulation {

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.PPSSERVICE.toString)

  val paymentsInstrumentHeader = Map(
    "cookie" -> "at=ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklqSWlmUS5leUpoY0hCT1lXMWxJam9pYlhsdWRISmhJaXdpYVhOeklqb2lTVVJGUVNJc0luUnZhMlZ1WDNSNWNHVWlPaUpoZENJc0luTjBiM0psU1dRaU9pSXlNamszSWl3aWJITnBaQ0k2SWpZM1ptRTFPV0V6TFRaalpXRXROR1poT0MxaFlqUTJMVFJqWWpOaE5tTXhOVE5pTXkweE5qQXdPRGM1TWpNek5EZzVJaXdpY0NJNklqSXlPVGNpTENKd2NITWlPalV3TUN3aVkybGtlQ0k2SW0xNWJuUnlZUzB3TW1RM1pHVmpOUzA0WVRBd0xUUmpOelF0T1dObU55MDVaRFl5WkdKbFlUVmxOakVpTENKemRXSmZkSGx3WlNJNk1Dd2ljMk52Y0dVaU9pSkNRVk5KUXlCUVQxSlVRVXdpTENKbGVIQWlPakUyTURBNE9ESTRNek1zSW01cFpIZ2lPaUkyTW1JMU5tWXdNUzFtWkdFeUxURXhaV0V0WVRReU5pMHdNREJrTTJGbU1qaGxOREVpTENKcFlYUWlPakUyTURBNE56a3lNek1zSW5WcFpIZ2lPaUp6Wm14dllXUjBaWE4wTFRGak56RmtPRGsxTG1Jd09Ea3VOR0kxTkM1aFltVmlMbUk0WlRJd1kyUTNabVV6TTJsWmRFWjFNMkZXWTNraWZRLmR1dlVYZFVmSlJsb0ViTlN0VVFadDNsZUtaNmxJQXVxMWxtbHJPQjh3VTJiWVp5WTNXdnNNVDlHdWx0NFZnN1doY0RLbzh2SE1KUVpEOFRReWhiYm53ZTNqVzhzVnlTRE5yczEyT0FHZWVMaS1lSkFqOXliZHQwZDktS0RPcDNHMmVsZXV0TUNOUFNLcm81YWdEeUt0VGpYYzZ0ZUZXT1NvNW9lc2IxcHpHcw==;",
    "Origin" -> "https://www.myntra.com",
    "user-agent" -> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36"
  )

  val ppsBaseUrl = System.getProperty("baseUrl", defaultUrl)
  val httpProtocol = http
    .baseUrl(ppsBaseUrl)
    .headers(paymentsInstrumentHeader)

  val PaymentInstument = scenario("PaymentsInstrument")
      .exec(http("PaymentsInstrument")
      .post("/myntra-payment-plan-service/v3/paymentInstruments")
        .header("x-myntraweb", "Yes")
        .header("x-meta-app", "channel=web")
        .header("client", "checkout")
        .header("content-type", "application/json")
        .header("xmetaapp", "deviceID=2466ef27-755a-4993-8670-07cafe7e9275")
        .headers(paymentsInstrumentHeader)
        .body(StringBody("""{"cartId": "98dc101c-dc63-41f8-977d-d35c0c683159", "email": "1500001@myntra360.com", "mobile":"9373181575"}""")).asJson
        .check(status.is(200))
        .check(bodyString.saveAs("paymentInstrumentResponse"))
        .check(jsonPath("$.csrfToken").saveAs("csrfToken"))
        .check(jsonPath("$.formSubmitUrl").saveAs("formSubmitUrl"))
    )
    .exec(session => {
      print("paymentInstrumentResponse is :: ")
      println(session("paymentInstrumentResponse").as[String])
      session
    })

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpProtocol)
}