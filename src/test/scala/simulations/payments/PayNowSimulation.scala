package simulations.payments

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, HLTUtility}
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random

class PayNowSimulation extends BaseSimulation {

//  protected val util = new Utils

  private val csBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)).disableWarmUp

//  util.downloadFile("test-data/styleids.csv")
//  val styleDataFeeder = csv("test-data/styleids.csv").random

  /* Steps to be executed before running load tests */
  val util = new HLTUtility()
  util.downloadFile("styleids.csv")
  private val csvData: List[String] = CSVUtils.readCSV2("styleids.csv", isIncludeHeader = false)
    .map(e => e.apply(0)).toList
  private val numberOfStyles: Int = csvData.length
  val styleDetailsFeeder: Iterator[Map[String, String]] =
    Iterator.continually(
      Map(
        "styleId" -> csvData.apply(Random.nextInt(numberOfStyles))
      )
    )

  /* Initialize Page Objects */
  val sessionSetUp: ChainBuilder = new LoginPage().sessionSetUp
  val pdpPageCalls =  new PdpPage().StyleDetails
  val cartPage = new CartPage().CartApis
  val paymentsPage = new PaymentsPage().Payments
  val addressPage = new AddressPage().AddressApi
  val paymentInstrumentPage = new PaymentInstrumentPage().PaymentInstrument

  private val getPaymentInstrumentApi =
    scenario("Payment Instrument")
      .exec(sessionSetUp)
      .feed(styleDetailsFeeder)
      .exec(
        pdpPageCalls.styleData,
        cartPage.addToCart,
        addressPage.getAllAddress,
        paymentInstrumentPage.paymentInstument
      )

  private val paynowFormOnline =
    scenario("pay_now_form_online")
      .exec(sessionSetUp)
      .feed(styleDetailsFeeder)
      .exec(
        pdpPageCalls.styleData,
        cartPage.addToCart,
        addressPage.getAllAddress,
        paymentInstrumentPage.paymentInstument,
        paymentsPage.onlinePayNow,
        paymentsPage.onlineCallBack
      )

  private val paynowFormOnlineForMultipleSku =
    scenario("paynowFormOnlineForMultipleSku")
      .exec(sessionSetUp)
      .feed(styleDetailsFeeder)
      .exec(
        pdpPageCalls.styleData,
        cartPage.addToCartForMultipleSku,
        addressPage.getAllAddress,
        paymentInstrumentPage.paymentInstument,
        paymentsPage.onlinePayNow,
        paymentsPage.onlineCallBack
      )

  private val paynowFormCod =
    scenario("paynowFormCod")
      .exec(sessionSetUp)
      .feed(styleDetailsFeeder)
      .exec(
        pdpPageCalls.styleData,
        cartPage.addToCart,
        addressPage.getAllAddress,
        paymentInstrumentPage.paymentInstument,
        paymentsPage.codPayNow
      )


  private val paynowformWallet =
    scenario("paynowFormWallet")
      .exec(sessionSetUp)
      .feed(styleDetailsFeeder)
      .exec(
        pdpPageCalls.styleData,
        cartPage.addToCart,
        addressPage.getAllAddress,
        paymentInstrumentPage.paymentInstument,
        paymentsPage.walletPay,
        paymentsPage.walletCallBack
      )


  private val v3PayNowCod =
    scenario("v3PayNowCod")
      .exec(sessionSetUp)
      .feed(styleDetailsFeeder)
      .exec(
        pdpPageCalls.styleData,
        cartPage.addToCart,
        addressPage.getAllAddress,
        paymentInstrumentPage.paymentInstument,
        paymentsPage.v3PayNowCod
      )

  private val v3PayNowOnline =
    scenario("v3PayNowOnline")
      .exec(sessionSetUp)
      .feed(styleDetailsFeeder)
      .exec(
        pdpPageCalls.styleData,
        cartPage.addToCart,
        addressPage.getAllAddress,
        paymentInstrumentPage.paymentInstument,
        paymentsPage.v3PayNowOnline,
        paymentsPage.onlineCallBack
      )

  private val v3PayNowWallet =
    scenario("v3PayNowWallet")
      .exec(sessionSetUp)
      .feed(styleDetailsFeeder)
      .exec(
        pdpPageCalls.styleData,
        cartPage.addToCart,
        addressPage.getAllAddress,
        paymentInstrumentPage.paymentInstument,
        paymentsPage.v3PayNowWallet,
        paymentsPage.walletCallBack
      )

  private val paynowDebug =
    scenario("paynow debug")
      .exec(
        paymentInstrumentPage.paymentInstument
      )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(csBaseUrl)
}
