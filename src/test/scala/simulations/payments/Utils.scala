package simulations.payments

import java.io._
import java.net.{HttpURLConnection, URL}

class Utils {

  private val solrURI = "http://pas-solr8:8984/solr/sprod/select?fl=styleid&fq=count_options_availbale:[1%20TO%20*]&_route_=2297&fq=styletype:P&q=*:*&rows=1000000&wt=csv"

  def downloadFile(fileToDownload: String): Unit = {
    try {
      println("Creating Styles file !!!!")
      var fileToDownloadAs = new java.io.File(fileToDownload)
      if (!fileToDownloadAs.exists()){
        val fileCreated = fileToDownloadAs.createNewFile()
        println(("File creation :: " + fileCreated))
      }
      println("File exists")
      val url = new URL(solrURI)
      val connection = url.openConnection().asInstanceOf[HttpURLConnection]
      connection.setRequestMethod("GET")
      val in: InputStream = connection.getInputStream
      /* Do not change the below path */
      val out: OutputStream = new BufferedOutputStream(new FileOutputStream(fileToDownloadAs))
      val byteArray = Stream.continually(in.read).takeWhile(-1 !=).map(_.toByte).toArray
      out.write(byteArray)
      in.close()
      out.flush()
      out.close()
    } catch {
      case e: FileNotFoundException =>
        println("Couldn't find that file.")
        e.printStackTrace()
      case e: IOException =>
        println("Got an IOException!")
        e.printStackTrace()
      case _: Throwable =>
        println("Failed to get Styles before Simulation")
    }
  }
}
