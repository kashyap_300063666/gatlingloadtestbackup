package simulations.payments

import java.security.MessageDigest

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.http.Predef.status
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import org.apache.commons.codec.binary.Hex._
import simulations.BaseSimulation

class PaymentPlanSimulation extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "cartpayments"

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.PPSINTERNAL.toString)
  private val defaultHeaders = Map("Accept"->"application/json",
    "Content-Type"->"application/json",
    "client"->"test",
    "version"->"1.0",
    "x-mynt-ctx"->"storeid=2297",
    "user-agen" ->  "Load Test User Agent",
    "deviceId" -> "1234",
    "Initiator" -> "loadtest")

/*  private val myDataFiles = Map(
    "testDataFile" -> "test-data/testPlanData.csv",
    "normalDataFile" -> "test-data/paymentPlanData.csv",
    "archivalDataFile" -> "test-data/archivalPaymentPlanData.csv"

  )*/

  private val myDataFiles = Map(
    "testDataFile" -> "testPlanData.csv",
    "normalDataFile" -> "paymentPlanData.csv",
    "archivalDataFile" -> "archivalPaymentPlanData.csv"

  )

  val csvDataFile = System.getProperty("paymentPlanDataFile", "testDataFile")
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, myDataFiles(csvDataFile))

  //val csvDataFileArchival = System.getProperty("archivalPaymentPlanDataFile", "test-data/archivalPaymentPlanData.csv")
  val csvDataFileArchival = System.getProperty("archivalPaymentPlanDataFile", myDataFiles("archivalDataFile"))


  val ppsBaseUrl = System.getProperty("baseUrl", defaultUrl)

  val httpProtocol = http
    .baseUrl(ppsBaseUrl)
    .headers(defaultHeaders)

  //val paymentPlanDataFeeder = csv(myDataFiles(csvDataFile)).circular
  val paymentPlanDataFeeder = csv(filePath).circular

  private val getArchivalPaymentPlan = scenario("getArchivalPaymentPlan")
    .feed(paymentPlanDataFeeder)
    .exec(http("getArchivalPaymentPlan")
      .post("/myntra-payment-plan-service/v3/archival/getPaymentPlan")
      .header("Authorization", session => getAuthorizationHeader("test", session("pps_Id").as[String], "test"))
      .body(StringBody("""{"ppsId": "${pps_Id}"}""")).asJson
      .check(status.is(200))
      .check(jsonPath("$.paymentPlanEntry.id").exists)
    )

  private val getArchivalPartialPaymentPlan = scenario("getArchivalPartialPaymentPlan")
    .feed(paymentPlanDataFeeder)
    .exec(http("getArchivalPartialPaymentPlan")
      .post("/myntra-payment-plan-service/v3/archival/getPartialPaymentPlan")
      .header("Authorization", session => getAuthorizationHeader("test", session("pps_Id").as[String], session("pricePerUnit").as[String], session("groupId").as[String], "test"))
      .body(StringBody("""{ "partialPaymentPlanItemRequests" : [{"ppsId":"${pps_Id}","groups":[{"groupId":"${groupId}","instrAmount":${pricePerUnit}}]}]}""")).asJson
      .check(jsonPath("$.paymentPlanEntries[0].id").exists)
      .check(status.is(200))
    )

  private val getPaymentPlan = scenario("getPaymentPlan")
    .feed(paymentPlanDataFeeder)
    .exec(http("getPaymentPlan")
      .post("/myntra-payment-plan-service/v3/getPaymentPlan")
      .header("Authorization", session => getAuthorizationHeader("test", session("pps_Id").as[String], "test"))
      .body(StringBody("""{"ppsId": "${pps_Id}"}""")).asJson
      .check(status.is(200))
      .check(jsonPath("$.paymentPlanEntry.id").exists)
    )

  private val getPartialPaymentPlan = scenario("getPartialPaymentPlan")
    .feed(paymentPlanDataFeeder)
    .exec(http("getPartialPaymentPlan")
      .post("/myntra-payment-plan-service/v3/getPartialPaymentPlan")
      .header("Authorization", session => getAuthorizationHeader("test", session("pps_Id").as[String], session("pricePerUnit").as[String], session("groupId").as[String], "test"))
      .body(StringBody("""{ "partialPaymentPlanItemRequests" : [{"ppsId":"${pps_Id}","groups":[{"groupId":"${groupId}","instrAmount":${pricePerUnit}}]}]}""")).asJson
      .check(jsonPath("$.paymentPlanEntries[0].id").exists)
      .check(status.is(200))
    )


  def getAuthorizationHeader(client : String, ppsId : String, clientKey: String): String = {
    val data = client + "|1.0|" + ppsId + "|" + clientKey
    val md = MessageDigest.getInstance("SHA-256")
    val hashBytes = md.digest(data.getBytes)
    encodeHexString(hashBytes)
  }

  def getAuthorizationHeader(client : String, ppsId : String, amount : String, groupId : String, clientKey: String): String = {
    val data = client + "|1.0|" + groupId + "|" + amount + "|0|" + ppsId + "|" + clientKey
    val md = MessageDigest.getInstance("SHA-256")
    val hashBytes = md.digest(data.getBytes)
    encodeHexString(hashBytes)
  }

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)

}
