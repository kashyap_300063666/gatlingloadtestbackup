package simulations.payments

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class PdpPage {

  val pdpHeaders = Map(
    "Content-Type" -> "application/json",
    "origin" -> "myntra.com",
    "x-server" -> "gateway"
  )

  object StyleDetails {
    val styleData = exec(http("Get Style Details")
      .get("/v1/product/${styleId}")
      .header("at" , "ZXlKaGJHY2lPaUpJVXpJMU5pSXNJbXRwWkNJNklqRWlMQ0owZVhBaU9pSktWMVFpZlEuZXlKdWFXUjRJam9pT0dKaE4yVTNPRFl0WXpKaU1DMHhNV1ZoTFdFME1qWXRNREF3WkROaFpqSTRaVFF4SWl3aVkybGtlQ0k2SW0xNWJuUnlZUzB3TW1RM1pHVmpOUzA0WVRBd0xUUmpOelF0T1dObU55MDVaRFl5WkdKbFlUVmxOakVpTENKaGNIQk9ZVzFsSWpvaWJYbHVkSEpoSWl3aWMzUnZjbVZKWkNJNklqSXlPVGNpTENKbGVIQWlPakUyTURrNU16azBNVEFzSW1semN5STZJa2xFUlVFaWZRLkVGblpDMldZcWs3Nzl6UzUxWWdYQ0pycjJTYkFBNWxkcm85R2FBRjVKdG8=")
      .headers(pdpHeaders)
      .check(status.is(200))
//      .check(jsonPath("$.style.sizes[0].skuId").findAll.saveAs("skuIdsList")))
      .check(jsonPath("$..sizes[?(@.inventory >= 1)].skuId").findAll.saveAs("skuIdsList")))
  }

}
