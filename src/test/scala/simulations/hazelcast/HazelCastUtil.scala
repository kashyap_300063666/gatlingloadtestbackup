package simulations.hazelcast

import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}


class HazelCastUtil {

  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "Authorization" -> "Basic YWJjOjEyMw==",
    "Connection" -> "Keep-Alive",
    "Keep-Alive" -> "timeout=600, max=1500"
  )

  val headers1 = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json"
  )

  object Api {
    val putReplicateCall = {
      exec(http("inventoryPut")
        .post("/addInvHC")
        .headers(headers)
        .body(StringBody(
          """{
            |  "id": ${id},
            |  "storeId": ${store_id},
            |  "sellerId": ${seller_id},
            |  "storePartnerId": ${store_partner_id},
            |  "sellerPartnerId": ${seller_partner_id},
            |  "styleId": ${style_id},
            |  "skuId": ${sku_id},
            |  "skuCode": "${sku_code}",
            |  "inventoryCount": ${inventory_count},
            |  "blockedOrderCount": ${blocked_order_count},
            |  "blockedFutureCount": ${blocked_future_count},
            |  "enabled": true,
            |  "lastSyncedOn": "${last_synced_on}",
            |  "availableInWarehouses": "${available_in_warehouses}",
            |  "vendorId": ${vendor_id},
            |  "createdOn": "${created_on}",
            |  "lastModifiedOn": "${last_modified_on}",
            |  "version": ${version},
            |  "createdBy": "${created_by}",
            |  "supplyType": "${supply_type}"
            |}""".stripMargin))
        //        .body(ElFileBody("test-data/inv_vertx_put_payload.json")).asJson
        .check(status.is(200)))
    }

    val getReplicatedCall = {
      exec(http("inventoryRead")
        .post("/portalInvHC")
        .headers(headers)
        .body(StringBody("""{ "sellerId": ${seller_id}, "storeId": ${store_id}, "styleId": ${style_id} }"""))
        //        .body(ElFileBody("test-data/inv_vertx_get_payload.json")).asJson
        .check(status.is(200)))
    }

    val getWebFluxCall = {
      exec(http("inventoryWebfluxAsync")
        .post("inventory")
        .headers(headers)
        .body(StringBody(
          """ {"inventoryList":[
            |{ "sellerId": ${seller_id}, "storeId": ${store_id}, "styleId": ${style_id} }
            |]
            |}""".stripMargin))
        .check(status.is(200)))
    }

    val getWebFluxCall2 = {
      exec(http("inventoryWebfluxSync")
        .post("inventorySync")
        .headers(headers1)
        .body(StringBody(
          """ {"inventoryList":[
            |{ "sellerId": ${seller_id}, "storeId": ${store_id}, "styleId": ${style_id} }
            |]
            |}""".stripMargin))
        .check(status.is(200)))
    }
  }

}



