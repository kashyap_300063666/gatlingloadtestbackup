package simulations.assignwarehouse

import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import simulations.BaseSimulation

/**
  * Created by madhava on 2021-01-14.
  */

class AssignWarehouseSimulation extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "wms"
  /* Name of File to be downloaded */
  val fileName = "awhData.csv"

  val httpConf: HttpProtocolBuilder = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.ASSIGNWAREHOUSE.toString)).disableWarmUp

  //val csvFile: String = System.getProperty("uatuStoreOrderIds", "test-data/awhData.csv")
  val csvFile: String = System.getProperty("uatuStoreOrderIds", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  val awhDataFeeder: BatchableFeederBuilder[String]#F = separatedValues(filePath, '|').circular

  val assignwarehouse: ScenarioBuilder = scenario("assignwarehouse")
    .feed(awhDataFeeder)
    .exec(http("assignwarehouse")
      .put("/myntra-assignwarehouse-service/awh/owner/2297/order/1/assignWh")
      .header("Authorization", "Basic cTph")
      .header("is-awh-load-test", "true")
      .headers(headers)
      .body(StringBody("${payload}")).asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusType").is("SUCCESS"))
      .check(jsonPath("$.data[0].onHold").is("false"))
    )


  setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec).protocols(httpConf)
}
