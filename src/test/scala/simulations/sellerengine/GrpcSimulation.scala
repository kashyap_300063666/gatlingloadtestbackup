package simulations.sellerengine

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import simulations.BaseSimulation
import io.grpc.ManagedChannelBuilder
import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.commons.ServiceType
import com.myntra.charge.dto.charge._
import com.myntra.charge.enums.charge_enum.ChargeType
import com.myntra.rubik.rubik.RedeemRequest
import io.gatling.core.session.Expression
import io.grpc.Metadata
import sellerengine.sellerResponse._;

class GrpcSimulation extends BaseSimulation{

  var grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.SELLERENGINE.toString, ""), grpcPortNum).usePlaintext())


  val searchRequest: Expression[SearchInputEntry] = SearchInputEntry(styleIds = List(10367281))

  val searchSpe = scenario("SPE-Get Style")
    .exec(grpc("SPE-Get Style")
      .rpc(SellerEngineSearchGrpc.METHOD_GET_STYLE_RESPONSE)
      .payload(searchRequest)
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)



}
