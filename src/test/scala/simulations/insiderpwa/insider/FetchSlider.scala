package simulations.insiderpwa


import io.gatling.core.Predef.configuration
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.scenario
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.value2Expression
import io.gatling.http.Predef.http
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.myntra.commons.util.BaseUrlConstructor
import scala.io.Source
import io.gatling.http.Predef.{http, status, _}


class FetchSlider extends BaseSimulation{
    
  
  var agentHeader = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 Edg/81.0.416.77".toString()
  

  val url = System.getProperty("baseUrl", "https://www.myntra.com/").toString
  
  protected val cookie = System.getProperty("insiderpwa.cookie").toString
  val cookieheader = Map("cookie" -> cookie)
  
  
  val httpProtocol = http
    .baseUrl(url)
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader(agentHeader).disableWarmUp
  
   private val nodeapp = scenario("insiderpwa")
      .exec(http("insiderSliderApi")
      .get("/myntrainsider/proxy/styleplusapi/slider-data/6?loadtest2=insiderpwa")
      .headers(cookieheader)
      .check(status.is(200)))
 
    
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}