package simulations.matrix.matrixservice

import java.util.UUID

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef.scenario
import io.gatling.http.Predef.status
import simulations.BaseSimulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class CreateSimulation extends BaseSimulation {

  private val matrixBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MATRIX_BACKEND.toString)).disableWarmUp

  // Video and job fetch API
  private val createJob = scenario("Create encoding job")
    .exec(http("Create encoding job")
      .post("/matrix/jobs")
      .body(StringBody("""{"videoId": "5ef3f5e3bd720711abd1082b"}"""))
      .asJson
      .check(status.is(202)))

  var randomNameGenerator = Iterator.continually(
    Map("name" -> ("LTtest-" + UUID.randomUUID().toString)))

  private val createVideo = scenario("create Video")
    .feed(randomNameGenerator)
    .exec(http("create Video")
      .post("/matrix/videos")
      .body(StringBody("""{"name": "${name}","rawVideoBlobId": "vid2.mp4","description": "Testing Purpose"}"""))
      .asJson
      .check(status.is(201)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(matrixBaseUrl)


}
