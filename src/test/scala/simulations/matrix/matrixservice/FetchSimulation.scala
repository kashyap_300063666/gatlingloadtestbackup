package simulations.matrix.matrixservice

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef.scenario
import io.gatling.http.Predef.status
import simulations.BaseSimulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class FetchSimulation extends BaseSimulation{
  private val matrixBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MATRIX_BACKEND.toString)).disableWarmUp

  // Video and job fetch API
  private val getAllJob = scenario("get All encoding jobs")
    .exec(http("getAllJobs")
      .get("/matrix/jobs")
      .check(status.is(200)))

  private val getAllVideos = scenario("get All Videos")
    .exec(http("getAllVideos")
      .get("/matrix/videos")
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(matrixBaseUrl)
}
