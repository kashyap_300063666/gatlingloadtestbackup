package simulations.lgp

import com.github.phisgr.gatling.grpc.Predef.{$, grpc}
import com.github.phisgr.gatling.pb._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import feed.feed.{FeedRequest, FeedServiceGrpc}
import io.gatling.core.Predef.{csv, scenario, _}
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.core.session.Expression
import io.grpc.{ManagedChannelBuilder, Metadata}
import simulations.BaseSimulation

class RawFeedCS extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "lookgood"
  /* Name of File to be downloaded */
  val fileNameAppFamily = "appfamily.csv"
  val filePathAppFamily: String = feederUtil.getFileDownloadedPath(containerName, fileNameAppFamily)
  val fileNameDeviceId = "deviceID.csv"
  val filePathDeviceId: String = feederUtil.getFileDownloadedPath(containerName, fileNameDeviceId)
  val fileNameDeviceIp = "deviceIP.csv"
  val filePathDeviceIp: String = feederUtil.getFileDownloadedPath(containerName, fileNameDeviceIp)
  val fileNamePageContext = "pageContext.csv"
  val filePathPageContext: String = feederUtil.getFileDownloadedPath(containerName, fileNamePageContext)
  val fileNamePincodes = "pincodecs.csv"
  val filePathPincodes: String = feederUtil.getFileDownloadedPath(containerName, fileNamePincodes)
  val fileNameUidx = "uidxlistcs.csv"
  val filePathUidx: String = feederUtil.getFileDownloadedPath(containerName, fileNameUidx)

  val platformFeeder = csv(filePathAppFamily).circular

  //val grpcConf = grpc(ManagedChannelBuilder.forAddress("devapp-lgpcampaignservice.dockins.myntra.com", 8080).usePlaintext())
  private val baseUrl = System.getProperty("singleServerURL")//Pass the complete url of single server if req
  var grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.LGPCAMPAIGNSERVICE.toString,""), grpcPortNum).usePlaintext())

  if(baseUrl != null) {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(baseUrl, grpcPortNum).usePlaintext())
    println("Updated BaseUrl:" + baseUrl)
  }

  //val pageContextFeeder = csv("test-data/pageContext.csv").circular
  val pageContextFeeder = csv(filePathPageContext).circular

  val uidxFeeder = csv(filePathUidx).circular

  //val deviceIdFeeder = csv("test-data/deviceID.csv").circular
  val deviceIdFeeder = csv(filePathDeviceId).circular

  //val deviceIpFeeder = csv("test-data/deviceIP.csv").circular
  val deviceIpFeeder = csv(filePathDeviceIp).circular

  //val platformFeeder = csv("test-data/appfamily.csv").circular

  val pinCodeFeeder = csv(filePathPincodes).circular.convert {

    case ("pincode", string) => string.toInt
  }

  //Feed request object
  var feedRequest: Expression[FeedRequest] = FeedRequest(pageContext = "home").updateExpr(_.pageContext :~ $("pagecontext"), (_.pincode :~ $("pincode")))

  val lgpcs = scenario("LGP Campaign Service grpc call")
    .feed(pageContextFeeder)
    .feed(pinCodeFeeder)
    .feed(uidxFeeder)
    .feed(deviceIdFeeder)
    .feed(deviceIpFeeder)
    .feed(platformFeeder)

    .exec(grpc("LGP - CampaignService - Get RawFeed")

      .rpc(FeedServiceGrpc.METHOD_GET_RAW_FEED)

      .payload(feedRequest)

      .header(Metadata.Key.of("uidx", Metadata.ASCII_STRING_MARSHALLER))("${uidx}")
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
      .header(Metadata.Key.of("x-myntra-app", Metadata.ASCII_STRING_MARSHALLER))("deviceID=${deviceid};appFamily=${appfamily};deviceIP=${deviceip};appVersion=3.29.0;")
    )

  setUp(scenarios map (e => e.inject(step)) toList)
    .protocols(grpcConf)
}