package simulations.mipricing


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import simulations.mipricing.MiPricingUtils.getCiData

class CiOnLoad extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "vorta"

  private val serviceUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MIPRICING.toString)

  def forceRefresh: Boolean = getProperty("CACHE_REFRESH", "false").toBoolean

  private val httpProtocol = http
    .baseUrl(serviceUrl)
    .header("Accept", "application/json")
    .header("userId", "mi_partner_2@myntra.com")
    .header("tenantId", "spectrum")
    .header("Content-Type", "application/json")
    .header("X-Myntra-forceRefreshCache", forceRefresh.toString())
    .disableWarmUp


  val filePath38: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0038_request.json")
  val filePath39: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0039_request.json")
  val filePath40: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0040_request.json")
  val filePath41: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0041_request.json")
  val filePath42: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0042_request.json")
  val filePath43: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0043_request.json")
  val filePath44: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0044_request.json")
  val filePath45: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0045_request.json")
  val filePath46: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0046_request.json")
  val filePath47: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0047_request.json")
  val filePath48: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0048_request.json")
  val filePath49: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0049_request.json")
  val filePath50: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0050_request.json")
  val filePath51: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0051_request.json")
  val filePath52: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0052_request.json")
  val filePath53: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0053_request.json")
  val filePath54: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0054_request.json")
  val filePath55: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0055_request.json")
  val filePath56: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0056_request.json")
  val filePath57: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0057_request.json")
  val filePath58: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0058_request.json")
  val filePath59: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0059_request.json")
  val filePath60: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0060_request.json")
  val filePath61: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0061_request.json")
  val filePath12: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0012_request.json")

  val scn = scenario("Ci Page Loading")
    .exec(http("Get CI Bags")
      .get("ci/bags"))
    .exec(http("CI Metadata")
        .get("metadata/CI?workspaceType%5Bmessage%5D=Consumer%20Insights%20-%20Metadata"))
    .exec(http("CI Competitor Search")
      .post("ci/search")
      .body(RawFileBody(filePath12)))
    .pause(389 milliseconds)
    .exec(getCiData(filePath38))
    .exec(getCiData(filePath39))
    .exec(getCiData(filePath40))
    .exec(getCiData(filePath41))
    .exec(getCiData(filePath42))
    .exec(getCiData(filePath43))
    .exec(getCiData(filePath44))
    .exec(getCiData(filePath45))
    .exec(getCiData(filePath46))
    .exec(getCiData(filePath47))
    .exec(getCiData(filePath48))
    .exec(getCiData(filePath49))
    .exec(getCiData(filePath50))
    .exec(getCiData(filePath51))
    .exec(getCiData(filePath52))
    .exec(getCiData(filePath53))
    .exec(getCiData(filePath54))
    .exec(getCiData(filePath55))
    .exec(getCiData(filePath56))
    .exec(getCiData(filePath57))
    .exec(getCiData(filePath58))
    .exec(getCiData(filePath59))
    .exec(getCiData(filePath60))
    .exec(getCiData(filePath61))

  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}
