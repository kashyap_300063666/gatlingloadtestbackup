package simulations.mipricing

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import MiPricingUtils._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import simulations.BaseSimulation

class WorkbenchBrandSelection extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "vorta"
  /* Name of File to be downloaded */

  private val serviceUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MIPRICING.toString)

  def forceRefresh: Boolean = getProperty("CACHE_REFRESH", "false").toBoolean

  private val httpProtocol = http
    .baseUrl(serviceUrl)
    .header("Accept", "application/json")
    .header("userId", "gaurav.raj@myntra.com")
    .header("Content-Type", "application/json")
    .header("X-Myntra-forceRefreshCache", forceRefresh.toString())
    .disableWarmUp

  val filePath28: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0028_request.json")
  val filePath29: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0029_request.json")
  val filePath30: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0030_request.json")
  val filePath32: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0032_request.json")
  val filePath33: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0033_request.json")
  val filePath34: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0034_request.json")
  val filePath35: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0035_request.json")
  val filePath36: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0036_request.json")
  val filePath37: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0037_request.json")
  val filePath38: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0038_request.json")
  val filePath39: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0039_request.json")
  val filePath40: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0040_request.json")
  val filePath41: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0041_request.json")
  val filePath42: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0042_request.json")
  val filePath43: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0043_request.json")
  val filePath44: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0044_request.json")
  val filePath45: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0045_request.json")
  val filePath46: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0046_request.json")
  val filePath47: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0047_request.json")
  val filePath48: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0048_request.json")
  val filePath49: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0049_request.json")
  val filePath50: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0050_request.json")
  val filePath51: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0051_request.json")
  val filePath52: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0052_request.json")
  val filePath53: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0053_request.json")
  val filePath54: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0054_request.json")

  val filePath4: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0004_request.json")
  val filePath6: String = feederUtil.getFileDownloadedPath(containerName, "workbenchbrandselectionsimulation_0006_request.json")


  private val scn = scenario("Workbench Brand Selection Scenario")
    .exec(getDashboards("SYSTEM", filePath4))
    .exec(getBrandsMetadata(filePath6))
    .pause(500 milliseconds)
    .exec(getCategoryMetadata("Men-Sweatshirts"))
    .exec(getGraphData(filePath28))
    .exec(getGraphData(filePath29))
    .exec(getGraphData(filePath30))
    .exec(getGraphData(filePath32))
    .exec(getGraphData(filePath33))
    .exec(getGraphData(filePath34))
    .exec(getGraphData(filePath35))
    .exec(getGraphData(filePath36))
    .exec(getGraphData(filePath37))
    .exec(getGraphData(filePath38))
    .exec(getGraphData(filePath39))
    .exec(getGraphData(filePath40))
    .exec(getGraphData(filePath41))
    .exec(getGraphData(filePath42))
    .exec(getGraphData(filePath43))
    .exec(getGraphData(filePath44))
    .exec(getGraphData(filePath45))
    .exec(getGraphData(filePath46))
    .exec(getGraphData(filePath47))
    .exec(getGraphData(filePath48))
    .exec(getGraphData(filePath49))
    .exec(getGraphData(filePath50))
    .exec(getGraphData(filePath51))
    .exec(getGraphData(filePath52))
    .exec(getGraphData(filePath53))
    .exec(getGraphData(filePath54))
    .pause(3)

  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}

