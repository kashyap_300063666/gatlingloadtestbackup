package simulations.mipricing

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

object MiPricingUtils {

  def getBrandReportMetadata(brandName: String): ChainBuilder = {
    exec(
      http("Brand Report Metadata")
        .get(s"reports/metadata/brand?brandname=${brandName}")
    )
  }

  def getBrandScorecard(payloadFilename: String): ChainBuilder = {
    exec(
      http("Brand Scorecard")
        .post("reports/scorecard/brand")
        .body(RawFileBody(payloadFilename))
    )
  }

  def getWorkspaceFilters() = {
    exec(
      http("Get Workspace Filters")
        .get("workspace/filters")
    )
  }

  def getGraphData(payloadFilename: String) = {
    exec(
      http("Get Graph Data")
        .post("graph/data")
        .body(RawFileBody(payloadFilename)))
  }

  def getDashboards(dashboardType: String, filtersPayload: String) = {
    exec(
      http(s"Get ${dashboardType} dashboards")
        .post(s"dashboard/filter?workspaceType=${dashboardType}")
        .body(RawFileBody(filtersPayload))
    )
  }

  def getSystemDashboardFilters() = {
    exec(
      http("Get System Dashboard Metadata")
        .get("metadata?workspaceType=SYSTEM")
    )
  }

  def getCategoryMetadata(category: String) = {
    exec(
      http("Get category metadata")
        .get(s"metadata/workbench?category=${category}")
    )
  }

  def getBrandsMetadata(brandPayload: String) = {
    exec(http("Get Brand Metadata")
      .post("metadata/brands?workspaceType=SYSTEM")
      .body(RawFileBody(brandPayload)))
  }

  def getCiData(payloadFilename: String) = {
    exec(http("Get CI Data")
      .post("ci/data")
      .body(RawFileBody(payloadFilename)))
  }
}
