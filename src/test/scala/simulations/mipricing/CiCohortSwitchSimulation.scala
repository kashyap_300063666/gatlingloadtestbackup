package simulations.mipricing

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import simulations.mipricing.MiPricingUtils.getCiData

class CiCohortSwitchSimulation extends BaseSimulation {

  private val serviceUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MIPRICING.toString)
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "vorta"

  def forceRefresh: Boolean = getProperty("CACHE_REFRESH", "false").toBoolean

  val filePath111: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0111_request.json")
  val filePath112: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0112_request.json")
  val filePath113: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0113_request.json")
  val filePath114: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0114_request.json")
  val filePath115: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0115_request.json")
  val filePath116: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0116_request.json")
  val filePath117: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0117_request.json")
  val filePath118: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0118_request.json")
  val filePath119: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0119_request.json")
  val filePath120: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0120_request.json")

  val filePath210: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0210_request.json")
  val filePath211: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0211_request.json")
  val filePath212: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0212_request.json")
  val filePath213: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0213_request.json")
  val filePath214: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0214_request.json")
  val filePath215: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0215_request.json")
  val filePath216: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0216_request.json")
  val filePath217: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0217_request.json")
  val filePath218: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0218_request.json")
  val filePath219: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0219_request.json")

  val filePath310: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0310_request.json")
  val filePath311: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0311_request.json")
  val filePath312: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0312_request.json")
  val filePath313: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0313_request.json")
  val filePath314: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0314_request.json")
  val filePath315: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0315_request.json")
  val filePath316: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0316_request.json")
  val filePath317: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0317_request.json")
  val filePath318: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0318_request.json")
  val filePath319: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0319_request.json")


  private val httpProtocol = http
    .baseUrl(serviceUrl)
    .header("Accept", "application/json")
    .header("userId", "mi_partner_2@myntra.com")
    .header("tenantId", "spectrum")
    .header("Content-Type", "application/json")
    .header("X-Myntra-forceRefreshCache", forceRefresh.toString())
    .disableWarmUp

  val scn = scenario("CI Cohort Switching")
    .exec(getCiData(filePath111))
    .exec(getCiData(filePath112))
    .exec(getCiData(filePath113))
    .exec(getCiData(filePath114))
    .exec(getCiData(filePath115))
    .exec(getCiData(filePath116))
    .exec(getCiData(filePath117))
    .exec(getCiData(filePath118))
    .exec(getCiData(filePath119))
    .exec(getCiData(filePath120))
    .pause(10 seconds)
    .exec(getCiData(filePath210))
    .exec(getCiData(filePath211))
    .exec(getCiData(filePath212))
    .exec(getCiData(filePath213))
    .exec(getCiData(filePath214))
    .exec(getCiData(filePath215))
    .exec(getCiData(filePath216))
    .exec(getCiData(filePath217))
    .exec(getCiData(filePath218))
    .exec(getCiData(filePath219))
    .pause(10 seconds)
    .exec(getCiData(filePath310))
    .exec(getCiData(filePath311))
    .exec(getCiData(filePath312))
    .exec(getCiData(filePath313))
    .exec(getCiData(filePath314))
    .exec(getCiData(filePath315))
    .exec(getCiData(filePath316))
    .exec(getCiData(filePath317))
    .exec(getCiData(filePath318))
    .exec(getCiData(filePath319))

  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}
