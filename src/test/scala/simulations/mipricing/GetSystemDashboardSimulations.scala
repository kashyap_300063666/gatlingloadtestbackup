package simulations.mipricing

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import MiPricingUtils._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import simulations.BaseSimulation

class GetSystemDashboardSimulations extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "vorta"
  val fileNameBrandFeeder = "brands.csv"
  val filePathBrandFeeder: String = feederUtil.getFileDownloadedPath(containerName, fileNameBrandFeeder)
  val fileNameDashboardPayloadTemplate = "DashboardPayloadTemplate.json"
  val filePathDashboardPayloadTemplate: String = feederUtil.getFileDownloadedPath(containerName, fileNameDashboardPayloadTemplate)


  private val serviceUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MIPRICING.toString)

  private val httpProtocol = http
    .baseUrl(serviceUrl)
    .header("Accept", "application/json")
    .header("userId", "gaurav.raj@myntra.com")
    .header("Content-Type", "application/json")
    .disableWarmUp

  private val brandFeeder = csv(filePathBrandFeeder).random

  def getSystemDashboard() = {
    feed(brandFeeder)
      .exec(
        http(s"Get System dashboards")
          .post(s"dashboard/filter?workspaceType=SYSTEM")
          .body(ElFileBody(filePathDashboardPayloadTemplate))
      )
      .pause(2 seconds)
  }

  private val scn = scenario("Get System Dashboard Scenario")
    .exec(getSystemDashboard())

  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}
