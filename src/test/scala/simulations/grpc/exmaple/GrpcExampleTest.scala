package simulations.grpc.exmaple

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.github.phisgr.gatling.grpc.Predef._
import io.grpc.ManagedChannelBuilder
import com.github.phisgr.gatling.pb._
import io.grpc.health.v1.health.{HealthCheckRequest, HealthGrpc}


class GrpcExampleTest extends BaseSimulation {


 val grpcConf = grpc(ManagedChannelBuilder.forAddress("photoslibrary.googleapis.com", 443))

//
//    val s = scenario("Example")
//    .exec(
//      grpc("Register")
//      .rpc(HealthProto.)
//        .payload(HealthCheckRequest.defaultInstance)
//
//    )

 val s = scenario("Grpc Check hello")
   .exec(grpc("request_done")
   .rpc(HealthGrpc.METHOD_CHECK)
   .payload(HealthCheckRequest.defaultInstance))



  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}