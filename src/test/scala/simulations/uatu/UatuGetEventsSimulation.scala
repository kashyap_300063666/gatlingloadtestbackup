package simulations.uatu

import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import simulations.BaseSimulation

class UatuGetEventsSimulation extends BaseSimulation {
	private val feederUtil = new FeederUtil()
	/* Name of the team Name */
	val containerName = "oms"
	/* Name of File to be downloaded */
	val fileName = "uatuStoreOrderIds.csv"

	val httpConf: HttpProtocolBuilder = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.UATU.toString)).disableWarmUp
	//val csvFile: String = System.getProperty("uatuStoreOrderIds", "test-data/uatuStoreOrderIds.csv")
	val csvFile: String = System.getProperty("uatuStoreOrderIds", fileName)
	val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

	val storeOrderIdsFeeder: BatchableFeederBuilder[String]#F = csv(filePath).circular

	val UATU: ScenarioBuilder = scenario("UATU")
  	.feed(storeOrderIdsFeeder)
		.exec(http("UATU")
		.post("/uatu-service/uatu/owner/2297/tracking/search")
		.header("authorization", "Basic YTpi")
		.header("cache-control", "no-cache")
		.header("content-type", "application/json")
		.header("x-myntra-client-id", "scmloadtest")
		.headers(headers)
		.body(StringBody(
  	"""[{"storeOrderId":"${s1}","storePartnerId":2297},
			|{"storeOrderId":"${s2}","storePartnerId":2297},
			|{"storeOrderId":"${s3}","storePartnerId":2297},
			|{"storeOrderId":"${s4}","storePartnerId":2297},
			|{"storeOrderId":"${s5}","storePartnerId":2297}]""".stripMargin)
		).asJson
		.check(status.is(200))
	)


setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec).protocols(httpConf)
}