package simulations.genie

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.http
import simulations.BaseSimulation

class GenieCreatePicklistsAndVerifyStatus extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "wms"
  /* Name of File to be downloaded */
  val fileName = "genieSkuIds.csv"

  private val genieBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.GENIE.toString)).disableWarmUp

  //val csvFile = System.getProperty("genieOrderIdSkus", "test-data/genieSkuIds.csv")
  val csvFile: String = System.getProperty("genieOrderIdSkus", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  val csvfeeder = csv(filePath).circular

  private val preRequisiteScenario =
    scenario("CreatePicklistWithOrders")
      .feed(csvfeeder)
      .exec(GenieServices.searchCreatedOrdersWithSameSkus)
      .exec(GenieServices.createPicklistWithOrders)

  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(genieBaseUrl)
}