package simulations.atpvertxredis

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef.{configuration, constantUsersPerSec, csv, holdFor, openInjectionProfileFactory, reachRps, scenario}
import io.gatling.http.Predef.http
import simulations.BaseSimulation
import io.gatling.core.session._

class PortalInventorySimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "wms"
  /* Name of File to be downloaded */
  val fileName = "vertx-hz-inv.csv"

  private val url = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.ATP_VERTX_REDIS.toString)).disableWarmUp
  //private val csvFile = System.getProperty("dataprovider", "test-data/vertx-hz-inv.csv")
  val csvFile: String = System.getProperty("dataprovider", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  private val csvfeeder = csv(filePath).circular
  private val apis = new AtpVertxRedisUtil().API
  private val batchExpression = 20.expressionSuccess

  private val portalInvLT = scenario("portalInvLT")
    .feed(csvfeeder, batchExpression)
    .exec(
      apis.getPortalCall
    )

  setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec).protocols(url)
}
