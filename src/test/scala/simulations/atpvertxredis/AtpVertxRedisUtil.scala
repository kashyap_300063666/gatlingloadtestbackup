package simulations.atpvertxredis

import io.gatling.core.Predef._
import io.gatling.core.session.Expression
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef.{http, status, _}

class AtpVertxRedisUtil {
  private val headers = Map(
    "Accept" -> "application/json",
    "Content-type" -> "application/json",
    "Authorization" -> "YTpi"
  )

  object API {
    val getPortalCall: ChainBuilder = {
      exec(
        http("portalInvRedis")
          .post("/portalInvRedis")
          .headers(headers)
          .body(StringBody(
            """[
              |{ "sellerId": ${seller_id1}, "storeId": ${store_id1}, "styleId": ${style_id1} }
              |{ "sellerId": ${seller_id2}, "storeId": ${store_id2}, "styleId": ${style_id2} }
              |{ "sellerId": ${seller_id3}, "storeId": ${store_id3}, "styleId": ${style_id3} }
              |{ "sellerId": ${seller_id4}, "storeId": ${store_id4}, "styleId": ${style_id4} }
              |{ "sellerId": ${seller_id5}, "storeId": ${store_id5}, "styleId": ${style_id5} }
              |{ "sellerId": ${seller_id6}, "storeId": ${store_id6}, "styleId": ${style_id6} }
              |{ "sellerId": ${seller_id7}, "storeId": ${store_id7}, "styleId": ${style_id7} }
              |{ "sellerId": ${seller_id8}, "storeId": ${store_id8}, "styleId": ${style_id8} }
              |{ "sellerId": ${seller_id9}, "storeId": ${store_id9}, "styleId": ${style_id9} }
              |{ "sellerId": ${seller_id10}, "storeId": ${store_id10}, "styleId": ${style_id10} }
              |{ "sellerId": ${seller_id11}, "storeId": ${store_id11}, "styleId": ${style_id11} }
              |{ "sellerId": ${seller_id12}, "storeId": ${store_id12}, "styleId": ${style_id12} }
              |{ "sellerId": ${seller_id13}, "storeId": ${store_id13}, "styleId": ${style_id13} }
              |{ "sellerId": ${seller_id14}, "storeId": ${store_id14}, "styleId": ${style_id14} }
              |{ "sellerId": ${seller_id15}, "storeId": ${store_id15}, "styleId": ${style_id15} }
              |{ "sellerId": ${seller_id16}, "storeId": ${store_id16}, "styleId": ${style_id16} }
              |{ "sellerId": ${seller_id17}, "storeId": ${store_id17}, "styleId": ${style_id17} }
              |{ "sellerId": ${seller_id18}, "storeId": ${store_id18}, "styleId": ${style_id18} }
              |{ "sellerId": ${seller_id19}, "storeId": ${store_id19}, "styleId": ${style_id19} }
              |{ "sellerId": ${seller_id20}, "storeId": ${store_id20}, "styleId": ${style_id20} }
              |]""".stripMargin))
          .check(status.is(200))
      )
    }
  }
}
