package simulations.flash
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import simulations.hazelcast.HazelCastUtil

class TeslaSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "bicont"
  /* Name of File to be downloaded */
  val fileName = "flashLT.csv"
  val csvFile: String = System.getProperty("dataprovider", fileName)

  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  val httpConf = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.FLASH.toString)).disableWarmUp
  //val csvfeeder = csv(System.getProperty("dataprovider", "test-data/flashLT.csv")).circular
  val csvfeeder = csv(filePath).circular

  val GetMetricsv2Copy = scenario("GetMetricsv2Copy")
    .feed(csvfeeder)
    .exec(http("GetMetricsv2Copy")
      .get("/nrt/metrics?entity=person&id=${uidx}&vector-type=ads-vector")
      .headers(headers)
      .queryParam("id","$uidx")
        .queryParam("entity", "person")
        .queryParam("vector-type","vectors")
      .check(status.is(200))
    )

  //setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpConf)
  setUp(scenarios map (e =>
    e.inject(step))
  ).maxDuration(maxDurationInSec).protocols(httpConf)
}
