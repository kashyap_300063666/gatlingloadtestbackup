package simulations.poc

import com.myntra.commons.util.{HLTUtility, RedisUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class CartPage {

  /* Utility class for HLT */
  val util = new HLTUtility()

  val cartHeaders = Map(
    "content-type" -> "application/json",
    "at" -> "${at}"
  )

  object CartApis {
    val addToCart = doIf(session => util.redisUtil.hget("ApiEnableDisable", "addToCart").toBoolean) {
      exec(http("Add to Cart")
        .post("/v1/cart/default/add")
        .headers(cartHeaders)
        .body(StringBody("""{"id":${styleId},"skuId": ${skuId},"quantity":1}""")).asJson
        .check(status.is(200)))
    }
    val checkOutCart = doIf(session => util.redisUtil.hget("ApiEnableDisable", "checkOutCart").toBoolean) {
      exec(http("Checkout Cart")
        .get("/v1/cart/default")
        .headers(cartHeaders)
        .check(status.is(200)))
    }
  }

}
