package simulations.poc

import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}


class HazelCastUtil {

  val headers = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "Authorization" -> "Basic YWJjOjEyMw=="
  )

  object Api {
    val getCall = {
      exec(http("inventoryRead")
        .get("/inventory/skuId/${sku_id}/supplyType/${supply_type}/storeId/${store_id}/sellerId/${seller_id}")
        .headers(headers)
        .check(status.is(200)))
    }

    val getReplicatedCall = {
      exec(http("inventoryRead")
        .get("/inventory/replicated/skuId/${sku_id}/supplyType/${supply_type}/storeId/${store_id}/sellerId/${seller_id}")
        .headers(headers)
        .check(status.is(200)))
    }
  }
}



