package simulations.poc

import com.myntra.commons.util.HLTUtility
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class PdpPage {

  /* Utility class for HLT */
  val util = new HLTUtility()

  val pdpHeaders = Map(
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "content-type"-> "application/json",
    "at"-> "${at}",
    "xid"-> "${xid}",
    "sxid"-> "${sxid}"
  )


  object StyleDetails {
    val styleData = doIf(session => util.redisUtil.hget("ApiEnableDisable", "styleData").toBoolean) {
      exec(http("Get Style Details")
        .get("/product/${styleId}?co=1")
        .headers(pdpHeaders)
        .check(status.is(200)))
    }

    val styleOffers = doIf(session => util.redisUtil.hget("ApiEnableDisable", "styleOffers").toBoolean) {
      exec(http("Get Style Offers")
        .get("/product/${styleId}/offers?co=1")
        .headers(pdpHeaders)
        .check(status.is(200)))
    }

    val similarStyles = doIf(session => util.redisUtil.hget("ApiEnableDisable", "similarStyles").toBoolean) {
      exec(http("Get Similar Styles")
        .get("/product/${styleId}/related?co=1&colors=false")
        .headers(pdpHeaders)
        .check(status.is(200)))
    }
  }
}
