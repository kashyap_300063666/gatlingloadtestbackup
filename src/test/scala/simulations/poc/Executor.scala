package simulations.poc

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, HLTUtility, RedisUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.gatling.core.session.Session

class Executor extends BaseSimulation {
  private val csBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)).disableWarmUp
  private val counterKeyName = System.getProperty("counter.key", "counter")
  protected val util = new HLTUtility()

  val enableDisableApi: Iterator[Product2[String, String]] =
    List("home" -> "true", "topnav" -> "true", "recommended" -> "true",
      "pagination" -> "true", "sort" -> "true", "styleData" -> "true",
      "similarStyles" -> "true", "addToCart" -> "true", "checkOutCart" -> "true").iterator
  util.redisUtil.hmset("ApiEnableDisable", enableDisableApi.toIterable)


  private val setSession =
    scenario("Hlt poc run")
      .exec(session => {
        val sessionVar: Session = session.set("emailId", util.redisUtil.decreamentCounter(counterKeyName) + "@myntra360.com")
        val convertedObject: JsonObject = new Gson().fromJson(util.redisUtil.get(sessionVar("emailId").as[String])
          , classOf[JsonObject])

        sessionVar.set("at", convertedObject.get("at").getAsString)
          .set("rt", convertedObject.get("rt").getAsString)
          .set("sxid", convertedObject.get("sxid").getAsString)
          .set("xid", convertedObject.get("xid").getAsString)
          .set("uidx", convertedObject.get("uidx").getAsString)
          .set("styleId", 2272201)
          .set("skuId", 14814228)
          .set("queryParam", "shirt")
          .set("pageNos", 3)
          .set("filterquery", "sort=low")
      })
      .exec(new HomePage().Login.home, new HomePage().Login.topnav, new HomePage().Login.recommended,
        new ListPage().Search.pagination, new ListPage().Search.sort,
        new PdpPage().StyleDetails.styleData, new PdpPage().StyleDetails.similarStyles,
        new CartPage().CartApis.addToCart, new CartPage().CartApis.checkOutCart)

  setUp(scenarios map (e => e.inject(step)) toList)
    .protocols(csBaseUrl)
}
