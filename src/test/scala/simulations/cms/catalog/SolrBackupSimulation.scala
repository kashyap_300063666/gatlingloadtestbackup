package simulations.cms.catalog

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random


class SolrBackupSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "cms"
  /* Name of File to be downloaded */
  val fileName = "m_skus.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val solrBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.SOLR1.toString)).disableWarmUp
  var path = System.getProperty("user.dir");


  var skuIdsMyntraFeeder = Iterator.continually(
    Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePath, false).map(e => e.apply(0)).toList).take(5).mkString(" ")
    )
  )

  var skuIdsMyntraFeeder50 = Iterator.continually(
    Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePath, false).map(e => e.apply(0)).toList).take(50).mkString(",")
    )
  )

  var skuIdsMyntraFeeder10 = Iterator.continually(
    Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePath, false).map(e => e.apply(0)).toList).take(10).mkString(",")
    )
  )

  var skuIdsMyntraFeeder20 = Iterator.continually(
    Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePath, false).map(e => e.apply(0)).toList).take(20).mkString(",")
    )
  )

  private val v2_searchMyntra =
      scenario("v2_searchMyntra")
        .feed(skuIdsMyntraFeeder)
        .exec(http("v2_searchMyntra")
          .get("/solr/cms/select?q=skuIds:(${skuIds})")
          .headers(headers)
          .check(status.is(200)))


  private val v2search_MyntraFifty =
    scenario("v2search_MyntraFifty")
      .feed(skuIdsMyntraFeeder50)
      .exec(http("v2search_MyntraFifty")
        .get("/solr/cms/select?q=skuIds:(${skuIds})")
        .headers(headers)
        .check(status.is(200)))

  private val v2searchMyntra_Ten =
    scenario("v2searchMyntra_Ten")
      .feed(skuIdsMyntraFeeder10)
      .exec(http("v2searchMyntra_Ten")
        .get("/solr/cms/select?q=skuIds:(${skuIds})")
        .headers(headers)
        .check(status.is(200)))


  private val v2_search_MyntraTwenty =
    scenario("v2_search_MyntraTwenty")
      .feed(skuIdsMyntraFeeder20)
      .exec(http("v2_search_MyntraTwenty")
        .get("/solr/cms/select?q=skuIds:(${skuIds})")
        .headers(headers)
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(solrBaseUrl)
}