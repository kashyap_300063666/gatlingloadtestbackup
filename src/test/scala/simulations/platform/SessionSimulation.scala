package simulations.platform


import io.gatling.core.Predef.configuration
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.scenario
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.value2Expression
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation


class SessionSimulation extends BaseSimulation {
  private val sessionBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.SESSION_SERVICE.toString)).disableWarmUp
  val r = scala.util.Random

  private val create = exec(session => session.set("r1_id", r.nextInt(2147483647)))
    .exec(session => session.set("r2_id", r.nextInt(2147483647)))
    .exec(session => session.set("r3_id", r.nextInt(2147483647)))
    .exec(session => session.set("r4_id", r.nextInt(2147483647)))
    .exec(session => session.set("r5_id", r.nextInt(2147483647)))
    .exec(session => session.set("r6_id", r.nextInt(2147483647)))
    .exec(http("session create")
      .put("/SessionStoreService/rest/opt/sessions/")
      .body(StringBody("""{"r1Id":"test${r1_id}","r2Id":"test${r2_id}","r3Id":"test${r3_id}","r4Id":"test${r4_id}","r5Id":"test${r5_id}","r6Id":"test${r6_id}"}""")).asJson
      .headers(headers)
      .check(bodyString.saveAs("session_id"))
      .check(
        status.in(200, 404, 405, 408, 500, 503, 504),
        status.transform(status => 200.equals(status)).saveAs("PASS")
      )
    )


  private val update = exec(http("session update")
    .post("/SessionStoreService/rest/opt/sessions/${session_id}")
    .body(StringBody("""{"loginId":"test"}""")).asJson
    .headers(headers)
    .header("XED", "1571126379")
    .header("XLA", "1571126379")
    .check(status.is(200))
  )

  private val fetch = doIf("${PASS}") {
    repeat(10){
      exec(http("session fetch")
        .get("/SessionStoreService/rest/opt/sessions/${session_id}")
        .headers(headers)
        .check(status.is(200))
      )
    }
  }

  val finalSeq = Seq(create, fetch)
  val finalScenario = scenario("finalScenario").exec(finalSeq)




//  private val poheaders = Map("cache-control" -> "no-cache", "accept" -> "application/json", "Content-Type" -> "application/json")
//
//  private val tokenServiceLoad =
//    scenario("get token service")
//      .exec(http("get token service")
//        .post("/tokenservice/token/")
//        .headers(headers)
//        .headers(poheaders)
//        .body(StringBody("""{"clientId": "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61"}""")).asJson
//        .check(status.is(200))
//        .check(jsonPath("$.status.statusMessage").is("Token Created Successfully")))

    setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(sessionBaseUrl)


}

