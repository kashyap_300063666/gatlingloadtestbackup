package simulations.platform

import java.time.{Instant, LocalDateTime, ZoneId}
import java.util.Date

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class MYSSSimulation extends BaseSimulation {
  private val myssBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MYSS.toString)).disableWarmUp

  val random = new scala.util.Random(31)

  val date1 = new Date()

  val airbusCallbackOffsetTime = getIntProperty("airbusCallbackOffsetTime", "10")
  val httpCallbackOffsetTime = getIntProperty("httpCallbackOffsetTime", "10")
  val pauseTime = getIntProperty("pauseTime", "10")
  val clientName = getProperty("clientName", "test")

  private val poheaders = Map("cache-control" -> "no-cache", "accept" -> "application/json", "Content-Type" -> "application/json", "x-myntra-client-id" -> clientName)


  // convert to LocalDateTime
  val ldt = LocalDateTime.ofInstant(Instant.ofEpochMilli(date1.getTime), ZoneId.systemDefault)

  // add five minutes
  val incrementedBy2 = ldt.plusMinutes(5)
  val sTime = java.lang.Long.getLong("sTime", 0L)

  val appName = System.getProperty("appName", "YantraLoadTest").toString
    val payloadSize = java.lang.Integer.getInteger("payloadSize", 1)
  println(appName + "-------------" + payloadSize)

  val rampUpPercentage = getIntProperty("rampUpPercentage", "20")

  // convert to Long
  val scheduleTime: Long = incrementedBy2.atZone(ZoneId.systemDefault()).toEpochSecond
  println(scheduleTime)
  println(sTime)


  def callbackOffsetTime(min: Int) = LocalDateTime.ofInstant(Instant.ofEpochMilli(new Date().getTime), ZoneId.systemDefault).plusMinutes(min).atZone(ZoneId.systemDefault()).toEpochSecond


  object randomStringGenerator {
    def randomString(length: Int) = scala.util.Random.alphanumeric.filter(_.isLetter).take(length).mkString
  }

  val randomString = randomStringGenerator.randomString(262000)

  val firstFeeder = Array("ajdjkaasdabsdmnabsmndbmasbdashdkjahkdhkajhsdhkasjhdkjshakdhkashdasjasjckjhaschaskhckashchaskhckascmnbamsnbdmnabsmdbmasbmdasdjsjhdakshdkahdhasd", "jhakjhasdbasnbdnmbasbdnmasbndbnamsbmdasdasdahaashkdjhadas", "ajdhasd", "akdhakshdahskjdhhaskjhdahskjhdkahsjdhasdhashdashdkjahsdasdsadaaskjdhkajhsd", "ac", "kwhdhwajkdhawhjdhajhwhdawkjdhahwhdawd", "", "", "", "asdhjasgdgasjgdjagshjdgajhgsdjhgmasmdbmanbsdmbmnasbnmdbmbasmbdmabsmnbdmabsmdbasasjgdjhagsjgda", "", "", "ajshdjhad", "", "OUTRIGHT").map(e => Map("firstFeedData" -> e)).array.circular
  val secondFeeder = Array("adsg", "sadsad", "ajdhasd", "adagd", "ac", "qeeyqywieyqiweqwqewqie", "", "", "", "queqwoieqwoiuqwe", "", "", "ajshdjhad", "", "", "", "", "", "iqpieqepqiepqw").map(e => Map("secondFeedData" -> e)).array.circular
  val appIdFeeder = Array("loadtest1", "loadtest2", "loadtest3", "loadtest4", "loadtest5", "loadtest6", "loadtest7", "loadtest8", "loadtest9", "loadtest10", "loadtest11", "loadtest12", "loadtest13", "loadtest14", "loadtest15", "loadtest16", "loadtest17", "loadtest18", "loadtest19",
    "loadtest20", "loadtest21", "loadtest22", "loadtest23", "loadtest24", "loadtest25", "loadtest26", "loadtest27", "loadtest28", "loadtest29", "loadtest30", "loadtest31", "loadtest32", "loadtest33", "loadtest34", "loadtest35", "loadtest36", "loadtest37", "loadtest38", "loadtest39", "loadtest40",
    "loadtest41", "loadtest42", "loadtest43", "loadtest44", "loadtest45", "loadtest46", "loadtest47", "loadtest48", "loadtest49", "loadtest50").map(e => Map("appId" -> e)).array.circular


  val randomFeeder = Array(randomString).map(e => Map("randomFeedData" -> e)).array.circular

  private val createScheduleLoad = scenario("Create Schedule API 5 minute")
    .feed(firstFeeder)
    .feed(secondFeeder)
    .exec(session => session.set("scheduleTime", scheduleTime))
    .exec(http("Create Schedule API 5 minute")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(
        """{"appId": "YantraLoadTest","payload": "{\"planId\":6, \"role\":\"${firstFeedData}\", \"delistingEvent\": \"${firstFeedData}\",\"event\": \"${secondFeedData}\", \"type\" : \"${secondFeedData}\"}","scheduleTime": ${scheduleTime},"httpCallback": {
		"url": "http://google.com",
		"headers": {
			"Content-Type": "application/json"
		}
	}}""")).asJson
      .check()
      .check(status.is(200)))


  private val createScheduleLoadWithTime = scenario("Create Schedule API With Time")
    .feed(firstFeeder)
    .feed(secondFeeder)
    .exec(session => session.set("sTime", sTime))
    .exec(http("Create Schedule API")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(
        """{"appId": "YantraLoadTest","payload": "{\"planId\":6, \"role\":\"${firstFeedData}\", \"delistingEvent\": \"${firstFeedData}\",\"event\": \"${secondFeedData}\", \"type\" : \"${secondFeedData}\"}","scheduleTime": ${sTime},"httpCallback": {
		"url": "http://google.com",
		"headers": {
			"Content-Type": "application/json"
		}
	}}""")).asJson
      .check()
      .check(status.is(200)))


  private val createScheduleWith50AppsLoad = scenario("Create Schedule With 50 APPS")
    .feed(firstFeeder)
    .feed(appIdFeeder)
    .feed(secondFeeder)
    .exec(session => session.set("sTime", sTime))
    .exec(http("Create Schedule With 50 APPS")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(
        """{"appId": "${appId}","payload": "{\"planId\":6, \"role\":\"${firstFeedData}\", \"delistingEvent\": \"${firstFeedData}\",\"event\": \"${secondFeedData}\", \"type\" : \"${secondFeedData}\"}","scheduleTime": ${sTime},"httpCallback": {
		"url": "http://google.com",
		"headers": {
			"Content-Type": "application/json"
		}
	}}""")).asJson
      .check()
      .check(status.is(200)))


  private val createScheduleWithAirbusCallbackWith256Kbpayload = scenario("Create Schedule With Airbus 256Kb payload")
    .feed(firstFeeder)
    .feed(appIdFeeder)
    .feed(secondFeeder)
    .feed(randomFeeder)
    .exec(session => session.set("sTime", sTime))
    .exec(http("Create Schedule With Airbus Callback 256Kb")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(
        """{"appId": "YantraLoadTest","payload": "${randomFeedData}","scheduleTime": ${sTime},"airbusCallback": {
		"eventName": "IntegrationTest",
		"headers": {
			"Content-Type": "application/json"
		}
	}}""")).asJson
      .check()
      .check(status.is(200)))





  private val createScheduleWithAirbusCallbackWithXBpayloadCallbackAfterXMinutes =
    scenario("Create Schedule With Airbus 1 app and xB payload and callback after x minutes")
    .exec(http("Create Schedule With Airbus Callback random time")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(session =>
        s"""
          |{
          |  "appId": "YantraLoadTest",
          |  "payload": "${randomStringGenerator.randomString(payloadSize)}",
          |  "scheduleTime": ${callbackOffsetTime(airbusCallbackOffsetTime)},
          |  "airbusCallback": {
          |    "eventName": "MyssAirbusLoadTest",
          |    "headers": {
          |      "Content-Type": "application/json"
          |    }
          |  }
          |}
          |""".stripMargin)).asJson
      .check()
      .check(status.is(200)))



  private val createScheduleWithAirbusCallbackWith1KbpayloadCallbackAfterXMinutes =
    scenario("Create Schedule With Airbus 1 app and 1Kb payload and callback after x minutes")
    .exec(http("Create Schedule With Airbus Callback random time")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(session =>
        s"""
          |{
          |  "appId": "YantraLoadTest",
          |  "payload": "${randomStringGenerator.randomString(1024)}",
          |  "scheduleTime": ${callbackOffsetTime(airbusCallbackOffsetTime)},
          |  "airbusCallback": {
          |    "eventName": "MyssAirbusLoadTest",
          |    "headers": {
          |      "Content-Type": "application/json"
          |    }
          |  }
          |}
          |""".stripMargin)).asJson
      .check()
      .check(status.is(200)))


  private val createScheduleWithAirbusCallbackWith1KbpayloadFixedCallbackTime =
    scenario("Create Schedule With Airbus 1 app and 1Kb payload and fixed callback time")
    .exec(http("Create Schedule With Airbus Callback fixed time")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(session =>
        s"""
          |{
          |  "appId": "YantraLoadTest",
          |  "payload": "${randomStringGenerator.randomString(1024)}",
          |  "scheduleTime": ${callbackOffsetTime(airbusCallbackOffsetTime)},
          |  "airbusCallback": {
          |    "eventName": "MyssAirbusLoadTest",
          |    "headers": {
          |      "Content-Type": "application/json"
          |    }
          |  }
          |}
          |""".stripMargin)).asJson
      .check()
      .check(status.is(200)))


   private val createScheduleWithAirbusCallbackWithGivenpayloadAndAppNameFixedCallbackTime =
     scenario("Create Schedule With Airbus given app and given payload and fixed callback time")
    .exec( session => session.set("appName", appName))
    .exec(http("Create Schedule With Airbus Callback fixed time")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(session =>
        s"""
          |{
          |  "appId": "${appName}",
          |  "payload": "${randomStringGenerator.randomString(payloadSize)}",
          |  "scheduleTime": ${callbackOffsetTime(airbusCallbackOffsetTime)},
          |  "airbusCallback": {
          |    "eventName": "MyssAirbusLoadTest",
          |    "headers": {
          |      "Content-Type": "application/json"
          |    }
          |  }
          |}
          |""".stripMargin)).asJson
      .check()
      .check(status.is(200)))


    private val createAndDeleteScheduleInFixedRatioWithAirbusCallbackWithGivenPayloadAndRandomScheduleTimeIn =
      scenario("Create and Delete Schedule in 3:1 ratio with Airbus given app and given payload and callback after x minutes")
      .exec(http("Create Schedule1 With Airbus Callback random time")
        .post("/myss/schedule")
        .headers(headers)
        .headers(poheaders)
        .body(StringBody(session =>
          s"""
             |{
             |  "appId": "YantraLoadTest",
             |  "payload": "${randomStringGenerator.randomString(payloadSize)}",
             |  "scheduleTime": ${callbackOffsetTime(airbusCallbackOffsetTime)},
             |  "airbusCallback": {
             |    "eventName": "MyssAirbusLoadTest",
             |    "headers": {
             |      "Content-Type": "application/json"
             |    }
             |  }
             |}
             |""".stripMargin)).asJson
        .check()
        .check(status.is(200))
        .check(jsonPath("$.data.schedule.scheduleId").saveAs("scheduleId"))
      ).exec(http("Create Schedule2 With Airbus Callback random time")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(session =>
        s"""
          |{
          |  "appId": "YantraLoadTest",
          |  "payload": "${randomStringGenerator.randomString(payloadSize)}",
          |  "scheduleTime": ${callbackOffsetTime(airbusCallbackOffsetTime)},
          |  "airbusCallback": {
          |    "eventName": "MyssAirbusLoadTest",
          |    "headers": {
          |      "Content-Type": "application/json"
          |    }
          |  }
          |}
          |""".stripMargin)).asJson
      .check()
      .check(status.is(200))
    ).exec(http("Create Schedule3 With Airbus Callback random time")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(session =>
        s"""
          |{
          |  "appId": "YantraLoadTest",
          |  "payload": "${randomStringGenerator.randomString(payloadSize)}",
          |  "scheduleTime": ${callbackOffsetTime(airbusCallbackOffsetTime)},
          |  "airbusCallback": {
          |    "eventName": "MyssAirbusLoadTest",
          |    "headers": {
          |      "Content-Type": "application/json"
          |    }
          |  }
          |}
          |""".stripMargin)).asJson
      .check()
      .check(status.is(200))
    ).pause(pauseTime)
        .exec(http("Delete one schedule")
          .delete("/myss/schedule/${scheduleId}")
          .headers(headers)
          .headers(poheaders)
          .check()
          .check(status.is(200)))


  private val createScheduleWithMixedCallbacks =
    scenario("Create Schedule with 1:1 http and airbus callbacks")
    .exec(http("Create Schedule1 with Airbus Callback with callback offset time")
      .post("/myss/schedule")
      .headers(headers)
      .headers(poheaders)
      .body(StringBody(session =>
      s"""{
        |  "appId": "YantraLoadTest",
        |  "payload": "${randomStringGenerator.randomString(payloadSize)}",
        |  "scheduleTime": ${callbackOffsetTime(airbusCallbackOffsetTime)},
        |  "airbusCallback": {
        |    "eventName": "MyssAirbusLoadTest",
        |    "headers": {
        |      "Content-Type": "application/json"
        |    }
        |  }
        |}""".stripMargin)).asJson
      .check()
      .check(status.is(200))
    ).exec(http("Create Schedule2 with Http Callback with callback offset time")
    .post("/myss/schedule")
    .headers(headers)
    .headers(poheaders)
    .body(StringBody(session =>
      s"""{
        |  "appId": "YantraLoadTest",
        |  "payload": "${randomStringGenerator.randomString(payloadSize)}",
        |  "scheduleTime": ${callbackOffsetTime(httpCallbackOffsetTime)},
        |  "httpCallback": {
        |    "url": "http://google.com",
        |    "headers": {
        |      "Content-Type": "application/json"
        |    }
        |  }
        |}
        |""".stripMargin)).asJson
    .check()
    .check(status.is(200)))



  setUp(
    scenarios map (
      e => e.inject(
        rampUsersPerSec(1) to noOfUsers
          during ((duration * rampUpPercentage) / 100),
        constantUsersPerSec(noOfUsers)
          during ((duration * (100 - rampUpPercentage)) / 100)
      )
      )
  ).maxDuration(maxDurationInSec).protocols(myssBaseUrl)

}