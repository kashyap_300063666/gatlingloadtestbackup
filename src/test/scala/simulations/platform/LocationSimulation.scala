package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.math._


///myntra-location-service/location/ip/${ip}
//myntra-location-service/location/city/${city}


class LocationSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "ips.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

//  val citiesFileLocation = System.getProperty("citiesfilelocation", "/Users/11151/Downloads/cities.txt").toString
  //val ipsFileLocation = System.getProperty("ipsfilelocation", "test-data/ips.csv").toString
  val ipsFileLocation = System.getProperty("ipsfilelocation", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, ipsFileLocation)
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.LOCATION_SERVICE.toString)
  private val locationServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  val r = scala.util.Random

//  val lines = readFile(citiesFileLocation)
  val linesIps = readFile(filePath)

  private val api1 = scenario("location service ip test")
    .exec(session => session.set("ip", linesIps(r.nextInt(1000000))))
    .exec(http("location service ip test")
      .get("/myntra-location-service/location/ip/${ip}")
      .headers(headers)
      .check(status.is(200))
    )

//  private val api2 = scenario("location service city test")
//    .exec(session => session.set("city", lines(r.nextInt(2000))))
//    .exec(http("location service city test")
//      .get("/myntra-location-service/location/city/${city}")
//      .headers(headers)
//      .check(status.is(200))
//      .check(jsonPath("$.status.statusCode").is("2"))
//    )

    
     setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec)
    .protocols(locationServiceBaseUrl)
 
}

/*
  Command for the simulation.
  Refresh token is called 19 times with the token afrer login

  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -Dfilter.scenarios="location" -Dcitiesfilelocation="/Users/11151/Downloads/cities.txt" -jar inbound-performance-test-all.jar -s simulations.platform.LocationSimulation  -df . -rf .
  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -Dfilter.scenarios="location" -Dcitiesfilelocation="/home/guest/perf/cities.txt" -jar inbound-performance-test-all.jar -s simulations.platform.LocationSimulation  -df . -rf .
 */
