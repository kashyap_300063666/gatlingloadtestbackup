package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation
import scala.math._
import java.io._

import java.io.File
import java.io.PrintWriter
import java.io.FileOutputStream



//auth/mobile/securerefresh
//auth email
//profile/uidx/{app}/{uidx}
//profile/uidx/{uidx}


class IdeaSimulation_MOA extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "userfile.csv"
  val fileNameUidx = "uidxlist1.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }


  val secureRefreshCallPerUser = System.getProperty("securerefresh.call", "1.5").toInt // rattio of refresh vs login is 19:1
  val ignoreCache = System.getProperty("ignore.cache", "true").toString
  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/uidxlist1.csv").toString
  val uidxFileLocation = System.getProperty("uidxFileLocation", fileNameUidx).toString

  val loginUsers = System.getProperty("users.count", "1").toInt
  val idStart = System.getProperty("start.id", "1").toInt
  val idEnd = System.getProperty("end.id", "1").toInt
  //val csvFile = System.getProperty("uidxFileLocation", "test-data/userfile.csv").toString
  val csvFile = System.getProperty("uidxFileLocation", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  val getUsersByUidx = loginUsers * 3 // getUserByUidx call to login ration is 1.7:1
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.IDEA_SERVICE.toString)
  private val ideaServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  val r = scala.util.Random


  var ids = (idStart to idEnd).iterator
  val currId = Iterator.continually(Map("currId" -> ids.next()))
  private val linkPhoneLogin = feed(currId)
    .exec(http("send OTP test")
      .post("/idea/opt/auth/v2/phone/sendotp")
      .body(StringBody("""{"otp":"1234","phoneNo":"111${currId}"}""")).asJson
      .headers(headers)
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("3"))
      .check(jsonPath("$.urt").saveAs("urt"))
    ).exec(http("verify OTP test")
      .post("/idea/opt/auth/v2/phone")
      .body(StringBody("""{"otp":"1234","urt":"${urt}"}""")).asJson
      .headers(headers)
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("3"))
      .check(jsonPath("$.urt").saveAs("urt"))
    ).exec(http("idea login test with associated phone no")
      .post("/idea/opt/auth/v2/email")
      .body(StringBody("""{"emailId":"${currId}@myntra360.com","accessKey":"${currId}@myntra360.com","urt":"${urt}"}""")).asJson
      .headers(headers)
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("3"))
      .check(jsonPath("$.tokenEntry.at").saveAs("at"))
      .check(jsonPath("$.tokenEntry.rt").saveAs("rt"))
    )

//  val file = new File("/Users/300066992/Desktop/numbersIDs.csv")
//  val bw = new BufferedWriter(new FileWriter(file))




  //  scala.tools.nsc.io.File("../user-files/data/refenceId.csv").appendAll(session("refenceId").as[String]+"\n")
  //  session}

  //val sendOtp = exec(session => session.set("PhoneNo", "1111" + r.nextInt((260000-100000)+1)))
  //    .exec(
  //      session => {
  //        //val id = session("PhoneNo").as[String]
  //        bw.write(session("PhoneNo").as[String])
  //        session
  //      }
  //    )

//  var PhoneNumbers = (1110000001 to 1110000020).iterator
//  val CustomFeeder1 = Iterator.continually(Map("PhoneNo" -> PhoneNumbers.next()))

  val csvfeeder = csv(filePath).circular

  val sendOtp =
    feed(csvfeeder).
      exec(http("send OTP test")
        .post("/idea/opt/auth/v2/phone/sendotp")
        .body(StringBody("""{"phoneNo":"${PhoneNo}"}""")).asJson
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.urt").saveAs("urt"))
        //   .check(jsonPath("$.tokenEntry.rt").saveAs("rt")
      )

////      // def verifyOTP() = {
      .exec(http("verify OTP test")
        .post("/idea/opt/auth/v2/phone")
        .body(StringBody("""{"otp":"1234","phoneNo":"${PhoneNo}","urt":"${urt}"}""")).asJson
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.tokenEntry.urt").is("${urt}"))
        //  .check(jsonPath("$.tokenEntry.rt").saveAs("rt"))

      )


  var idNumbers2 = (20 to 40).iterator
  val customFeeder = Iterator.continually(Map("emailId" -> idNumbers2.next()))
 // val EmailLogin = exec(session => session.set("emailId", r.nextInt(5000)+"@myntra360.com"))

   val EmailLogin =
     feed(customFeeder).
    exec(http("idea login test")
      .post("/idea/opt/auth/email")
      .body(StringBody("""{"emailId":"${emailId}@myntra360.com"+,"accessKey":"${emailId}@myntra360.com","appName":"myntra"}""")).asJson
      .headers(headers)
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("3"))
      .check(jsonPath("$.tokenEntry.at").saveAs("at"))
      .check(jsonPath("$.tokenEntry.rt").saveAs("rt"))
    )


  //}
//    //def MobileSecureRefresh()= {
//    .exec(http("idea mobile refresh")
//      .post("/idea/opt/auth/mobile/securefresh?ic="+ignoreCache)
//      .headers(headers)
//      .body(StringBody("""{"rt":${rt}}""")).asJson
//      .check(status.is(200)))
//    //}

  var idNumbers3 = (40 to 60).iterator
  val customFeeder3 = Iterator.continually(Map("idId" -> idNumbers3.next()))
  private val loginIdea = feed(customFeeder3).
    exec(http("idea login test without associated phone no")
      .post("/idea/opt/auth/email")
      .body(StringBody("""{"emailId":"${idId}@myntra360.com","accessKey":"${idId}@myntra360.com","appName":"myntra"}""")).asJson
      .headers(headers)
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("3"))
      .check(jsonPath("$.tokenEntry.at").saveAs("at"))
      .check(jsonPath("$.tokenEntry.rt").saveAs("rt"))
    )


  val OTPlogin = scenario("send and verify OTP").exec(sendOtp)
  val emailLoginwithPhoneNo = scenario("email login with associated phone No").exec(EmailLogin)

  val emailonlyLogin = scenario("email only login").exec(loginIdea)
  val linkPhoneAndLogin = scenario("aug data test").exec(linkPhoneLogin)






  //val SecureRefresh = scenario("Mobile secure Refresh").exec(MobileSecureRefresh())
  //
//  private val mobileRefreshChain = repeat(secureRefreshCallPerUser) {
//    exec(http("idea mobile refresh")
//      .post("/idea/opt/auth/mobile/securefresh?ic=" + ignoreCache)
//      .headers(headers)
//      .body(StringBody("""{"rt":"${rt}"}""")).asJson
//      .check(status.is(200)))
//  }
  //
  //private val mobileRefresh = exec(mobileRefreshChain)
  //  private val loginIdeaAndMoobileRefresh = Seq(OTPlogin,mobileRefresh)
  //
  // // private val loginIdeaTest = scenario("idea login test").exec(loginIdeaAndMoobileRefresh)

//  val lines = readFile(uidxFileLocation)
//
//  private val ideafetchUidxTest = scenario("idea fetch by uidx")
//    .exec(session => session.set("id", lines(r.nextInt(300000))))
//    .exec(http("idea get by uidx")
//      .get("/idea/opt/profile/uidx/myntra/${id}?ic=" + ignoreCache)
//      .headers(headers)
//      .check(status.is(200)))

 // bw.close()


  //  val list = scenarios map (e => e.inject(step).protocols())
  //  var toFilterScenariosList = getProperty("filter.scenarios").split(",") map (e => e.trim.toLowerCase) toList;
  //  var newList = list;
  //  if(toFilterScenariosList.contains(ideafetchUidxTest.name.toLowerCase)) {
  //    newList = ideafetchUidxTest.inject(constantUsersPerSec(floor(getUsersByUidx)) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized) :: list
  //  }

  //  else if(toFilterScenariosList.contains(SecureRefresh.name.toLowerCase)) {
  //    newList = SecureRefresh.inject(step)  }

  //  setUp(scenarios map (e => e.inject(step)) toList)
  //    .protocols(ideaServiceBaseUrl)

  //

  val otpcount = loginUsers/3
  val emaillogincount = (loginUsers)/3
  setUp(
//    OTPlogin.inject(constantUsersPerSec(otpcount) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized).protocols(ideaServiceBaseUrl),
//    emailLoginwithPhoneNo.inject(constantUsersPerSec(emaillogincount) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized).protocols(ideaServiceBaseUrl),
//    emailonlyLogin.inject(constantUsersPerSec(emaillogincount) during (DurationJLong(getLongProperty("duration.seconds"))seconds)randomized).protocols(ideaServiceBaseUrl),
    linkPhoneAndLogin.inject(constantUsersPerSec(loginUsers) during(DurationJLong(getLongProperty("duration.seconds"))seconds)randomized).protocols(ideaServiceBaseUrl)
  )
}

/*
  Command for the simulation.
  Refresh token is called 19 times with the token afrer login

  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=300 -Dduration.seconds=60 -Dfilter.scenarios="idea" -Duidxfilelocation="/Users/11151/Downloads/uidxlist.txt" -jar inbound-performance-test-all.jar -s simulations.platform.IdeaSimulation  -df . -rf .
 */
