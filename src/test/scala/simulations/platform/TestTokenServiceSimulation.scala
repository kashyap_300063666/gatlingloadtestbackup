package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation


class TestTokenServiceSimulation extends BaseSimulation {
  private val tokenBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MYNTOS.toString)).disableWarmUp

  private val poheaders = Map("cache-control" -> "no-cache", "accept" -> "application/json", "Content-Type" -> "application/json")

  private val tokenServiceLoad =
    scenario("get token service")
      .exec(http("fetch token service")
        .post("/tokenservice/token/")
        .headers(headers)
        .headers(poheaders)
        .body(StringBody("""{"clientId": "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61"}""")).asJson
        .check(status.is(200))
        .check(jsonPath("$.status.statusMessage").is("Token Created Successfully")))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(tokenBaseUrl)

}