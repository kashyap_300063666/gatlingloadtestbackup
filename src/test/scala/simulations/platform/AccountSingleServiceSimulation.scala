package simulations.platform

import com.myntra.commons.util.FeederUtil
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

import scala.util.Random

/**
  * @author Abhishyam.c on 08/12/20
  */
class AccountSingleServiceSimulation extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "AccountServiceUsers.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromResource(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  //Data files
  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/AccountServiceUsers.csv").toString

  //val getByUidxCsvfeeder = csv(uidxF`ileLocation).circular

  val uidxFileLocation = System.getProperty("uidxFileLocation", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  val getByUidxCsvfeeder = csv(filePath).circular

  //val getByUidxCsvfeeder: BatchableFeederBuilder[String] = feederUtil.getFeeder(containerName, fileName).circular



  val accountServiceBaseUrl = System.getProperty("baseUrl", "http://10.162.171.26:7777")
  val httpProtocol = http
    .baseUrl(accountServiceBaseUrl)


   val ideafetchUidx =
    scenario("getByUidx")
      .feed(getByUidxCsvfeeder)
      .exec(http("Account service get by uidx")
        .get("/api/v1/accounts/users/uidx/${id}")
        .headers(headers)
        .header("clientId", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
        .header("tenantId","1")
        .check(status.is(200)))



   val loginEmail =
    scenario("loginByEmail")
    .exec(session => session.set("emailId", Random.nextInt(1600000)+1 + "@myntra360.com"))
    .exec(http("Account Service Login Email")
      .post("/api/v1/accounts/auth/email")
      .body(StringBody("""{"email":"${emailId}","password":"${emailId}"}""")).asJson
      .headers(headers)
      .header("clientId", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
      .header("tenantId","1")
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("3"))
      .check(jsonPath("$.token.at").saveAs("at"))
      .check(jsonPath("$.token.rt").saveAs("rt"))
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}