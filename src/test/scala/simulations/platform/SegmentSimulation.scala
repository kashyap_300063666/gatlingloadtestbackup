package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

import scala.math._


class SegmentSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileNameUidxFileLocation = "uidxlist1.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }


  val namespace = "personify"
  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/uidxlist1.csv").toString
  val uidxFileLocation = System.getProperty("uidxFileLocation", fileNameUidxFileLocation).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  val baseUrl = System.getProperty("baseUrl", "http://segment-service.myntra.com")

  private val segmentServiceBaseUrl = http.baseUrl(baseUrl).shareConnections.disableWarmUp
  val r = scala.util.Random
  val lines = readFile(filePath)

  private val api1 = scenario("Segment service GET segments test")
    .exec(session => session.set("uidx", lines(r.nextInt(10000))))
    .exec(http("Segment service GET segments test")
      .post("/api/v1/user_segments/get/${uidx}")
      .body(StringBody("""{"namespace":["personify", "cohort"]}""")).asJson
      .headers(headers)
      .check(status.is(200))
    )

  private val api2 = scenario("Segment service GET METADATA test")
    .exec(session => session.set("metadataId", lines(r.nextInt(200))))
    .exec(http("Segment service GET METADATA test")
      .get("/api/v1/get/metadata/${metadataId}")
      .headers(headers)
      .check(status.is(200))
    )

  private val api3 = scenario("Segment service GET CURRENT SYNC COUNT test")
    .exec(session => session.set("count", r.nextInt(200)))
    .exec(http("Segment service GET CURRENT SYNC COUNT test")
      .get("/api/v1/get/current/count/${count}")
      .headers(headers)
      .check(status.is(200))
    )

  private val api4 = scenario("Segment Service GET Complete Profile Vector")
      .exec(session => session.set("uidx", lines(r.nextInt(300000))))
      .exec(http("Segment Service GET Complete Profile Vector")
          .post("/api/v2/vector/get/${uidx}")
          .body(StringBody("""{"vectors": [ "profile"]}""")).asJson
          .headers(headers)
          .check(status.in(200,400))
      )

  private val api5 = scenario("Segment Service GET Complete Segment Vector")
    .exec(session => session.set("uidx", lines(r.nextInt(300000))))
    .exec(http("Segment Service GET Complete Segment Vector")
      .post("/api/v2/vector/get/${uidx}")
      .body(StringBody("""{"vectors": [ "segment"]}""")).asJson
      .headers(headers)
      .check(status.in(200,400))
    )

  private val api6 = scenario("Segment Service GET Vector With Dimension")
    .exec(session => session.set("uidx", lines(r.nextInt(300000))))
    .exec(http("Segment Service GET Complete Segment Vector")
      .post("/api/v2/vector/dimensions/get/${uidx}")
      .body(StringBody("""{"vectors": [{"name": "segment","dimensions" : ["personify","cohort"]},{"name" : "profile","dimensions" : ["gender","registrationOn"]}]}""".stripMargin)).asJson
      .headers(headers)
      .header("Host","segment-service.myntra.com")
      .check(status.in(200,400))
    )
     setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(segmentServiceBaseUrl)
    
}

/*
  Command for the simulation.

  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -Dfilter.scenarios="segment" -DuidxFileLocation="/Users/11151/Downloads/uidxlist.txt" -DsegmentValuesFileLocation="/Users/11151/Downloads/segmentValues.txt" -jar inbound-performance-test-all.jar -s simulations.platform.SegmentSimulation  -df . -rf .
  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -Dfilter.scenarios="segment" -DuidxFileLocation="/home/guest/perf/uidx.txt" -DsegmentValuesFileLocation="/home/guest/perf/segmentValues.txt" -jar inbound-performance-test-all.jar -s simulations.platform.SegmentSimulation  -df . -rf .
 */