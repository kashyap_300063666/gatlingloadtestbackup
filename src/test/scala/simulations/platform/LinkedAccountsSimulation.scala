package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

/*
/v1/uidx/${uidx}
/v2/uidx/${uidx}
/v1/attributes
/v2/attributes
/v1/order-delivered
/v2/order-delivered
 */
class LinkedAccountsSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileNameUidxFileLocation2 = "linkedAccountsUidx2.csv"
  val fileNameUidxFileLocation = "linkedAccountsUidx.csv"
  val fileNameDeviceFileLocation = "linkedAccountsDevice.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  val uidxFileLocation = System.getProperty("uidxFileLocation", fileNameUidxFileLocation)
  val filePathUidxFileLocation: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  val deviceFileLocation = System.getProperty("deviceFileLocation", fileNameDeviceFileLocation)
  val filePathDeviceFileLocation: String = feederUtil.getFileDownloadedPath(containerName, deviceFileLocation)

  val uidxFileLocation2 = System.getProperty("deviceUidxFileLocation", fileNameUidxFileLocation2)
  val filePathUidxFileLocation2: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  val linkedAccountsServiceBaseUrl = System.getProperty("baseUrl", "http://linkedaccounts.myntra.com")

  val httpProtocol = http
    .baseUrl(linkedAccountsServiceBaseUrl)

  val r = scala.util.Random

  val linesUidx = readFile(filePathUidxFileLocation)
  val linesDevice = readFile(filePathDeviceFileLocation)
  val linesUidx2 = readFile(filePathUidxFileLocation2)

  private val getLinkedAccountsForUserV1 =scenario("getLinkedAccountsForUserV1")
    .exec(session => session.set("uidx", linesUidx(r.nextInt(500000))))
    .exec(http("getLinkedAccountsForUserV1")
      .get("/v1/uidx/${uidx}")
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val getLinkedAccountsForUserV2 =scenario("getLinkedAccountsForUserV2")
    .exec(session => session.set("uidx", linesUidx(r.nextInt(500000))))
    .exec(http("getLinkedAccountsForUserV2")
      .get("/v2/uidx/${uidx}")
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val getLinkedAccountsForUserV2Heavy =scenario("getLinkedAccountsForUserV2Heavy")
    .exec(session => session.set("uidx", linesUidx2(r.nextInt(500000))))
    .exec(http("getLinkedAccountsForUserV2Heavy")
      .get("/v2/uidx/${uidx}")
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val getLinkedAccountsForDeviceV1 =scenario("getLinkedAccountsForDeviceV1")
    .exec(session => session.set("deviceId", linesDevice(r.nextInt(500000))))
    .exec(http("getLinkedAccountsForDeviceV1")
      .post("/v1/attributes")
      .body(StringBody("""{"deviceid":"${deviceId}"}""")).asJson
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val getLinkedAccountsForDeviceV2 =scenario("getLinkedAccountsForDeviceV2")
    .exec(session => session.set("deviceId", linesDevice(r.nextInt(500000))))
    .exec(http("getLinkedAccountsForDeviceV2")
      .post("/v2/attributes")
      .body(StringBody("""{"deviceid":"${deviceId}"}""")).asJson
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val checkOrderDeliveredV1 =scenario("checkOrderDeliveredV1")
    .exec(session => session.set("uidx", linesUidx(r.nextInt(500000))))
    .exec(session => session.set("deviceId", linesDevice(r.nextInt(500000))))
    .exec(http("checkOrderDeliveredV1")
      .post("/v1/order-delivered")
      .body(StringBody("""{"uidx": "${uidx}", "deviceid":"${deviceId}"}""")).asJson
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val checkOrderDeliveredV2ByVertexAndEdge =scenario("checkOrderDeliveredV2ByVertexAndEdge")
    .exec(session => session.set("uidx", linesUidx(r.nextInt(500000))))
    .exec(session => session.set("deviceId", linesDevice(r.nextInt(500000))))
    .exec(http("checkOrderDeliveredV2")
      .post("/v2/order-delivered")
      .body(StringBody("""{"uidx": "${uidx}", "deviceid":"${deviceId}"}""")).asJson
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val checkOrderDeliveredV2ByEdge =scenario("checkOrderDeliveredV2ByEdge")
    .exec(session => session.set("uidx", linesUidx2(r.nextInt(500000))))
    .exec(session => session.set("deviceId", linesDevice(r.nextInt(500000))))
    .exec(http("checkOrderDeliveredV2ByEdge")
      .post("/v2/order-delivered/byEdge")
      .body(StringBody("""{"uidx": "${uidx}", "deviceid":"${deviceId}"}""")).asJson
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val checkOrderDeliveredV2ByStrinMatch =scenario("checkOrderDeliveredV2ByStringMatch")
    .exec(session => session.set("uidx", linesUidx2(r.nextInt(500000))))
    .exec(session => session.set("deviceId", linesDevice(r.nextInt(500000))))
    .exec(http("checkOrderDeliveredV2ByStringMatch")
      .post("/v2/order-delivered/byStringMatch")
      .body(StringBody("""{"uidx": "${uidx}", "deviceid":"${deviceId}"}""")).asJson
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )


  private val isNewUserV2 =scenario("isNewUserV2")
    .exec(session => session.set("uidx", linesUidx(r.nextInt(500000))))
    .exec(http("isNewUserV2")
      .get("/v2/user/${uidx}/isNew")
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val isNewDeviceV2 =scenario("isNewDeviceV2")
    .exec(session => session.set("deviceId", linesDevice(r.nextInt(500000))))
    .exec(http("isNewDeviceV2")
      .get("/v2/device/${deviceId}/isNew")
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val isNewDeviceOrUserV2 =scenario("isNewDeviceOrUserV2")
    .exec(session => session.set("deviceId", linesDevice(r.nextInt(500000))))
    .exec(session => session.set("uidx", linesUidx(r.nextInt(500000))))
    .exec(http("isNewDeviceOrUserV2")
      .get("/v2/user/${uidx}/device/${deviceId}/isNew")
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  private val readAPIsV2 = Seq(isNewUserV2, isNewDeviceV2, isNewDeviceOrUserV2, checkOrderDeliveredV2ByVertexAndEdge)
  private val readOnlyScenario = scenario("readOnlyScenarioV2").exec(readAPIsV2)

  setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)

}

/*
  Command for the simulation.
  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -jar inbound-performance-test-all.jar -s simulations.platform.LinkedAccountsSimulation  -df . -rf .
 */