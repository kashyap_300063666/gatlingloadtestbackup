package simulations.gauntlet

import com.github.phisgr.gatling.grpc.Predef.{$, grpc}
import io.gatling.core.Predef.{during, _}
import simulations.BaseSimulation
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.gauntlet.domain.maximus.{StyleRequest, _}
import io.gatling.core.session.Expression
import io.grpc.{ManagedChannelBuilder, Metadata}

class GauntletPdpGrpcSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "revenue"
  /* Name of File to be downloaded */
  val fileName = "gauntletStyles1.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val baseUrl = System.getProperty("singleServerURL") //Pass the complete url of single server if req
  private val headerValue = System.getProperty("headerGauntlet","")
  private val storeId = System.getProperty("storeId","storeid=2297")
  var grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.GAUNTLET.toString, ""), grpcPortNum).usePlaintext())

  if (baseUrl != null) {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(baseUrl, grpcPortNum).usePlaintext())
  }
  println("Updated BaseUrl:" + BaseUrlConstructor.getBaseUrl(ServiceType.GAUNTLET.toString, ""))

  val styleIdFeeder = csv(filePath).random.convert {
    case ("styleId", string) => string.toLong
  }

  def seqOfStyleRequest(session: Session): StyleRequest = {
    val sr: StyleRequest = StyleRequest(styleId = Option(session("styleId").as[Long]))
    sr
  }

  val dynamicPayload: Expression[StyleRequest] = session => seqOfStyleRequest(session)

  val pdp = scenario("gauntletPdpGrpc")
    .feed(styleIdFeeder)
    .exec(grpc("gauntletPdpGrpc - discountDetails")
      .rpc(GauntletInfoGrpc.METHOD_DISCOUNT_DETAILS)
      .payload(dynamicPayload)
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))(storeId)
      .header(Metadata.Key.of("x-mynt-client-id", Metadata.ASCII_STRING_MARSHALLER))(headerValue)
    )

  setUp(scenarios map (e => e.inject(rampUsersPerSec(getLongProperty("ramp_up_rate.start"))
    to getLongProperty("ramp_up_rate.end")
    during (DurationJLong(getLongProperty("ramp_duration")) seconds),
    constantUsersPerSec(getLongProperty("hold_rate"))
      during (DurationJLong(getLongProperty("hold_duration")) seconds) )))
    .maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}