package simulations.gauntlet

import com.github.phisgr.gatling.grpc.Predef.grpc
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.gauntlet.domain.cart.{ApplyDiscountRequest, CartItem, GauntletGrpc}
import io.gatling.core.Predef._
import io.gatling.core.session.Expression
import io.grpc.{ManagedChannelBuilder, Metadata}
import simulations.BaseSimulation

class GauntletApplyGrpcSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "revenue"
  /* Name of File to be downloaded */
  val fileName = "gauntletApply1.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val baseUrl = System.getProperty("singleServerURL") //Pass the compl
  // ete url of single server if req
  private val headerValue = System.getProperty("headerGauntlet", "")
  private val storeId = System.getProperty("storeId","storeid=2297")

  var grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor
    .getBaseUrl(ServiceType.GAUNTLET.toString, ""), grpcPortNum).usePlaintext())

  if (baseUrl != null) {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(baseUrl, grpcPortNum).usePlaintext())
    println("Updated BaseUrl:" + baseUrl)
  }

  //val styleIdFeeder = csv("test-data/gauntletApply1.csv").random
  val styleIdFeeder = csv(filePath).random

  val dist = Map("1" -> 0.6217, "2" -> 0.1443, "3" -> 0.0627, "4" -> 0.0381, "5" -> 0.0227, "6" -> 0.0184, "7" -> 0.0142
    , "8" -> 0.0122, "9" -> 0.0103, "10" -> 0.0087, "15" -> 0.0382, "25" -> 0.0056, "35" -> 0.0014, "45" -> 0.0014)

  final def sample[A](dist: Map[A, Double]): String = {
    val p = scala.util.Random.nextDouble
    val it = dist.iterator
    var accum = 0.0
    while (it.hasNext) {
      val (item, itemProb) = it.next
      accum += itemProb
      if (accum >= p) {
        return item.toString
      }
    }
    "1"
  }

  def seqOfCartItems(session: Session): Seq[CartItem] = {
    var seq: Seq[CartItem] = Seq[CartItem]()
    val noOfCartItems: Int = sample(dist).toInt
    for (i <- 1 to noOfCartItems) {
      val ci: CartItem = CartItem(styleId = Option(session("styleId" + i).as[Long]),
        sellerPartnerId = Option(session("sellerPartnerId" + i).as[Long]),
        skuId = Option(session("skuId" + i).as[Long]),
        quantity = Option(1)
      )
      seq = seq :+ ci
    }
    seq
  }

  val dynamicPayload: Expression[ApplyDiscountRequest] = session => ApplyDiscountRequest(cartItem = seqOfCartItems(session))

  val apply = scenario("Gauntlet - ApplyDiscount")
    .feed(styleIdFeeder, 50)
    .exec(grpc("Gauntlet - ApplyDiscountRequest")
      .rpc(GauntletGrpc.METHOD_APPLY)
      .payload(dynamicPayload)
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))(storeId)
      .header(Metadata.Key.of("x-mynt-client-id", Metadata.ASCII_STRING_MARSHALLER))(headerValue)
    )

  setUp(scenarios map (e => e.inject(rampUsersPerSec(getLongProperty("ramp_up_rate.start"))
    to getLongProperty("ramp_up_rate.end")
    during getIntProperty("ramp_duration"),
    constantUsersPerSec(getIntProperty("hold_rate"))
      during getIntProperty("hold_duration"))))
    .maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}
