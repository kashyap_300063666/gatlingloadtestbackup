package simulations.partners.contracts

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class ContractsSimulation extends BaseSimulation {

  private val vmsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.VMS_SERVICE.toString)).disableWarmUp

  private val roles = Array("B2B_SELLER", "B2C_SELLER", "LOGISTICS", "BUYER", "STORE").map(e => Map("role" -> e)).array.random

  private val getPatnersByRole =
    scenario("partner contract api")
      .feed(roles)
      .exec(http("get partner by role")
        .get("/myntra-vms-service/vendorService/contract/partner/${role}")
        .headers(headers)
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(vmsBaseUrl)


}