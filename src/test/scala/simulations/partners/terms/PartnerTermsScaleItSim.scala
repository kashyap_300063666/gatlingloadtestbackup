package simulations.partners.terms

import simulations.BaseSimulation
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor

import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}


class PartnerTermsScaleItSim extends BaseSimulation{
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.PARTNER_TERMS_SERVICE.toString)

  private val partnerTermsBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  val masterCategoryFeeder = Array("FOOTWEAR", "APPAREL", "APPAREL", "APPAREL", "ACCESSORIES").map(e => Map("masterCategory" -> e)).array.circular
  val articleTypeFeeder = Array("FOSH", "JENS", "TRPT", "SWSH", "WTCH").map(e => Map("articleType" -> e)).array.circular
  val genderFeeder = Array("MEN", "WOMEN", "MEN", "WOMEN", "WOMEN").map(e => Map("gender" -> e)).array.circular
  val brandsFeeder = Array("RDTP", "HSTR", "HBHM", "PLUS", "DSBY").map(e => Map("brand" -> e)).array.circular
  val contractIdFeeder = Array(9236, 20146, 9227, 19861, 9237).map(e => Map("contractId" -> e)).array.circular
  val statusFeeder = Array(200).map(e => Map("status" -> e)).array.circular

  private val activeTermSearch = scenario("active term search")
    .feed(masterCategoryFeeder)
    .feed(articleTypeFeeder)
    .feed(genderFeeder)
    .feed(brandsFeeder)
    .feed(contractIdFeeder)
    .feed(statusFeeder)
    .exec(http("active term search")
      .post("/search/active/seller").queryParam("contractId", "${contractId}")
      .headers(headers)
      .body(StringBody("""{"brand":"${brand}","masterCategory":"${masterCategory}","articleType":"${articleType}","gender":"${gender}"}""")).asJson
      .check(status.is("${status}")))
    .inject(rampUsersPerSec( getIntProperty("activeTermSearch_rampup_user_startrate"))
      to ( getIntProperty("activeTermSearch_rampup_user_endrate"))
      during ( getIntProperty("duration.seconds")))

  private val activeTermBulkSearch = scenario("active term bulk search")
    .feed(masterCategoryFeeder)
    .feed(articleTypeFeeder)
    .feed(genderFeeder)
    .feed(brandsFeeder)
    .feed(contractIdFeeder)
    .exec(http("active term bulk search")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody("""[ {"_contractId": "${contractId}", "_type":"seller",  "brand":"${brand}","masterCategory":"${masterCategory}","articleType":"${articleType}","gender":"${gender}"}, {"_contractId": "${contractId}", "_type":"seller",  "brand":"ALL","masterCategory":"ALL","articleType":"ALL","gender":"ALL"} ]""")).asJson)
    .inject(rampUsersPerSec(getIntProperty("activeTermBulkSearch_rampup_user_startrate"))
      to ( getIntProperty("activeTermBulkSearch_rampup_user_endrate"))
      during ( getIntProperty("duration.seconds")))

  setUp(List(activeTermSearch, activeTermBulkSearch)).maxDuration(maxDurationInSec)
    .protocols(partnerTermsBaseUrl)
}
