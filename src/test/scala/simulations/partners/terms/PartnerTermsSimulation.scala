package simulations.partners.terms

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class PartnerTermsSimulation extends BaseSimulation {

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.PARTNER_TERMS_SERVICE.toString)

  private val partnerTermsBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  private val getAllTaxonomies = scenario("get all taxonomies")
    .exec(http("get all taxonomies")
      .get("/taxonomies")
      .headers(headers)
      .check(status.is(200)))

  val taxonomyIdFeeder = Array(5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 21).map(e => Map("id" -> e)).random.circular

  private val findTaxonomyById = scenario("find taxonomy by id")
    .feed(taxonomyIdFeeder)
    .exec(http("find taxonomy by id")
      .get("/taxonomies/${id}")
      .headers(headers)
      .check(status.is(200)))

  val taxonomyTypeFeeder = Array("additionalClassification", "agreementType", "billTo", "brandType", "catalogContributionBasis", "contributionBasis", "creditBasis", "discountSharingType", "frequency", "gender", "marginBasis", "paymentMethod", "payment_method", "purchaseType", "salesCycleSOR", "sellerCommissionBasis", "sellerCustomerPaymentType", "sellerPaymentMethod", "stockCorrectionBasis", "stockCorrectionType").map(e => Map("type" -> e)).random.circular

  private val findTaxonomyByType = scenario("find taxonomy by type")
    .feed(taxonomyTypeFeeder)
    .exec(http("find taxonomy by type")
      .get("/taxonomies/search/byType").queryParam("type", "${type}")
      .headers(headers)
      .check(status.is(200)))

  val termIdFeeder = Array(3551, 1152, 1153, 1154, 1404, 1405, 1406, 1535, 1536, 1537, 7239, 7240, 7241, 9657, 9658, 9659, 11303, 11304, 11305, 11499, 11500, 11501).map(e => Map("id" -> e)).random.circular

  private val findTermById = scenario("find terms by id")
    .feed(termIdFeeder)
    .exec(http("find terms by id")
      .get("/terms/${id}")
      .headers(headers)
      .check(status.is(200)))

  private val getTermTransitions = scenario("get term transitions")
    .feed(termIdFeeder)
    .exec(http("get term transitions")
      .get("/terms/${id}/transitions")
      .headers(headers)
      .check(status.is(200)))

  val agreementTypeFeeder = Array("JIT_MARKETPLACE", "JIT_MARKETPLACE", "JIT_MARKETPLACE", "JIT_MARKETPLACE", "JIT_MARKETPLACE").map(e => Map("agreementType" -> e)).array.circular
  val masterCategoryFeeder = Array("FOOTWEAR", "APPAREL", "APPAREL", "ACCESSORIES", "ACCESSORIES").map(e => Map("masterCategory" -> e)).array.circular
  val articleTypeFeeder = Array("", "", "", "", "").map(e => Map("articleType" -> e)).array.circular
  val genderFeeder = Array("", "", "ALL", "ALL", "ALL").map(e => Map("gender" -> e)).array.circular
  val brandsFeeder = Array("RPIR", "TRRY", "FLMW", "ATUN", "DNSN").map(e => Map("brand" -> e)).array.circular
  val additionalClassificationFeeder = Array("NOT_APPLICABLE", "NOT_APPLICABLE", "NOT_APPLICABLE", "NOT_APPLICABLE", "NOT_APPLICABLE").map(e => Map("additionalClassification" -> e)).array.circular
  val contractIdFeeder = Array(9658, 20739, 8161, 7830, 8854).map(e => Map("contractId" -> e)).array.circular
  val statusFeeder = Array(200).map(e => Map("status" -> e)).array.circular

  private val activeTermSearch = scenario("active term search")
    .feed(agreementTypeFeeder)
    .feed(masterCategoryFeeder)
    .feed(articleTypeFeeder)
    .feed(genderFeeder)
    .feed(brandsFeeder)
    .feed(contractIdFeeder)
    .feed(additionalClassificationFeeder)
    .feed(statusFeeder)
    .exec(http("active term search")
      .post("/search/active/po").queryParam("contractId", "${contractId}")
      .headers(headers)
      .body(StringBody("""{"brand":"${brand}","agreementType":"${agreementType}","masterCategory":"${masterCategory}","articleType":"${articleType}","gender":"${gender}","additionalClassification":"${additionalClassification}"}""")).asJson
      .check(status.is("${status}")))

  private val activeTermBulkSearch = scenario("active term bulk search")
    .feed(agreementTypeFeeder)
    .feed(masterCategoryFeeder)
    .feed(articleTypeFeeder)
    .feed(genderFeeder)
    .feed(brandsFeeder)
    .feed(contractIdFeeder)
    .feed(additionalClassificationFeeder)
    .exec(http("active term bulk search")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody("""[ {"_contractId": "${contractId}", "_type":"po",  "brand":"${brand}","agreementType":"${agreementType}","masterCategory":"${masterCategory}","articleType":"${articleType}","gender":"${gender}","additionalClassification":"${additionalClassification}"}, {"_contractId": "${contractId}", "_type":"po",  "brand":"ALL","agreementType":"","masterCategory":"ALL","articleType":"ALL","gender":"ALL","additionalClassification":"NOT_APPLICABLE"} ]""")).asJson)

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(partnerTermsBaseUrl)

}