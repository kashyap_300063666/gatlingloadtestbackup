package simulations.partners.terms

import simulations.BaseSimulation
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor

import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}

class PartnerTermsSimConstantUser extends BaseSimulation{
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.PARTNER_TERMS_SERVICE.toString)

  private val partnerTermsBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  val rampUpPercentage = getIntProperty("rampUpPercentage")

  val statusFeeder = Array(200).map(e => Map("status" -> e)).array.circular

  val entityIdFeeder = Array(22264, 21844, 22072).map(e => Map("entity_id" -> e)).array.circular
  val when = Array(2019-10-22).map(e => Map("when" -> e)).array.circular

  private val activeContractInfoTermBulkSearch = scenario("active contract info bulk search for 3 contracts")
    .feed(entityIdFeeder)
    .feed(when)
    .feed(statusFeeder)
    .exec(http("active contract info bulk search for 3 contracts")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody(
        """[ {"_when": "2019-10-22", "_type":"contractinfo", "_entityId":"${entity_id}"} ]""")).asJson
      .check(status.is("${status}")))
    .inject(
      rampUsersPerSec(1) to noOfUsers
        during ((duration * rampUpPercentage) / 100),
      constantUsersPerSec(noOfUsers)
        during ((duration * (100 - rampUpPercentage)) / 100)
    )


  val _entityIdFeeder = Array(9227,22836,22934,22894,22892,22106,22954,22944,22938,22946).map(e => Map("_entity_id" -> e)).array.circular
  private val activeContractInfoTermBulkSearchSecond = scenario("active contract info bulk search for 1 api")
    .feed(_entityIdFeeder)
    .feed(statusFeeder)
    .exec(http("active contract info bulk search for 1 api")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody("""[ {"_when": "2019-07-01", "_type":"contractinfo", "_entityId":"${_entity_id}"} ]""")).asJson
      .check(status.is("${status}")))
    .inject(
      rampUsersPerSec(1) to noOfUsers
        during ((duration * rampUpPercentage) / 100),
      constantUsersPerSec(noOfUsers)
        during ((duration * (100 - rampUpPercentage)) / 100)
    )


  val agreementTypeFeeder = Array("JIT_MARKETPLACE", "JIT_MARKETPLACE", "JIT_MARKETPLACE", "JIT_MARKETPLACE", "JIT_MARKETPLACE").map(e => Map("agreementType" -> e)).array.circular
  val masterCategoryFeeder = Array("FOOTWEAR", "APPAREL", "APPAREL", "ACCESSORIES", "ACCESSORIES").map(e => Map("masterCategory" -> e)).array.circular
  val articleTypeFeeder = Array("", "", "", "", "").map(e => Map("articleType" -> e)).array.circular
  val genderFeeder = Array("", "", "ALL", "ALL", "ALL").map(e => Map("gender" -> e)).array.circular
  val brandsFeeder = Array("RPIR", "TRRY", "FLMW", "ATUN", "DNSN").map(e => Map("brand" -> e)).array.circular
  val additionalClassificationFeeder = Array("NOT_APPLICABLE", "NOT_APPLICABLE", "NOT_APPLICABLE", "NOT_APPLICABLE", "NOT_APPLICABLE").map(e => Map("additionalClassification" -> e)).array.circular
  val contractIdFeeder = Array(9658, 20739, 8161, 7830, 8854).map(e => Map("contractId" -> e)).array.circular

  private val activeTermBulkSearchPo = scenario("active term bulk search PO")
    .feed(agreementTypeFeeder)
    .feed(masterCategoryFeeder)
    .feed(articleTypeFeeder)
    .feed(genderFeeder)
    .feed(brandsFeeder)
    .feed(contractIdFeeder)
    .feed(additionalClassificationFeeder)
    .feed(statusFeeder)
    .exec(http("active term bulk search PO")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody(
        """[ {"_contractId": "${contractId}", "_type":"po", "brand":"${brand}", "agreementType":"${agreementType}", "masterCategory":"${masterCategory}", "articleType":"${articleType}", "gender":"${gender}", "additionalClassification":"${additionalClassification}"}]""".stripMargin)).asJson
      .check(status.is("${status}")))
    .inject(
      rampUsersPerSec(1) to noOfUsers
        during ((duration * rampUpPercentage) / 100),
      constantUsersPerSec(noOfUsers)
        during ((duration * (100 - rampUpPercentage)) / 100)
    )


  val _brandFeeder = Array("PUMA", "SJYA", "GRSN", "SIUT", "STON", "TYLB", "CLWS", "EPIC", "FYWD", "FLUD").map(e => Map("brand" -> e)).array.circular
  private val activeCommssionTermBulkSearch = scenario("active commission bulk search for contracts")
    .feed(_brandFeeder)
    .feed(when)
    .feed(statusFeeder)
    .exec(http("active commission bulk search for contracts")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody(
        """[ {"_when": "2019-10-22", "_type": "commission", "_entityId": 2297, "model" : "COMBINED_COMMISSION", "gender" : "ALL", "articleType" : "ALL", "brand" : "${brand}"}]""")).asJson
      .check(status.is("${status}")))
    .inject(
      rampUsersPerSec(1) to noOfUsers
        during ((duration * rampUpPercentage) / 100),
      constantUsersPerSec(noOfUsers)
        during ((duration * (100 - rampUpPercentage)) / 100)
    )


//  val brandFeederSi = Array("TAYA", "VEMO", "LBAS", "YRIS").map(e => Map("brandCode" -> e)).array.circular
//  val entityIdFeederSi = Array(20331, 24750, 24682, 24168).map(e => Map("entityIdSi" -> e)).array.circular
//  val genderFeederSi = Array("UNISEX", "WOMEN", "WOMEN", "WOMEN").map(e => Map("gender" -> e)).array.circular
//  val articleTypeFeederSi = Array("LLLS", "CLTH", "PLZS", "PLZS").map(e => Map("articleType" -> e)).array.circular
//  val whenSi = Array(2020-10-3).map(e => Map("when" -> e)).array.circular
  val brandFeederSi = Array("TAYA", "HNCK", "DNSN", "NRII", "LGBR", "MYTH", "LOOC", "SSCR", "SADG", "BMER").map(e => Map("brandCode" -> e)).array.circular
  val genderFeederSi = Array("UNISEX", "MEN", "MEN", "WOMEN", "WOMEN", "UNISEX", "GIRLS", "GIRLS", "WOMEN", "MEN").map(e => Map("gender" -> e)).array.circular
  val articleTypeFeederSi = Array("PTFM", "KRTA", "BELT", "DRSS", "OTMK", "CSCV", "TSHT", "DRSS", "SKRT", "SWSH").map(e => Map("articleType" -> e)).array.circular
  val whenSi = Array(2020-12-4).map(e => Map("when" -> e)).array.circular
  val entityIdFeederSi = Array(20331, 20134, 24854, 25738, 25794, 26510, 26088, 23902, 25638, 23760).map(e => Map("_entity_id" -> e)).array.circular
  private val activeSellerIncentiveOptedInTermBulkSearch = scenario("active sellerincentiveoptedin bulk search")
    .feed(brandFeederSi)
    .feed(genderFeederSi)
    .feed(articleTypeFeederSi)
    .feed(entityIdFeederSi)
    .feed(statusFeeder)
    .exec(http("active seller incentive optedin bulk search")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody(
        """[ {"_when": "2020-12-04", "_type": "sellerincentiveoptedin", "_entityId": "${_entity_id}", "gender" : "${gender}", "articleType" : "${articleType}", "brand" : "${brandCode}"}]""")).asJson
      .check(status.is("${status}")))
    .inject(
      rampUsersPerSec(1) to noOfUsers
        during ((duration * rampUpPercentage) / 100),
      constantUsersPerSec(noOfUsers)
        during ((duration * (100 - rampUpPercentage)) / 100)
    )


  val articleLevelFeeder = Array("1", "2", "3", "4", "5").map(e => Map("articleLevel" -> e)).array.circular
  val fulfillmentMode = Array("FBM").map(e => Map("fulfillmentMode" -> e)).array.circular
  private val activeLogisticsFulfillmentTermBulkSearch = scenario("active logisticsfulfillment bulk search")
    .feed(articleLevelFeeder)
    .feed(statusFeeder)
    .exec(http("active logisticsfulfillment bulk search")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody(
        """[ {"_type": "logisticsfulfillment", "_entityId": 2297, "articleLevel" : "${articleLevel}"}]""")).asJson
      .check(status.is("${status}")))
    .inject(
      rampUsersPerSec(1) to noOfUsers
        during ((duration * rampUpPercentage) / 100),
      constantUsersPerSec(noOfUsers)
        during ((duration * (100 - rampUpPercentage)) / 100)
    )


  val entityIdFeederLdc = Array(4216, 4215).map(e => Map("entityIdLdc" -> e)).array.circular
  private val activeLdcTermBulkSearch = scenario("active ldc bulk search")
    .feed(statusFeeder)
    .exec(http("active ldc bulk search")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody(
        """[ {"_type": "ldc", "_entityId": "${entityIdLdc}"}]""")).asJson
      .check(status.is("${status}")))
    .inject(
      rampUsersPerSec(1) to noOfUsers
        during ((duration * rampUpPercentage) / 100),
      constantUsersPerSec(noOfUsers)
        during ((duration * (100 - rampUpPercentage)) / 100)
    )


  setUp(List(activeContractInfoTermBulkSearch, activeContractInfoTermBulkSearchSecond, activeTermBulkSearchPo, activeCommssionTermBulkSearch, activeSellerIncentiveOptedInTermBulkSearch)).maxDuration(maxDurationInSec)
    .protocols(partnerTermsBaseUrl)

}
