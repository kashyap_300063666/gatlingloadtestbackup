package simulations.partners.terms

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class MfpLoadSimulation extends BaseSimulation {

  val planningIdFeeder = Array(
    Map("key" -> 57346, "value" -> 109),
    Map("key" -> 27329, "value" -> 126),
    Map("key" -> 50000, "value" -> 111),
    Map("key" -> 17288, "value" -> 107),
    Map("key" -> 57346, "value" -> 110),
    Map("key" -> 21517, "value" -> 107),
    Map("key" -> 14044, "value" -> 117),
    Map("key" -> 28817, "value" -> 122),
    Map("key" -> 14040, "value" -> 120),
    Map("key" -> 34928, "value" -> 117),
  ).random

  val dateFeeder = Array("2018-05-20", "2018-05-21", "2018-05-22", "2018-05-23", "2018-05-24", "2018-05-25", "2018-05-26", "2018-05-17", "2018-05-18", "2018-05-19").map(e => Map("toDate" -> e)).circular
    val eventDateFeeder = Array("2018-06-10", "2018-06-11", "2018-06-12", "2018-06-13", "2018-06-14", "2018-06-15", "2018-06-16", "2018-06-17", "2018-06-18", "2018-06-19").map(e => Map("eventDate" -> e)).circular
  val monthFeeder = Array(
    Map("start" -> 4, "end" -> 5),
    Map("start" -> 5, "end" -> 9),
    //Map("start" -> 4,"end" -> 9)
  ).random

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MFP.toString)
  //private val mfpBaseUrl = "http://localhost:8080/"

  private val mfpBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  private val createScenario = scenario("mfp createScenario")
    .feed(monthFeeder)
    .feed(planningIdFeeder)
    .exec(http("createScenario")
      .post("/myntra-mfp-service/financialPlan/scenario/createScenario")
      .headers(headers)
      .body(StringBody(
        """{"scenarioType":"PRE_SEASON",
            "roleUp":"MONTH",
            "financialYear":209,
            "roleUpStartValue":${start},
            "roleUpEndValue":${end},
            "channel":2,
            "businessType":50702,
            "scenarioName":"mfptest050",
            "planningAxis":${value},
            "userId":${key}}""")).asJson
      .check(status.is(200)))

  private val editScenarioDelta = scenario("mfp editScenarioDelta")
    .exec(http("editScenarioDelta")
      .post("/myntra-mfp-service/financialPlan/scenario/editScenario").queryParam("page", 1).queryParam("pageLimit", 5)
      .headers(headers)
      .body(StringBody(
        """{
                           "roleUp": "event",
                           "scenarioId": 326,
                           "planningAxisId": 46,
                           "strategyType": "BreakBack",
                           "grandTotalHold": false,
                           "breakBacktype": "DELTA",
                           "freshness": "none",
                           "denomination": "CR",
                           "aggregateMetricChanges": [
                             {
                               "rowId": "2",
                               "metricName": "gmv",
                               "metricNewValue": "5",
                               "roleUpValue": "6",
                               "freshnessNewValue": 0,
                               "freshnessOldValue": 0,
                               "freshnessNewEdited": false,
                               "freshnessOldEdited": false,
                               "eventType": "PRE_EVENT"
                             }
                           ],
                           "localMetricChanges": [

                           ],
                           "aggregateMetricHolds": [

                           ],
                           "localMetricHolds": [

                           ]
                         }""")).asJson
      .check(status.is(200)))

  private val editScenarioAbsolute = scenario("mfp editScenarioAbsolute")
    .exec(http("editScenarioAbsolute")
      .post("/myntra-mfp-service/financialPlan/scenario/editScenario").queryParam("page", 1).queryParam("pageLimit", 5)
      .headers(headers)
      .body(StringBody(
        """{
                            "roleUp":"event",
                            "scenarioId":326,
                            "planningAxisId":46,
                            "strategyType":"BreakBack",
                            "grandTotalHold":false,
                            "breakBacktype":"ABSOLUTE",
                            "freshness":"none",
                            "denomination":"CR",
                            "aggregateMetricChanges":[

                            ],
                            "localMetricChanges":[
                               {
                                  "rowId":"0",
                                  "dimension":[
                                     {
                                        "id":2,
                                        "value":"Myntra",
                                        "type":"channel"
                                     },
                                     {
                                        "id":4,
                                        "value":"MMB",
                                        "type":"source"
                                     },
                                     {
                                        "id":50717,
                                        "value":"Men's Work Wear",
                                        "type":"businessUnit"
                                     },
                                     {
                                        "id":50733,
                                        "value":"Arrow",
                                        "type":"brandGroup"
                                     },
                                     {
                                        "id":45730,
                                        "value":"Arrow",
                                        "type":"brand"
                                     },
                                     {
                                        "id":55866,
                                        "value":"Jackets",
                                        "type":"article"
                                     },
                                     {
                                        "id":55704,
                                        "value":"Men",
                                        "type":"gender"
                                     }
                                  ],
                                  "metricdelta":{
                                     "metricName":"gmv",
                                     "oldValue":"0",
                                     "newValue":"10",
                                     "roleUpValue":"6",
                                     "freshnessNewValue":0,
                                     "freshnessOldValue":0,
                                     "freshnessNewEdited":false,
                                     "freshnessOldEdited":false,
                                     "eventType":"PRE_EVENT"
                                  }
                               }
                            ],
                            "aggregateMetricHolds":[

                            ],
                            "localMetricHolds":[

                            ]
                         }""")).asJson
      .check(status.is(200)))

  private val editScenarioAbsoluteLocks = scenario("mfp editScenarioAbsoluteLocks")
    .exec(http("editScenarioAbsoluteLocks")
      .post("/myntra-mfp-service/financialPlan/scenario/editScenario").queryParam("page", 1).queryParam("pageLimit", 5)
      .headers(headers)
      .body(StringBody(
        """{
                           "roleUp": "event",
                           "scenarioId": 326,
                           "planningAxisId": 46,
                           "strategyType": "BreakBack",
                           "grandTotalHold": false,
                           "breakBacktype": "ABSOLUTE",
                           "freshness": "none",
                           "denomination": "CR",
                           "aggregateMetricChanges": [
                           ],
                           "localMetricChanges": [
                             {
                               "rowId": "1",
                               "dimension": [
                                 {
                                   "id": 2,
                                   "value": "Myntra",
                                   "type": "channel"
                                 },
                                 {
                                   "id": 4,
                                   "value": "MMB",
                                   "type": "source"
                                 },
                                 {
                                   "id": 50717,
                                   "value": "Men's Work Wear",
                                   "type": "businessUnit"
                                 },
                                 {
                                   "id": 50733,
                                   "value": "Arrow",
                                   "type": "brandGroup"
                                 },
                                 {
                                   "id": 45730,
                                   "value": "Arrow",
                                   "type": "brand"
                                 },
                                 {
                                   "id": 55868,
                                   "value": "Jeans",
                                   "type": "article"
                                 },
                                 {
                                   "id": 55704,
                                   "value": "Men",
                                   "type": "gender"
                                 }
                               ],
                               "metricdelta": {
                                 "metricName": "gmv",
                                 "oldValue": "0",
                                 "newValue": "7",
                                 "roleUpValue": "6",
                                 "freshnessNewValue": 0,
                                 "freshnessOldValue": 0,
                                 "freshnessNewEdited": false,
                                 "freshnessOldEdited": false,
                                 "eventType": "PRE_EVENT"
                               }
                             }
                           ],
                           "aggregateMetricHolds": [
                             {
                               "metricName": "gmv",
                               "metricOldValue": "16.52",
                               "metricNewValue": "16.52",
                               "eventType": "PRE_EVENT",
                               "roleUpValue": "6"
                             },
                             {
                               "metricName": "gmv",
                               "metricOldValue": "12.44",
                               "metricNewValue": "12.44",
                               "eventType": "EVENT",
                               "roleUpValue": "6"
                             },
                             {
                               "metricName": "gmv",
                               "metricOldValue": "52.25",
                               "metricNewValue": "52.25",
                               "eventType": "POST_EVENT",
                               "roleUpValue": "6"
                             }
                           ],
                           "localMetricHolds": [

                           ]
                         }""")).asJson
      .check(status.is(200)))

  private val createEvent = scenario("mfp createEvent")
    .feed(dateFeeder)
    .exec(http("createEvent")
      .post("/myntra-mfp-service/financialPlan/event/createEvent")
      .headers(headers)
      .body(StringBody(
        """{"event":15,
                         	"year":209,
                         	"eventStart":"2018-05-02",
                         	"eventEnd":"${toDate}"}""")).asJson
      .check(status.is(200)))

  private val viewScenario = scenario("mfp viewScenario")
    .exec(http("viewScenario")
      .post("/myntra-mfp-service/financialPlan/scenario/viewScenario")
      .headers(headers)
      .body(StringBody(
        """{
                            "page":1,
                            "pageLimit":20,
                            "roleUpValueStart":6,
                            "roleUpValueEnd":6,
                            "roleUp":"event",
                            "scenarioId":326,
                            "planningAxisId":46,
                            "commercialType":"none",
                            "freshness":"none",
                            "compare":"none",
                            "compareStartDate":"",
                            "compareEndDate":"",
                            "doSort":false,
                            "doSearch":false,
                            "denomination":"CR",
                            "metricsOrderBy":{
                               "metricName":"gmv",
                               "orderBy":"ASC",
                               "roleUpValue":4,
                               "eventType":4
                            },
                            "dimentionSearch":{

                            },
                            "userId":21349
                         }""")).asJson
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(mfpBaseUrl)

}
