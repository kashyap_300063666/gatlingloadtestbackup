package simulations.poc2

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, HLTUtility}
import com.myntra.hltScenarioParams
import com.myntra.hlt._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class HLTRunner extends BaseSimulation {


  /* Utility class for HLT */
  protected val util = new HLTUtility()

  /* Scenario Injection Steps Builder */
  private val injectSteps = new InjectionSteps()

  /* Initializing local variables */
  private val csBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)).disableWarmUp.disableFollowRedirect.shareConnections

  if(!getBooleanProperty("isHLT"))
  {
    util.downloadFile("styles.csv")
  }
  /* Download all prod StyleIds to a file */


  /* Set all the APIs as enabled by default */
  util.setAPIStatuses()

  /* Initialize all scenarios objects */
  val allScenarios = new Scenarios()

  private val loadParams = new LoadParams()

  /* Add required load settings for each scenario */
  setUp(
      
     allScenarios.landingPageBiro.inject(
      injectSteps.getRatioSteps(hltScenarioParams.landingPageBiroRatio, hltScenarioParams.landingPageBiroValue)
    ),
    
     allScenarios.landingPageMen.inject(
      injectSteps.getRatioSteps(hltScenarioParams.landingPageMenRatio, hltScenarioParams.landingPageMenValue)
    ),
    
     allScenarios.landingPageWomen.inject(
      injectSteps.getRatioSteps(hltScenarioParams.landingPageWomenRatio, hltScenarioParams.landingPageWomenValue)
    ),
    
     allScenarios.landingPageSmashbox.inject(
      injectSteps.getRatioSteps(hltScenarioParams.landingPageSmashboxRatio, hltScenarioParams.landingPageSmashboxValue)
    ),

     allScenarios.landingPageMall.inject(
      injectSteps.getRatioSteps(hltScenarioParams.landingPageMallRatio, hltScenarioParams.landingPageMallValue)
    ),

    /* Landing Page */
    allScenarios.landingPage.inject(
      injectSteps.getRatioSteps(hltScenarioParams.landingPageRatio, hltScenarioParams.landingPageValue)
    ),
    /* Recommended API */
    allScenarios.landingPageRecommended.inject(
      injectSteps.getRatioSteps(hltScenarioParams.landingPageRecommendedRatio, hltScenarioParams.landingPageRecommendedValue)
    ),

    allScenarios.landingPageGetLoyaltyBalance.inject(
      injectSteps.getRatioSteps(hltScenarioParams.landingPageGetLoyaltyBalanceRatio, hltScenarioParams.landingPageGetLoyaltyBalanceValue)
    ),

    allScenarios.searchCallAutoSuggest.inject(
      injectSteps.getRatioSteps(hltScenarioParams.searchCallAutoSuggestRatio, hltScenarioParams.searchCallAutoSuggestValue)
    ),
    
    //search paginationMTV2
     allScenarios.searchCallPaginationsV2.inject(
      injectSteps.getRatioSteps(hltScenarioParams.searchCallPaginationsV2Ratio, hltScenarioParams.searchCallPaginationsV2Value)
    ),
    
    //search paginationMTV1
     allScenarios.searchpaginationMTV1.inject(
      injectSteps.getRatioSteps( hltScenarioParams.searchpaginationMTV1Ratio, hltScenarioParams.searchpaginationMTV1Value)
    ),
    
    
    //search - listPage.clusterV2
    allScenarios.searchClusterV2.inject(
      injectSteps.getRatioSteps( hltScenarioParams.searchClusterV2Ratio, hltScenarioParams.searchClusterV2Value)
    ),
    
    //search - listPage.filtersV2
    allScenarios.searchFiltersV2.inject(
      injectSteps.getRatioSteps( hltScenarioParams.searchFiltersV2Ratio, hltScenarioParams.searchFiltersV2Value)
    ),
    
    //search - listPage.filtersV1
     allScenarios.searchFiltersV1.inject(
      injectSteps.getRatioSteps( hltScenarioParams.searchFiltersV1Ratio, hltScenarioParams.searchFiltersV1Value)
    ),
    
    
    //PDP - pdpPageCalls.similarStylesLego
      allScenarios.pdpSimilarStylesLego.inject(
      injectSteps.getRatioSteps( hltScenarioParams.pdpSimilarStylesLegoRatio, hltScenarioParams.pdpSimilarStylesLegoValue)
    ),
    
    //PDP - pdpPageCalls.crossSellLego
    
     allScenarios.pdpcrossSellLego.inject(
      injectSteps.getRatioSteps( hltScenarioParams.pdpcrossSellLegoRatio, hltScenarioParams.pdpcrossSellLegoValue)
    ),
    
    //PDP - pdpPageCalls.similarStylesV2

    allScenarios.pdpSimilarStyles.inject(
      injectSteps.getRatioSteps( hltScenarioParams.pdpSimilarStylesRatio, hltScenarioParams.pdpSimilarStylesValue)
    ),

     allScenarios.pdpsimilarStylesV2.inject(
      injectSteps.getRatioSteps( hltScenarioParams.pdpsimilarStylesV2Ratio, hltScenarioParams.pdpsimilarStylesV2Value)
    ),
    
    //PDP - pdpPageCalls.crossSellV2
     allScenarios.pdpcrossSellV2.inject(
      injectSteps.getRatioSteps( hltScenarioParams.pdpcrossSellV2Ratio, hltScenarioParams.pdpcrossSellV2Value)
    ),
    
    //PDP - pdpPageCalls.crossSell
     allScenarios.pdpcrossSell.inject(
      injectSteps.getRatioSteps( hltScenarioParams.pdpcrossSellRatio, hltScenarioParams.pdpcrossSellValue)
    ),
    
    //PDP - pdpPageCalls.clickForOffer
     allScenarios.pdpStyleOffers.inject(
      injectSteps.getRatioSteps( hltScenarioParams.pdpStyleOffersRatio, hltScenarioParams.pdpStyleOffersValue)
    ),

    /* PDP Style data APIs */
    allScenarios.pdpStyleData.inject(
      injectSteps.getRatioSteps(hltScenarioParams.pdpStyleDataRatio, hltScenarioParams.pdpStyleDataValue)
    ),

    /* PDP Style V2 */
    allScenarios.pdpStyleV2.inject(
      injectSteps.getRatioSteps(hltScenarioParams.pdpStyleV2Ratio, hltScenarioParams.pdpStyleV2Value)
    ),

    allScenarios.pdpStyleLego.inject(
      injectSteps.getRatioSteps(hltScenarioParams.pdpStyleLegoRatio, hltScenarioParams.pdpStyleLegoValue)
    ),

    //PDP - styleV2 and ClickForOfferV2
    allScenarios.stylePdpV2OfferV2SizeRecommendSizeProfileInsiderPoints.inject(
      injectSteps.getRatioSteps( hltScenarioParams.stylePdpV2OfferV2SizeRecommendSizeProfileInsiderPointsRatio, hltScenarioParams.stylePdpV2OfferV2SizeRecommendSizeProfileInsiderPointsValue)
    ),

    //PDP - styleLego and ClickForOfferLego
    allScenarios.stylePdpLegoOfferLegoShoppableLooks.inject(
      injectSteps.getRatioSteps( hltScenarioParams.stylePdpLegoOfferLegoShoppableLooksRatio, hltScenarioParams.stylePdpLegoOfferLegoShoppableLooksValue)
    ),

    allScenarios.styleLegoOffersLegoShoppableLooksAddCheckoutApplyCouponRemoveCartFiller.inject(
      injectSteps.getRatioSteps( hltScenarioParams.styleLegoOffersLegoShoppableLooksAddCheckoutApplyCouponRemoveCartFillerRatio, hltScenarioParams.styleLegoOffersLegoShoppableLooksAddCheckoutApplyCouponRemoveCartFillerValue)
    ),

    /* Delivery Checks*/
    allScenarios.checkDelivery.inject(
      injectSteps.getRatioSteps( hltScenarioParams.checkDeliveryRatio, hltScenarioParams.checkDeliveryValue)
    ),
    allScenarios.checkDeliveryV2.inject(
      injectSteps.getRatioSteps( hltScenarioParams.checkDeliveryV2Ratio, hltScenarioParams.checkDeliveryV2Value)
    ),
    allScenarios.checkDeliveryLego.inject(
      injectSteps.getRatioSteps( hltScenarioParams.checkDeliveryLegoRatio, hltScenarioParams.checkDeliveryLegoValue)
    ),
    allScenarios.checkDeliveryV3.inject(
      injectSteps.getRatioSteps( hltScenarioParams.checkDeliveryV3Ratio, hltScenarioParams.checkDeliveryV3Value)
    ),

    //UserAPIs - userWishlistAPIs.getWishListV2
    
     allScenarios.UserAPIsgetWishListV2.inject(
      injectSteps.getRatioSteps( hltScenarioParams.UserAPIsgetWishListV2Ratio, hltScenarioParams.UserAPIsgetWishListV2Value)
    ),

    allScenarios.wishListSummary.inject(
      injectSteps.getRatioSteps( hltScenarioParams.wishListSummaryRatio, hltScenarioParams.wishListSummaryValue)
    ),

    allScenarios.addToWishListUsingStylePdpLego.inject(
      injectSteps.getRatioSteps( hltScenarioParams.addToWishListUsingStylePdpLegoRatio, hltScenarioParams.addToWishListUsingStylePdpLegoValue)
    ),
    
    
     //addressPage.checkoutAddress
    
     allScenarios.addresscheckoutAddress.inject(
      injectSteps.getRatioSteps( hltScenarioParams.addresscheckoutAddressRatio, hltScenarioParams.addresscheckoutAddressValue)
    ),

    allScenarios.addNewAddress.inject(
      injectSteps.getRatioSteps( hltScenarioParams.addNewAddressRatio, hltScenarioParams.addNewAddressValue)
    ),

      //addressPage.checkoutAddressNode
    
     allScenarios.addresscheckoutAddressNode.inject(
      injectSteps.getRatioSteps( hltScenarioParams.addresscheckoutAddressNodeRatio, hltScenarioParams.addresscheckoutAddressNodeValue)
    ),
    
     //AddressPage:isPinCodeServicable
    
     allScenarios.addresspinCodeService.inject(
      injectSteps.getRatioSteps( hltScenarioParams.addresspinCodeServiceRatio, hltScenarioParams.addresspinCodeServiceValue)
    ),
    
     //addressPage.checkoutAddress AND AddressPage:selectAddress
    
     allScenarios.checkoutAndselectAddress.inject(
      injectSteps.getRatioSteps( hltScenarioParams.checkoutAndselectAddressRatio, hltScenarioParams.checkoutAndselectAddressValue)
    ),
    
    //EngagementPage - EngagementAccountSummary
     allScenarios.engagementAccountSummary.inject(
      injectSteps.getRatioSteps( hltScenarioParams.engagementAccountSummaryRatio, hltScenarioParams.engagementAccountSummaryValue)
    ),
    
    //EngagementPage - EngagementRewards
     allScenarios.engagementRewards.inject(
      injectSteps.getRatioSteps( hltScenarioParams.engagementRewardsRatio, hltScenarioParams.engagementRewardsValue)
    ),
    
    /* Cart APIs */
    allScenarios.styleLegoOfferLegoAddCheckoutApplyCouponRemoveFromCartCartFiller.inject(
      injectSteps.getRatioSteps(hltScenarioParams.styleLegoOfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerRatio, hltScenarioParams.styleLegoOfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerValue)
    ),

    allScenarios.styleV2OfferLegoAddCheckoutApplyCouponRemoveFromCartCartFiller.inject(
      injectSteps.getRatioSteps(hltScenarioParams.styleV2OfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerRatio, hltScenarioParams.styleV2OfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerValue)
    ),

    allScenarios.styleV2OfferLegoAddCheckoutToCartApplyCoupon.inject(
      injectSteps.getRatioSteps(hltScenarioParams.styleV2OfferLegoAddCheckoutToCartApplyCouponRatio, hltScenarioParams.styleV2OfferLegoAddCheckoutToCartApplyCouponValue)
    ),

    allScenarios.checkoutCartGetAllCoupons.inject(
      injectSteps.getRatioSteps(hltScenarioParams.checkoutCartGetAllCouponsRatio, hltScenarioParams.checkoutCartGetAllCouponsValue)
    ),

    allScenarios.cartDefaultApply.inject(
      injectSteps.getRatioSteps(hltScenarioParams.cartDefaultApplyRatio, hltScenarioParams.cartDefaultApplyValue)
    ),

    allScenarios.giftCardApply.inject(
      injectSteps.getRatioSteps(hltScenarioParams.giftCardApplyRatio, hltScenarioParams.giftCardApplyValue)
    ),

    allScenarios.checkoutCartNodeAPI.inject(
      injectSteps.getRatioSteps(hltScenarioParams.checkoutCartNodeAPIRatio, hltScenarioParams.checkoutCartNodeAPIValue)
    ),

    allScenarios.myOrders.inject(
      injectSteps.getRatioSteps(hltScenarioParams.myOrdersRatio, hltScenarioParams.myOrdersValue)
    ),

    
    //WebPage - Myntraweb
    allScenarios.Myntraweb.inject(
      injectSteps.getRatioSteps(hltScenarioParams.MyntrawebRatio, hltScenarioParams.MyntrawebValue)
    ),
    
    
    //WebPage - Myntrapwa
    allScenarios.Myntrapwa.inject(
      injectSteps.getRatioSteps(hltScenarioParams.MyntrapwaRatio, hltScenarioParams.MyntrapwaValue)
    ),
    
    //PdpPage.looks
    allScenarios.looks.inject(
      injectSteps.getRatioSteps(hltScenarioParams.looksRatio, hltScenarioParams.looksValue)
    ),
    
   //PdpPage.looksPlayground
    allScenarios.looksPlayground.inject(
      injectSteps.getRatioSteps(hltScenarioParams.looksPlaygroundRatio, hltScenarioParams.looksPlaygroundValue)
    ),
    
   //pdpPageCalls.looksV2Lego
    allScenarios.looksV2Lego.inject(
      injectSteps.getRatioSteps(hltScenarioParams.looksV2LegoRatio, hltScenarioParams.looksV2LegoValue)
    ),
    
   // HomePage.GetScratchCard
    allScenarios.GetScratchCard.inject(
      injectSteps.getRatioSteps(hltScenarioParams.GetScratchCardRatio, hltScenarioParams.GetScratchCardValue)
    ),
    
   // HomePage.Referralcode
    allScenarios.Referralcode.inject(
      injectSteps.getRatioSteps(hltScenarioParams.ReferralcodeRatio, hltScenarioParams.ReferralcodeValue)
    ),
    
   // HomePage.fetchSlot
    allScenarios.fetchSlot.inject(
      injectSteps.getRatioSteps(hltScenarioParams.fetchSlotRatio, hltScenarioParams.fetchSlotValue)
    )
  

    
//    allScenarios.removefromcartAPI.inject(
//      injectSteps.getRatioSteps(hltScenarioParams.maxHltLoad)
//    ),
//    
     /* Address APIs */
//      allScenarios.addressAPI.inject(
//      injectSteps.getRatioSteps(hltScenarioParams.maxHltLoad)
//    ),
//      allScenarios.addressPincodeAPI.inject(
//      injectSteps.getRatioSteps(hltScenarioParams.maxHltLoad)
//    ),
//     
//    /* PDP Style data APIs */
//    allScenarios.pdpStyleData.inject(
//      injectSteps.getRatioSteps(hltScenarioParams.maxHltLoad)
//    ),
//    /* All PdpStyle APIs */
//    allScenarios.pdpPageAllCalls.inject(
//      injectSteps.getRatioSteps(hltScenarioParams.maxHltLoad)
//    ),
//    /* All Search APIs */
//    allScenarios.searchAllCalls.inject(
//      injectSteps.getRatioSteps(hltScenarioParams.maxHltLoad)
//    ),

  ).protocols(csBaseUrl).maxDuration(loadParams.maxLoadHoldDuration.toInt * 60
    + loadParams.rampUpDuration.toInt * 60 + getLongProperty("extraDuration.seconds").toInt)

}
