package simulations.poc2

import io.gatling.core.Predef._
import com.myntra.commons.util.{CSVUtils, PropertyLoader}
import io.gatling.core.feeder.BatchableFeederBuilder

import scala.util.Random

class Feeders  extends  PropertyLoader{

  /* Feeder file location in test/resources */
  private val feederFilesPath = "hlt-test-data/feeders"

  /* Read the downloaded styles csv to create list of StyleIds */
  var styleIdsData: List[String] = CSVUtils.readCSV(feederFilesPath + "/HltStyleData.csv")
    .map(e => e.apply(0)).toList

  //private var styleIdsData
  if(getBooleanProperty("isHLT"))
  {
     styleIdsData = CSVUtils.readCSV(feederFilesPath + "/HltStyleData.csv")
        .map(e => e.apply(0)).toList
  }else {
     styleIdsData = CSVUtils.readCSV2("styles.csv", isIncludeHeader = false)
      .map(e => e.apply(0)).toList
  }
//  private val styleIdsData: List[String] = CSVUtils.readCSV2("styles.csv", isIncludeHeader = false)
//    .map(e => e.apply(0)).toList

  /* Read the card numbers csv from resources folder to create list of card numbers */
  private val amexCardsData: List[String] = CSVUtils.readCSV(feederFilesPath + "/amex.csv")
    .map(e => e.apply(0)).toList

  private val masterCardsData: List[String] = CSVUtils.readCSV(feederFilesPath + "/mastercard.csv")
    .map(e => e.apply(0)).toList

  private val visaCardsData: List[String] = CSVUtils.readCSV(feederFilesPath + "/visa.csv")
    .map(e => e.apply(0)).toList

  private val savedCardsData: List[String] = CSVUtils.readCSV(feederFilesPath + "/savecards.csv")
    .map(e => e.apply(0)).toList

  private val allCreditCardsData: List[String] = amexCardsData ::: masterCardsData ::: visaCardsData

  /* Get the number of Size of Lists */
  private val numberOfStyles: Int = styleIdsData.length
  private val numberOfCards: Int = allCreditCardsData.length
  private val numberOfSavedCards: Int = savedCardsData.length

  /* Create StyleId Feeder, from csv data, which picks random element from the styleId list */
  val stylesFeeder: Iterator[Map[String, String]] =
    Iterator.continually(
      Map(
        "styleId" -> styleIdsData.apply(Random.nextInt(numberOfStyles))
      )
    )

  /* Saved cards for Online Payments */
  val savedCardsFeeder: Iterator[Map[String, String]] =
    Iterator.continually(
      Map(
        "cardNumber" -> savedCardsData.apply(Random.nextInt(numberOfSavedCards))
      )
    )

  /* All credit cards for cash back / gift-card payments */
  val allCardsFeeder: Iterator[Map[String, String]] =
    Iterator.continually(
      Map(
        "cardNumber" -> allCreditCardsData.apply(Random.nextInt(numberOfCards))
      )
    )

  /* ${queryParam} */
  val queryParamFeeder: BatchableFeederBuilder[String] = csv(feederFilesPath + "/UserQuery.csv").random
  
  /* To Read Coupons from the csv file*/
  val couponsFeeder : BatchableFeederBuilder[String] = csv(feederFilesPath + "/coupons.csv").random
  
  /* To Read Pincode from the csv file - AddressPage */
  val pincodeFeeder : BatchableFeederBuilder[String] = csv(feederFilesPath + "/Pincode.csv").random

  /* To Read Search filter queries from the csv file */
  val filterQueryFeeder : BatchableFeederBuilder[String] = csv(feederFilesPath + "/filterQuery.csv").random

  /* To Read Scroll to page number from the csv file */
  val listPageNosFeeder : BatchableFeederBuilder[String] = csv(feederFilesPath + "/pageNos.csv").random
  
   /* To Read Search filter queries from the csv file */
  val filterQueryFeederCircular : BatchableFeederBuilder[String] = csv(feederFilesPath + "/filterQuery.csv").circular
  
  /* To Read Scroll to page number from the csv file */
  val listPageNosFeederCircular : BatchableFeederBuilder[String] = csv(feederFilesPath + "/pageNos.csv").circular
  
  
  /* ${queryParam} */
  val queryParamFeederCircular: BatchableFeederBuilder[String] = csv(feederFilesPath + "/UserQuery.csv").circular
  
  /* To Read ${queryParam} for Myntraweb and Myntrapwa */
  val queryParamWebAndPwaCircular : BatchableFeederBuilder[String] = csv(feederFilesPath + "/WebPwaQuery.csv").circular
}
