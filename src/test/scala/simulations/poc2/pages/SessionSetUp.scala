  package simulations.poc2.pages

import com.google.gson.{Gson, JsonObject}
import com.myntra.commons.FormGenTokenAndLoginUser
import com.myntra.commons.util.{HLTUtility, PropertyLoader}
import io.gatling.core.Predef._
import io.gatling.core.session.Session
import io.gatling.core.structure.ChainBuilder

import scala.util.Random

class SessionSetUp extends PropertyLoader{
  val util = new HLTUtility()

  /* Key name of counter which maintains the number of sessions stored in redis */
  private val counterKeyName = getProperty("counter.key", "counter")
  private val startUserCount = getIntProperty("startUserCount", "0")
  private val personalisationUsers = getIntProperty("personalisationUsers", "640000")
  private var count: Int = 0

  try{
    count = util.redisUtil.get(counterKeyName).toInt
  }catch {
    case x : Exception => count = 0
  }

  object SessionObj {

    val storeSessionDataInRedis: ChainBuilder = doIf(
      session =>
        session.contains("at") && !session("at").as[String].isEmpty &&
      //    session.contains("xid") && !session("xid").as[String].isEmpty &&
          session.contains("rt") && !session("rt").as[String].isEmpty &&
      //    session.contains("sxid") && !session("sxid").as[String].isEmpty &&
          session.contains("uidx") && !session("uidx").as[String].isEmpty
    ) {
      exec(session => {
        util.redisUtil.set(
          session.apply("emailId").as[String], FormGenTokenAndLoginUser.formJson(
            session.apply("at").as[String],
            session.apply("rt").as[String],
        //    session.apply("xid").as[String],
        //    session.apply("sxid").as[String],
            session.apply("uidx").as[String]))
        session
      })
    }

    /* ChainBuilder to set session data for a scenario */
    val sessionFromRedis: ChainBuilder =
      exec(session => {

        val spnCount = personalisationUsers
        val emailPrefix = Random.nextInt(count - startUserCount) + 1 + startUserCount    // Random.nextInt(Max-min) + min

        /* Initializing session along with generating random test LT emailId */
        val sessionVar: Session = session.set(
          "emailId", emailPrefix.toString + "@myntra360.com")

        /* Get session data of LT user from redis */
        val convertedObject: JsonObject = new Gson().fromJson(util.redisUtil.get(sessionVar("emailId").as[String])
          , classOf[JsonObject])

        /* Set session data from redis to local session (which will be used inside scenario) */
        if (emailPrefix < spnCount){                                    // 40% of users need to have header "spn"
          sessionVar.set("at", convertedObject.get("at").getAsString)
            .set("rt", convertedObject.get("rt").getAsString)
      //      .set("sxid", convertedObject.get("sxid").getAsString)
      //      .set("xid", convertedObject.get("xid").getAsString)
            .set("uidx", convertedObject.get("uidx").getAsString)
            .set("spn", "true")
        } else {
          sessionVar.set("at", convertedObject.get("at").getAsString)
            .set("rt", convertedObject.get("rt").getAsString)
       //   .set("sxid", convertedObject.get("sxid").getAsString)
       //   .set("xid", convertedObject.get("xid").getAsString)
            .set("uidx", convertedObject.get("uidx").getAsString)
            .set("spn", "false")
        }

      })
  }
}
