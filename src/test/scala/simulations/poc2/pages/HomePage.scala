package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class HomePage {
  val util: HLTUtility = new HLTUtility()

  val homePageHeaders = Map(
    "cache-control" -> "no-cache",
    "accept" -> "application/json",
    "Content-Type" -> "application/json",
    "x-myntra-app" -> "deviceID=devID;appFamily=MyntraRetailAndroid;deviceIP=locationIP;appVersion=4.2001.3;",
    "x-myntra-abtest" -> "lgp.smart.store=inactiveVarB;",
    "at"-> "${at}"
    
  )

  object HomePageAPIs {
    val home: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.home")
    ) {
      exec(http("get home page")
        .get("/lgp/v2.9/stream")
        .headers(homePageHeaders)
        .check(status.is(200)))
    }

    val topnav: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.topnav")
    ) {
      exec(http("get topnav")
        .get("/lgp/v2.9/stream/topnav")
        .headers(homePageHeaders)
        .check(status.is(200)))
    }

    val recommended: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.recommended")
    ) {
      exec(http("get recommended")
        .get("/lgp/v2.9/stream/recommended")
        .headers(homePageHeaders)
        .check(status.is(200)))
    }
    
    
     val men: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.men")
    ) {
      exec(http("get men")
        .get("/lgp/v2.9/stream/men")
        .headers(homePageHeaders)
        .check(status.is(200)))
    }
     
      val women: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.women")
    ) {
      exec(http("get women")
        .get("/lgp/v2.9/stream/women")
        .headers(homePageHeaders)
        .check(status.is(200)))
    }
      
      val smashbox: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.obs-smashbox")
    ) {
      exec(http("get biro smashbox")
        .get("/biro/ios/obs-smashbox")
        .headers(homePageHeaders)
        .header("x-location-context", "pincode=560035; source=IP")
        .check(status.is(200)))
    }
      
      val mall: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.m-mall")
    ) {
      exec(http("get biro m-mall")
        .get("/biro/ios/m-mall")
        .headers(homePageHeaders)
        .header("x-location-context", "pincode=560035; source=IP")
        .check(status.is(200)))
    }
      

    val biro: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.biro")
    ) {
      exec(http("Biro")
        .get("/biro/android/kurta-store")
        .header("Accept-Encoding", "gzip")
        .headers(homePageHeaders)
        .check(status.is(200)))
    }

    val getLoyaltyBalance: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.getLoyaltyBalance")
    ) {
      exec(http("Get Loyalty Balance")
        .get("/v1/user/loyaltypoints/balance")
        .headers(homePageHeaders)
        .check(status.is(200)))
    }
    
    val GetScratchCard: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.GetScratchCard")
    ) {
      exec(http("GetScratchCard")
        .get("/v2/user/shoutearn/scratchcards")
        .header("accept","application/json")
        .header("at", "${at}")
        .check(status.is(200)))
    }
    
    val Referralcode: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.Referralcode")
    ) {
      exec(http("Referralcode")
        .get("/v1/user/shoutearn/referralcode")
        .header("accept","application/json")
        .header("at", "${at}")
        .check(status.is(200)))
    }
    
    val fetchSlot: ChainBuilder = doIf(
      _ => util.getAPIStatus("HomePage.fetchSlot")
    ) {
      exec(http("fetchSlot")
        .get("https://developer.myntra.com/admission/slot?context=demo")
        .header("accept","application/json")
        .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
        .header("content-type","application/json")
        .header("at", "${at}")
        .header("X-SLOT-MODE","enabled")
        .header("X-MYNTRA-APP","appFamily=MyntraRetailAndroid; appVersion=3.31.2-QA-0; appBuild=110241; deviceCategory=Phone; osVersion=9; sdkVersion=28; deviceID=840ce7d97e5c4b43; installationID=5908e9a7-7f88-3153-a987-589ac2dd75e8; sessionID=b28a2901-c76b-42a5-8de0-ae824699f633-840ce7d97e5c4b43;customerID=bbded8a1.d084.4353.92bc.46125d3accb7NwgE4Zqxi2;advertisingID=7dad02bd-43fe-45ce-9a4b-12b3173f2e31;advertisingIDKeySpace=GAID;bundleVersion=1.0.0;")
        .check(status.is(200)))
    }
  }
}



