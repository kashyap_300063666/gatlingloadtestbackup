package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class WebPage {

  val util: HLTUtility = new HLTUtility()
  
  val MyntrawebHeaders = Map(
    "content-type" -> "application/json",
    "at" -> "${at}",
    "User-Agent" -> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36",
    "cookie" -> "noac=1"
  )
  
   val MyntrapwaHeaders = Map(
    "content-type" -> "application/json",
    "at" -> "${at}",
    "User-Agent" -> "Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Mobile Safari/537.36",
    "cookie" -> "noac=1"
  )

  object WebPageAPIs {

    val Myntraweb: ChainBuilder = doIf(
      session => util.getAPIStatus("WebPage.Myntraweb")) {
        exec(http("WebPage : Myntraweb")
          .get("https://www.myntra.com/?loadtestweb=1")
          .headers(MyntrawebHeaders)
          .check(status.is(200)))
      }
    
    val MyntrawebWithQueryParam: ChainBuilder = doIf(
      session => util.getAPIStatus("WebPage.MyntrawebWithQueryParam")) {
        exec(http("WebPage : MyntrawebWithQueryParam")
          .get("https://www.myntra.com/${queryParam}?loadtestweb=2")
          .headers(MyntrawebHeaders)
          .check(status.is(200)))
      }
    
    val MyntrawebWithStyleID: ChainBuilder = doIf(
      session => util.getAPIStatus("WebPage.MyntrawebWithStyleID")) {
        exec(http("WebPage : MyntrawebWithStyleID")
          .get("https://www.myntra.com/${styleId}?loadtestweb=3")
          .headers(MyntrawebHeaders)
          .check(status.is(200)))
      }
    
    val Myntrapwa: ChainBuilder = doIf(
      session => util.getAPIStatus("WebPage.Myntrapwa")) {
        exec(http("WebPage : Myntrapwa")
          .get("https://www.myntra.com/?loadtestpwa=1")
          .headers(MyntrapwaHeaders)
          .check(status.is(200)))
      }
    
    val MyntrapwaWithQueryParam: ChainBuilder = doIf(
      session => util.getAPIStatus("WebPage.MyntrapwaWithQueryParam")) {
        exec(http("WebPage : MyntrapwaWithQueryParam")
          .get("https://www.myntra.com/${queryParam}?loadtestpwa=2")
          .headers(MyntrapwaHeaders)
          .check(status.is(200)))
      }
    
    val MyntrapwaWithStyleID: ChainBuilder = doIf(
      session => util.getAPIStatus("WebPage.MyntrapwaWithStyleID")) {
        exec(http("WebPage : MyntrapwaWithStyleID")
          .get("https://www.myntra.com/${styleId}?loadtestpwa=3")
          .headers(MyntrapwaHeaders)
          .check(status.is(200)))
      }

  }

}