package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class ListPage {
  val util: HLTUtility = new HLTUtility()

  val lpHeaders = Map(
    "User-Agent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "content-type"-> "application/json",
    "at"-> "${at}"
  )

  object Search {
    val paginationMTV1: ChainBuilder = doIf(
      session =>
        util.getAPIStatus("ListPage.paginationMTV1") &&
          session.contains("queryParam") &&
          session.contains("pageNos")
    ) {
     
        exec(http("Search(v1) Scroll")
          .get("/v1/search/${queryParam}?rows=48&o=0&p=${pageNos}&searchRequestId=this-is-a-load-tests-specific-req-id")
          .headers(lpHeaders)
          .header("Cookie", session => {          // set "spn" cookie only when needed
            if(session("spn").as[String] == "true") {
              "spn=" + session("spn").as[String]
            } else {
              "at=" + session("at").as[String]
            }
          })
          .check(status.is(200)))
    }

    val paginationMTV2: ChainBuilder = doIf(
      session =>
        util.getAPIStatus("ListPage.paginationMTV2") &&
          session.contains("queryParam") &&
          session.contains("pageNos")
    ) {
      //repeat(session => session("pageNos").validate[Int], "scrollToPage") {
        exec(http("Search(v2) Scroll")
          .get("/v2/search/${queryParam}?rows=48&o=0&p=${pageNos}&searchRequestId=this-is-a-load-tests-specific-req-id")
          .headers(lpHeaders)
          .header("Cookie", session => {               // set "spn" cookie only when needed
            if(session("spn").as[String] == "true") {
              "spn=" + session("spn").as[String]
            } else {
              "at=" + session("at").as[String]
            }
          })
          .check(status.is(200)))
     // }
    }

    val filtersV1: ChainBuilder = doIf(
      session =>
        util.getAPIStatus("ListPage.filtersV1") &&
          session.contains("queryParam") &&
          session.contains("filterquery")
    ) {
      exec(http("Search(v1) With Sort")
        .get("/v1/search/${queryParam}")
        .queryParam("userQuery","true")
        .queryParam("rows","48")
        .queryParam("request_id","null")
        .queryParam("${filterquery}","undefined")
        .queryParam("searchRequestId","this-is-a-load-tests-specific-req-id")
        .headers(lpHeaders)
        .header("Cookie", session => {                // set "spn" cookie only when needed
          if(session("spn").as[String] == "true") {
            "spn=" + session("spn").as[String]
          } else {
            "at=" + session("at").as[String]
          }
        })
        .check(status.is(200)))
    }

    val filtersV2: ChainBuilder = doIf(
      session =>
        util.getAPIStatus("ListPage.filtersV2") &&
          session.contains("queryParam") &&
          session.contains("filterquery")
    ) {
      exec(http("Search(v2) With Sort")
        .get("/v2/search/${queryParam}?userQuery=true&rows=48&request_id=null&${filterquery}&searchRequestId=this-is-a-load-tests-specific-req-id")
        .headers(lpHeaders)
        .header("Cookie", session => {
          if(session("spn").as[String] == "true") {
            "spn=" + session("spn").as[String]
          } else {
            "at=" + session("at").as[String]
          }
        })
        .check(status.is(200)))
    }

    val autoSuggest: ChainBuilder = doIf(
      session =>
        util.getAPIStatus("ListPage.autoSuggest") &&
          session.contains("queryParam")
    ) {
      exec(http("Get Suggestions")
        .get("/search/v1/autosuggest")
        .queryParam("q","${queryParam}")
        .headers(lpHeaders)
        .header("Cookie", session => {
          if(session("spn").as[String] == "true") {
            "spn=" + session("spn").as[String]
          } else {
            "at=" + session("at").as[String]
          }
        })
        .check(status.is(200)))
    }

    val clusterV2: ChainBuilder = doIf(
      session =>
        util.getAPIStatus("ListPage.clusterV2") &&
          session.contains("queryParam")
    ) {
      exec(http("Get List (V2 cluster)")
        .get("/v2/search/${queryParam}")
        .queryParam("requestType","CLUSTER")
        .queryParam("rows","48")
        .queryParam("clusterRows","1")
        .queryParam("searchRequestId","this-is-a-load-tests-specific-req-id")
        .header("x-myntra-abtest", "productclusters=showbottomoptions")
        .headers(lpHeaders)
        .header("Cookie", session => {
          if(session("spn").as[String] == "true") {
            "spn=" + session("spn").as[String]
          } else {
            "at=" + session("at").as[String]
          }
        })
        .check(status.is(200)))
    }
  }
}
