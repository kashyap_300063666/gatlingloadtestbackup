package simulations.poc2.pages

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class Login {

  private val sessionToRedis = new SessionSetUp().SessionObj.storeSessionDataInRedis

  object LoginAPIs {
    val tokenApi: ChainBuilder = exec(http("knuthToken")
      .get("/auth/v1/token")
      .header("clientid", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
      .header("x-myntra-app", "appFamily=MyntraRetailAndroid;appVersion=4.2009.2")
      .check(status.is(200))
      .check(header("At").exists.saveAs("at"))
     // .check(header("Xid").exists.saveAs("xid"))
    )

    val loginApi: ChainBuilder = doIf(
      session =>
        session.contains("at") &&             //  Execute Login only when "at", "xid" and "emailId" are present
          //session.contains("xid") &&
          session.contains("emailId")
    ) {
      exec(http("login using gen token ")
        .post("/v1/auth/login")
        .header("at", "${at}")
     //   .header("xid", "${xid}")
        .header("x-myntra-app", "appFamily=MyntraRetailAndroid;appVersion=4.2009.2")
        .body(StringBody("""{"userId":"${emailId}","accessKey":"${emailId}"}""")).asJson
        .check(status.is(200))
        .check(header("Rt").exists.saveAs("rt"))
        .check(header("At").exists.saveAs("at"))
     //   .check(header("Sxid").exists.saveAs("sxid"))
        .check(jsonPath("$.uidx").exists.saveAs("uidx"))
      )
        .exec(sessionToRedis)                   // Save Tokens to redis
    }
  }
}
