package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class UserAPIs {
  val util: HLTUtility = new HLTUtility()

  val userHeaders = Map(
    "content-type"-> "application/json",
    "at"-> "${at}"
  )

  object WishList {

    val addToWishList: ChainBuilder = doIf(
      session =>
        util.getAPIStatus("UserAPIs.addToWishList") &&
          session.contains("styleId") &&
          session.contains("skuiId")
    ){
      exec(http("add Style to WishList")
        .post("/v1/user/wishlist/add")
        .headers(userHeaders)
        .body(ElFileBody("hlt-test-data/wishList.json"))
        .check(status.is(200))
      )
    }

    val getWishListV2: ChainBuilder = doIf(
      _ => util.getAPIStatus("UserAPIs.getWishListV2")
    ){
      exec(http("Get WishList V2")
        .get("/v2/user/wishlist?offset=1&size=20")
        .headers(userHeaders)
        .check(status.is(200))
      )
    }
    
    val wishListSummary: ChainBuilder = doIf(
      _ => util.getAPIStatus("UserAPIs.wishListSummary")
    ){
      exec(http("Get wishListSummary")
        .get("/v1/user/wishlist/summary")
        .header("User-Agent","MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
        .headers(userHeaders)
        .check(status.is(200))
      )
    }

  }

}
