package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class AddressPage {
  val util: HLTUtility = new HLTUtility()

  object Address {
    val addNewAddressAddress: ChainBuilder = doIf(
      session => util.getAPIStatus("AddressPage.addNewAddressAddress")) {
        exec(http("AddressPage:addNewAddressAddress")
          .post("/v2/addresses")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("Content-Type", "application/json")
          .header("at", "${at}")
          .body(ElFileBody("hlt-test-data/addNewAddress.json")).asJson
          .check(status.is(200))
          .check(jsonPath("$.id").findAll.saveAs("addressId")))
      }

    val checkoutAddress: ChainBuilder = doIf(
      session => util.getAPIStatus("AddressPage.checkoutAddress")) {
        exec(http("AddressPage:checkoutAddress")
          .get("/v2/addresses")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("Content-Type", "application/json")
          .header("at", "${at}")
          .check(status.is(200))
          .check(jsonPath("$.addresses[?(@.pincode=='999002')].id").find(0).saveAs("addressId")))
      }

    val checkoutAddressNode: ChainBuilder = doIf(
      session => util.getAPIStatus("AddressPage.checkoutAddressNode")) {
        exec(http("AddressPage:checkoutAddressNode")
          .get("https://www.myntra.com/checkout/address")
          .header("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.1; en-us; GT-N7100 Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30")
          .header("Content-Type", "application/json")
          .header("Cookie", "at=${at};rt=${rt};ilgim=true")
          .check(status.in(200, 302)))
      }
   
    val pinCodeService: ChainBuilder = doIf(
      session => util.getAPIStatus("AddressPage.pinCodeService")) {
        exec(http("AddressPage:pinCodeService")
          .get("https://secure.myntra.com/myntapi/pincode/${pincode}")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("Content-Type", "application/json")
          .header("at", "${at}")
          .check(status.is(200)))
      }

    val selectAddress: ChainBuilder = doIf(
      session => util.getAPIStatus("AddressPage.selectAddress")) {
        exec(http("AddressPage:selectAddress")
          .put("/v1/cart/default/orderaddress/${addressId}")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("Content-Type", "application/json")
          .header("at", "${at}")
          .header("pagesource", "address")
          .check(status.is(200)))
      }

  }
}