package simulations.selections

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import io.gatling.jdbc.Predef._

import scala.sys.process.processInternal


class IPLoadTest extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileNameFbm = "SampleReplenishmentManualSJITFBM.xlsx"
  val filePathFbm: String = feederUtil.getFileDownloadedPath(containerName, fileNameFbm)
  val fileNameNonFbm = "SampleReplenishmentManualSJITNonFBM.xlsx"
  val filePathNonFbm: String = feederUtil.getFileDownloadedPath(containerName, fileNameNonFbm)
  val fileNameMmbrp = "testmmbrp_1rows.xlsx"
  val filePathMmbrp: String = feederUtil.getFileDownloadedPath(containerName, fileNameMmbrp)

  private val ipBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.INVENTORY_PLANNER.toString)).disableWarmUp
  private val ipHeaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "content-type" -> "multipart/form-data")


  private val uploadManualSampleBAU = scenario("ManualuploadSampleBAU")
      .exec(actionBuilder = http("uploadSampleBAU")
      .post("/inventory-planner-service/inventory-planner/forecast/manual-replenish/BAU/")
      .headers(ipHeaders)
      .formUpload("file", filePathFbm)
        .check(status.is(200)))

    private val updateManualSampleFileHRD = scenario("ManualuploadSampleHRD")
    .exec(actionBuilder = http("updateSampleFileHRD")
      .post("/inventory-planner-service/inventory-planner/forecast/manual-replenish/HRD/")
      .headers(headers)
      .headers(ipHeaders)
      .formUpload("file", filePathNonFbm)
      .check(status.is(200)))

  private val uploadAutoSampleBAU = scenario("AutouploadSampleBAU")
    .exec(actionBuilder = http("uploadSampleBAU")
      .get("/inventory-planner-service/inventory-planner/forecast/auto/BAU/")
      .headers(headers)
      .headers(ipHeaders)
      .check(status.is(200)))

  private val updateAutoSampleFileHRD = scenario("AutouploadSampleHRD")
    .exec(actionBuilder = http("updateSampleFileHRD")
      .get("/inventory-planner-service/inventory-planner/forecast/auto/HRD/")
      .headers(headers)
      .headers(ipHeaders)
      .check(status.is(200)))

  private val uploadMMBFile=scenario("MMBFileUpload")
     .exec(actionBuilder = http("MMBFileUploadIP")
      .post("/inventory-planner-service/inventory-planner/inventory-planner-task/trigger-replenishment/MMB/")
       .headers(ipHeaders)
       .bodyPart(RawFileBodyPart("file" , filePathMmbrp)).asJson
    .bodyPart(StringBodyPart(
         "fileNameWithExtension" , "Test")).asJson
       .bodyPart(StringBodyPart(
         "businessUnit" , "Women's Western Wear")).asJson
       .bodyPart(StringBodyPart(
         "taskName" , "load_test")).asJson
       .bodyPart(StringBodyPart(
         "buyerId" , "3974")).asJson
       .bodyPart(StringBodyPart(
         "commercialType" , "OUTRIGHT")).asJson
       .bodyPart(StringBodyPart(
         "userEmail" , "abhishek.mishra1@myntra.com")).asJson
         .bodyPart(StringBodyPart(
           "brandGroup" , "Belle Fille")).asJson
       .bodyPart(StringBodyPart(
         "planType" , "MANUAL")).asJson
       .bodyPart(StringBodyPart(
         "additionalClassification" , "NOT_APPLICABLE")).asJson
       .bodyPart(StringBodyPart(
         "additionalClassificationId" , "97")).asJson
       .check(status.is(200)))

  private val uploadMFBFile=scenario("MFBFileUpload")
    .exec(actionBuilder = http("MFBFileUpload")
      .post("/inventory-planner-service/inventory-planner/inventory-planner-task/trigger-replenishment/MFB/")
      .headers(ipHeaders)
      .bodyPart(RawFileBodyPart("file" , filePathMmbrp)).asJson
      .bodyPart(StringBodyPart(
        "fileNameWithExtension" , "Test")).asJson
      .bodyPart(StringBodyPart(
        "taskName" , "load_test")).asJson
      .bodyPart(StringBodyPart(
        "buyerId" , "3974")).asJson
      .bodyPart(StringBodyPart(
        "commercialType" , "OUTRIGHT")).asJson
      .bodyPart(StringBodyPart(
        "userEmail" , "abhishek.mishra1@myntra.com")).asJson
      .bodyPart(StringBodyPart(
        "planType" , "MANUAL")).asJson
      //.check(jsonPath("data.workflowJobStatus").not("IN_PROGRESS"))
      .check(status.is(200)))

 // private val jobTracker=scenario("getJobtrackerStatus")
   // .exec(actionBuilder = http("getJobtrackerStatus")
        //  .get("/http://jobtracker.stage.myntra.com/job-tracker-service/jobtracker/job/{jobtrackerJobId}/")
       //  .headers(ipHeaders)
      //  .check(jsonPath("data.status").not("IN_PROGRESS"))
     //   .check(status.is(200)))



  //val scn1=scenario("scenario2").exec(uploadMMBFile).exec(jobTracker)
    //.exec("getJobtrackerStatuss")

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(ipBaseURL)
}
