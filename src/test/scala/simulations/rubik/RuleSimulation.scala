package simulations.rubik

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.rubik.rubik.RedeemRequest
import com.myntra.rubik.rubik.RubikServiceGrpc
import io.gatling.core.Predef.scenario
import io.gatling.core.session.Expression
import io.grpc.ManagedChannelBuilder
import simulations.BaseSimulation

class RuleSimulation extends BaseSimulation {
  private val baseUrl = System.getProperty("singleServerURL")
  var grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.RUBIK.toString,""), grpcPortNum).usePlaintext())

  if(baseUrl != null) {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(baseUrl, grpcPortNum).usePlaintext())
  }

  val redeemRequest: Expression[RedeemRequest] = RedeemRequest(tenant = "insider", offerIds = List(2433, 2434, 2435,
    2443, 2448, 2450, 2453, 2462, 2467, 2474, 2088, 2090, 2092, 2094, 2096), userId = "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")

  // FilterRedeemable
  val filterRedeemable = scenario("filter_redeemable").exec(grpc("FetchAvailableOffersByUser")
      .rpc(RubikServiceGrpc.METHOD_FILTER_REDEEMABLE).payload(redeemRequest))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)
}
