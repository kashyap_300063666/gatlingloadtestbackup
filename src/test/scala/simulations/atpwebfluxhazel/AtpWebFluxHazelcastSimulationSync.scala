package simulations.atpwebfluxhazel

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.http
import simulations.BaseSimulation
import simulations.hazelcast.HazelCastUtil

class AtpWebFluxHazelcastSimulationSync extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "wms"
  /* Name of File to be downloaded */
  val fileName = "vertx-hz-inv.csv"
  private val url = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.ATP_VERTX_REDIS.toString)).disableWarmUp

  //private val csvFile = System.getProperty("dataprovider", "test-data/vertx-hz-inv.csv")
  val csvFile: String = System.getProperty("dataprovider", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  private val csvfeeder = csv(filePath).circular
  private val pagecall = new HazelCastUtil().Api

  private val preRequisiteScenario =
    scenario("Pre Requisite Script")
      .feed(csvfeeder)
      .exec(
        pagecall.getWebFluxCall2
      )
//
//  setUp(scenarios map (e => e.inject(constantUsersPerSec(noOfUsers) during (duration))) toList).throttle(
//    reachRps(noOfUsers / 2) in ((duration * 10) / 100),
//    holdFor((duration * 10) / 100),
//    reachRps(noOfUsers) in ((duration * 20) / 100),
//    holdFor((duration * 60) / 100)
//  ).protocols(url)

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(url)
}
