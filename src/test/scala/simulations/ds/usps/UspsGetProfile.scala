package simulations.ds.usps


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation


class UspsGetProfile extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "datascience"
  /* Name of File to be downloaded */
  val fileName = "usps_user_data.json"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val uspsBaseUrl = http.baseUrl  (BaseUrlConstructor.getBaseUrl(ServiceType.USPS_DS_SERVICE.toString)).disableWarmUp

  private val uspsheaders = Map( "content-type" -> "application/json")




  private var profileFeederMap = jsonFile(filePath).circular

  private val usps_get_profiles_api =
    scenario("usps get profiles service")
      .feed(profileFeederMap)
      .exec(http("usps get profiles service")
        .get("/user/profile/${uidx}")
          .queryParam("gender","${gender}")
          .queryParam("articleType", "${articleType}")
        .headers(uspsheaders)
        .check(status.is(200)))

}