package simulations.ds.usps


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation


class SizingUspsSimulation extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "datascience"
  /* Name of File to be downloaded */
  val fileName = "profile_data.json"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val fileNameProfileOrder = "profile_data.json"
  val filePathProfileOrder: String = feederUtil.getFileDownloadedPath(containerName, fileNameProfileOrder)
  val fileNameUsps = "usps_gats.json"
  val filePathUsps: String = feederUtil.getFileDownloadedPath(containerName, fileNameUsps)
  val fileNameUspsUserData = "usps_user_data.json"
  val filePathUspsUserData: String = feederUtil.getFileDownloadedPath(containerName, fileNameUspsUserData)

  private val uspsBaseUrl = http.baseUrl  (BaseUrlConstructor.getBaseUrl(ServiceType.USPS_DS_SERVICE.toString)).disableWarmUp

  private val uspsheaders = Map( "content-type" -> "application/json")

  private var feederMap = jsonFile(filePathUsps).circular

  private val usps_get_questions_api =
    scenario("usps get questions service")
        .feed(feederMap)
        .exec(http("usps get questions service")
        .post("/v1/recommendations/apparel/questions/${gender}/${articleType}")
        .headers(uspsheaders)
        .body(StringBody("""{"uidx":"${uidx}","pidx":"${pidx}","gender":"${gender}","articleType":"${articleType}"}""")).asJson
        .check(status.is(200)))

  private var profileFeederMap = jsonFile(filePathUspsUserData).circular

  private val usps_get_profiles_api =
    scenario("usps get profiles service")
      .feed(profileFeederMap)
      .exec(http("usps get profiles service")
        .get("/user/profile/${uidx}")
          .queryParam("gender","${gender}")
          .queryParam("articleType", "${articleType}")
        .headers(uspsheaders)
        .check(status.is(200)))

  private var profileCreationFeederMap = jsonFile(filePath).circular

  private val usps_create_profile_api =
    scenario("usps_create_profile_api")
      .feed(profileCreationFeederMap)
      .exec(http("usps_create_profile_api")
      .post("/user/profile/${uidx}")
      .headers(uspsheaders)
      .body(StringBody("""{"gender":"${gender}","name":"${name}"}""")).asJson
      .check(status.is(200)))


  private var orderCreationFeederMap = jsonFile(filePathProfileOrder).circular

  private val usps_tag_order_api =
    scenario("usps_tag_order_api")
      .feed(orderCreationFeederMap)
      .exec(http("usps_tag_order_api")
        .post("/user/order")
        .headers(uspsheaders)
        .body(StringBody("""{"orderId":"${orderId}","uidx":"${uidx}","pidx":"${pidx}","gender":"${gender}","articleType":"${articleType}","styleId":${styleId},"skuId":${skuId}}""")).asJson
        .check(status.is(200)))
  setUp(scenarios map (e => e.inject(step)) toList).protocols(uspsBaseUrl)
}
