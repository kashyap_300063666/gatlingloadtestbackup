package simulations.ds.ppr


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation


class PPRSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "datascience"
  /* Name of File to be downloaded */
  val fileName = "ppr_style_uidx.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val pprBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PPR_DS_SERVICE.toString)).disableWarmUp

  private val pprheaders = Map( "content-type" -> "application/json")

  private val styleIdListMyntra= CSVUtils.readCSV2(filePath, false).map(e => e.apply(0))

  private val uidxListMyntra= CSVUtils.readCSV2(filePath, false).map(e => e.apply(1))

  private val feederMap = (styleIdListMyntra zip uidxListMyntra).map(e => Map("styleId"->e._1, "uidx"->e._2)).circular


  private val ppr_mix_users =
    scenario("mix_users")
      .feed(feederMap)
      .exec(http("mix_users")
        .post("/pprservice/v1/recommendations")
        .headers(pprheaders)
        .body(StringBody("""{ "styleIds": [${styleId}], "recommendationType": ["DSRECOMMENDATIONS"], "storeId": 2297, "uidx": "${uidx}", "personalisation": true }""")).asJson
        .check(status.is(202)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(pprBaseUrl)

}
