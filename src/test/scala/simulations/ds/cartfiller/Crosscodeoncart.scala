package simulations.ds.cartfiller


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random


class Crosscodeoncart extends BaseSimulation {

  private val cfBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.CF_DS_SERVICE.toString)).disableWarmUp

  private val cfheaders = Map( "content-type" -> "application/json")


  private val cf_single_user =
    scenario("single_user")
      .exec(http("single_user")
        .post("/api/v1/cartfiller")
        .headers(cfheaders)
        .body(StringBody("""{"uidx":null,"items":[{"styleId":11066462,"skuId":38178976,"price":987.0,"brand":"Libas","articleTypeId":543,"gender":"Women","size":"S","addedTime":1583752502,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10986580,"skuId":37873552,"price":929.0,"brand":"Anouk","articleTypeId":543,"gender":"Women","size":"S","addedTime":1583752735,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10338731,"skuId":35294361,"price":962.0,"brand":"Janasya","articleTypeId":543,"gender":"Women","size":"S","addedTime":1583752824,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10780284,"skuId":37029584,"price":1207.0,"brand":"ZIYAA","articleTypeId":543,"gender":"Women","size":"S","addedTime":1583753685,"wareHouseIds":[0],"wareHouseId":0},{"styleId":7606787,"skuId":32047283,"price":704.0,"brand":"Vishudh","articleTypeId":543,"gender":"Women","size":"S","addedTime":1583846032,"wareHouseIds":[0],"wareHouseId":0},{"styleId":9157673,"skuId":33845532,"price":779.0,"brand":"Vishudh","articleTypeId":543,"gender":"Women","size":"M","addedTime":1583846234,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10913798,"skuId":37567052,"price":2349.0,"brand":"Vishudh","articleTypeId":543,"gender":"Women","size":"S","addedTime":1583846273,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10653244,"skuId":36536284,"price":3599.0,"brand":"Libas","articleTypeId":543,"gender":"Women","size":"S","addedTime":1583846300,"wareHouseIds":[0],"wareHouseId":0},{"styleId":9197015,"skuId":33897170,"price":1199.0,"brand":"Anouk","articleTypeId":543,"gender":"Women","size":"S","addedTime":1584561212,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10356725,"skuId":35360991,"price":1223.0,"brand":"Bhama Couture","articleTypeId":543,"gender":"Women","size":"S","addedTime":1584562730,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10977942,"skuId":37840832,"price":3250.0,"brand":"clorals","articleTypeId":543,"gender":"Women","size":"L","addedTime":1584562917,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10841992,"skuId":37272292,"price":1399.0,"brand":"Libas","articleTypeId":543,"gender":"Women","size":"XS","addedTime":1592373953,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10841890,"skuId":37271702,"price":2999.0,"brand":"Libas","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592374035,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10653288,"skuId":36536546,"price":2799.0,"brand":"Libas","articleTypeId":543,"gender":"Women","size":"XS","addedTime":1592374092,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10653288,"skuId":36536548,"price":2799.0,"brand":"Libas","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592374100,"wareHouseIds":[0],"wareHouseId":0},{"styleId":7428887,"skuId":31737330,"price":899.0,"brand":"Vishudh","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592374148,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10560854,"skuId":36189570,"price":1699.0,"brand":"Sangria","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592374171,"wareHouseIds":[0],"wareHouseId":0},{"styleId":8448511,"skuId":32998562,"price":699.0,"brand":"DressBerry","articleTypeId":66,"gender":"Women","size":"Onesize","addedTime":1592374233,"wareHouseIds":[0],"wareHouseId":0},{"styleId":9869861,"skuId":34536703,"price":1154.0,"brand":"all about you","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592374308,"wareHouseIds":[0],"wareHouseId":0},{"styleId":9869883,"skuId":34536770,"price":1499.0,"brand":"all about you","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592374358,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10407069,"skuId":35561361,"price":1224.0,"brand":"Nayo","articleTypeId":543,"gender":"Women","size":"XS","addedTime":1592375139,"wareHouseIds":[0],"wareHouseId":0},{"styleId":11080108,"skuId":38225462,"price":1537.0,"brand":"Ahalyaa","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592375848,"wareHouseIds":[0],"wareHouseId":0},{"styleId":11250618,"skuId":38918116,"price":1499.0,"brand":"Shree","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592375907,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10848100,"skuId":37294620,"price":1049.0,"brand":"Shree","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592375930,"wareHouseIds":[0],"wareHouseId":0},{"styleId":10720564,"skuId":36796552,"price":1367.0,"brand":"Ahalyaa","articleTypeId":543,"gender":"Women","size":"S","addedTime":1592376128,"wareHouseIds":[0],"wareHouseId":0}],"cartPrice":39810.0,"freeShippingThreshold":799.0,"coupons":[]}""")).asJson
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(cfBaseUrl)

}
