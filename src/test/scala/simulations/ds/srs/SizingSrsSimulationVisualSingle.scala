package simulations.ds.srs


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation


class SizingSrsSimulationVisualSingle extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "datascience"
  /* Name of File to be downloaded */
  val fileName = "srs_data.json"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val srsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.SRS_DS_SERVICE_SINGLE.toString)).disableWarmUp

  private val srsheaders = Map( "content-type" -> "application/json", "x-myntra-abtest" -> "sizereco.confidencelevel=visual_percentage")

  private var feederMap = jsonFile(filePath).circular

  private val srs_api =
    scenario("srs service")
        .feed(feederMap)
        .exec(http("srs service")
        .post("/api/v1/recommend/")
        .headers(srsheaders)
        .body(StringBody("""{"pidx":"${pidx}", "uidx":"${uidx}","articleType":"${articleType}", "availableStyleSkuMap": {"${style}":1}}""")).asJson
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).protocols(srsBaseUrl)

}