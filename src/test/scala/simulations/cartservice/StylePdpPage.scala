package simulations.cartservice

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef.{exec, jsonPath, _}
import io.gatling.http.Predef.{http, status}
import io.gatling.http.Predef._

class StylePdpPage {

//  private val baseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)).disableWarmUp
  val pdpHeaders = Map(
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "content-type"-> "application/json"
  )

  object StyleDetails {
    val styleData = exec(http("Get SkuId from style Id")
      .get("https://api.myntra.com/product/${styleId}?co=1")
      .headers(pdpHeaders)
      .check(status.is(200))
      .check(jsonPath("$..sizes[?(@.inventory >= 1)].skuId").findAll.saveAs("skuIdsList")))
  }

}
