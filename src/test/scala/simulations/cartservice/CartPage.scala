package simulations.cartservice

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.http.Predef.{http, status}
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.Console.println


class CartPage {

  val cartHeaders = Map(
    "accept" -> "application/json",
    "content-type"-> "application/json",
    "x-mynt-ctx"-> "storeId=2297;nidx=${nidx};uidx=${uidx};"
  )

  object CartApis {

    val clearCartByUIDX = {
      exec(http("clearCartByUIDX")
        .delete("/myntra-absolut-service/securedcart/v2/user/${uidx}/default")
        .headers(cartHeaders)
        .check(status.is(200)))
    }

    val addToCart = {
      exec(http("addToCart")
        .put("/myntra-absolut-service/cart/v2/default/add")
        .headers(cartHeaders)
        .body(StringBody("""{"styleId":${styleId},"skuId": ${skuIdsList.random()},"quantity":1}""")).asJson
        .check(status.is(200)))
    }

    val printInputData = {
      exec((session: Session) => {
        println("nidx = " + session("nidx").as[String])
        println("uidx = " + session("uidx").as[String])
        println("styleId = " + session("styleId").as[String])
        println("skuId = " + session("skuIdsList").as[Vector[String]])
        session
      })
    }

    val printOutputData = {
      exec((session: Session) => {
        println("xMyntCtx = " + "storeId=2297;nidx=" + session("nidx").as[String] +
          ";uidx=" + session("uidx").as[String] + ";")
        println("cartId = " + session("cartId").as[String])
        session
      })
    }
    val getCart = {
      exec(http("getCart")
        .get("/myntra-absolut-service/cart/v2/default")
        .headers(cartHeaders)
        .check(status.is(200))
        .check(jsonPath("$.id").saveAs("cartId")))
    }
  }
}
