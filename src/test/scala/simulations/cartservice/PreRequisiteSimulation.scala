package simulations.cartservice

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil, HLTUtility}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import java.util.UUID.randomUUID

import scala.Console.println
import scala.util.Random

class PreRequisiteSimulation extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "cartpayments"
  /* Name of File to be downloaded */
  val fileName = "cartUsers.csv"

  private val cartBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.CARTSERVICEV2.toString)).disableWarmUp

  /* Steps to be executed before running load tests */
  val util = new HLTUtility()
  util.downloadFile("styles.csv")
  private val csvData: List[String] = CSVUtils.readCSV2("styles.csv", isIncludeHeader = false)
    .map(e => e.apply(0)).toList
  private val numberOfStyles: Int = csvData.length
  val styleDetailsFeeder: Iterator[Map[String, String]] =
    Iterator.continually(
      Map(
        "styleId" -> csvData.apply(Random.nextInt(numberOfStyles))
      )
    )
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val userDetailsFeeder = csv(filePath).queue

  //Redis implementation
  private val counterKeyName = "cartData_"
  before {
    util.redisUtil.set(counterKeyName, "0")
  }
  val nidxFeeder = Iterator.continually(Map("nidx" -> randomUUID.toString))

  /* Initialize Page Objects */
  val pdpPageCalls =  new StylePdpPage().StyleDetails
  val cartPage = new CartPage().CartApis

  private val preRequisiteScenario =
    scenario("Pre Requisite Script")
      .feed(userDetailsFeeder)
      .feed(styleDetailsFeeder)
      .feed(nidxFeeder)
      .exec(
        pdpPageCalls.styleData,
        cartPage.clearCartByUIDX,
        cartPage.addToCart,
        cartPage.printInputData,
        cartPage.getCart
      )
      .exec(session => {
        util.redisUtil.set(counterKeyName + util.redisUtil.increamentCounter(counterKeyName).toString,
          "storeId=2297;nidx=" + session("nidx").as[String] + ";uidx=" + session("uidx").as[String] + ";," + session("cartId").as[String])
        session
      })
      .exec(
        cartPage.printOutputData
      )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(cartBaseUrl)
}