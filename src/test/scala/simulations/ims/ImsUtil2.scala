package simulations.ims

import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}


class ImsUtil2 {

  val imsHeaders = Map(
    "accept" -> "application/json",
    "content-type" -> "application/json",
    "Authorization" -> "Basic YWJjOjEyMw==",
    "x-myntra-client-id" -> "ims_load_test"
  )

  val imsHeaders2 = Map(
    "accept" -> "application/json",
    "content-type" -> "application/xml",
    "Authorization" -> "Basic YWJjOjEyMw==",
    "x-myntra-client-id" -> "ims_load_test"
  )

  object ImsApis {

    val imsgetcall = {
      exec(http("imsstoreSellerAPI")
        .put("/myntra-ims-service/imsV2/inventory/v2/storeSeller/warehouse")
        .headers(imsHeaders)
        .body(StringBody(
          """{
              |"storePartnerId":2297,"orderSystem":"OMS","warehouseIds":[],
              |"data":[
                |{"sellerPartnerId":${sellerId1},"skuId":${skuId1},"supplyType":"${supplyType1}"},
                |{"sellerPartnerId":${sellerId2},"skuId":${skuId2},"supplyType":"${supplyType2}"}
              |]
            |}""".stripMargin)).asJson
        .check(status.is(200)))
    }

    val imsblockcall = {
      exec(http("inventoryBlockAPI")
        .put("/myntra-ims-service/imsV2/inventory/v2/block/")
        .headers(imsHeaders)
        .body(StringBody(
          """{"storePartnerId":2297,"orderSystem":"OMS",
              |"data":[
                {"warehouseId":${whId1},"supplyType":"${supplyType1}","sellerPartnerId":${sellerId1},"skuId":${skuId1},"quantity":1},
                |{"warehouseId":${whId2},"supplyType":"${supplyType2}","sellerPartnerId":${sellerId2},"skuId":${skuId2},"quantity":1}
              |]
            |}""".stripMargin)).asJson
        .check(status.is(200)))
    }

    val bulkSellerInventoryUpload = {
      exec(http("/myntra-ims-service/ims/inventory/v2/bulkSellerInventoryUpload")
        .post("/myntra-ims-service/ims/inventory/v2/bulkSellerInventoryUpload")
        .headers(imsHeaders2)
        .body(StringBody(
          """<whSyncInventoryRequest>
            |<data>
            |<whSyncInventory><warehouseId>${whId1}</warehouseId><storePartnerId>2297</storePartnerId><supplyType>ON_HAND</supplyType><sellerPartnerId>${sellerId1}</sellerPartnerId><skuId>${skuId1}</skuId><quantity>10000</quantity><fromQuality>Q1</fromQuality><toQuality>Q1</toQuality><whInventoryOperation>SELLER_ALLOCATE</whInventoryOperation></whSyncInventory>
            |<whSyncInventory><warehouseId>${whId2}</warehouseId><storePartnerId>2297</storePartnerId><supplyType>ON_HAND</supplyType><sellerPartnerId>${sellerId2}</sellerPartnerId><skuId>${skuId2}</skuId><quantity>10000</quantity><fromQuality>Q1</fromQuality><toQuality>Q1</toQuality><whInventoryOperation>SELLER_ALLOCATE</whInventoryOperation></whSyncInventory>
            |<whSyncInventory><warehouseId>${whId3}</warehouseId><storePartnerId>2297</storePartnerId><supplyType>ON_HAND</supplyType><sellerPartnerId>${sellerId3}</sellerPartnerId><skuId>${skuId3}</skuId><quantity>10000</quantity><fromQuality>Q1</fromQuality><toQuality>Q1</toQuality><whInventoryOperation>SELLER_ALLOCATE</whInventoryOperation></whSyncInventory>
            |<whSyncInventory><warehouseId>${whId4}</warehouseId><storePartnerId>2297</storePartnerId><supplyType>ON_HAND</supplyType><sellerPartnerId>${sellerId4}</sellerPartnerId><skuId>${skuId4}</skuId><quantity>10000</quantity><fromQuality>Q1</fromQuality><toQuality>Q1</toQuality><whInventoryOperation>SELLER_ALLOCATE</whInventoryOperation></whSyncInventory>
            |<whSyncInventory><warehouseId>${whId5}</warehouseId><storePartnerId>2297</storePartnerId><supplyType>ON_HAND</supplyType><sellerPartnerId>${sellerId5}</sellerPartnerId><skuId>${skuId5}</skuId><quantity>10000</quantity><fromQuality>Q1</fromQuality><toQuality>Q1</toQuality><whInventoryOperation>SELLER_ALLOCATE</whInventoryOperation></whSyncInventory>
            |<whSyncInventory><warehouseId>${whId6}</warehouseId><storePartnerId>2297</storePartnerId><supplyType>ON_HAND</supplyType><sellerPartnerId>${sellerId6}</sellerPartnerId><skuId>${skuId6}</skuId><quantity>10000</quantity><fromQuality>Q1</fromQuality><toQuality>Q1</toQuality><whInventoryOperation>SELLER_ALLOCATE</whInventoryOperation></whSyncInventory>
            |<whSyncInventory><warehouseId>${whId7}</warehouseId><storePartnerId>2297</storePartnerId><supplyType>ON_HAND</supplyType><sellerPartnerId>${sellerId7}</sellerPartnerId><skuId>${skuId7}</skuId><quantity>10000</quantity><fromQuality>Q1</fromQuality><toQuality>Q1</toQuality><whInventoryOperation>SELLER_ALLOCATE</whInventoryOperation></whSyncInventory>
            |</data>
            |</whSyncInventoryRequest>""".stripMargin)).asJson
        .check(status.is(200)))
    }
  }
}


