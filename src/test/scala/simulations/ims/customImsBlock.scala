package simulations.ims

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.http
import simulations.BaseSimulation

class customImsBlock extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "wms"
  /* Name of File to be downloaded */
  val fileName = "ims.csv"

  private val imsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.IMSSYNC.toString)).disableWarmUp

  //val csvFile = System.getProperty("imsdataprovider", "test-data/ims.csv")
  val csvFile = System.getProperty("imsdataprovider", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  //val csvfeeder = csv(csvFile).circular
  val csvfeeder = csv(filePath).circular
  val imspageCalls = new ImsUtil().ImsApis
  val imspageCalls2 = new ImsUtil2().ImsApis
  val imspageCalls3 = new ImsUtil3().ImsApis
  val imspageCalls4 = new ImsUtil4().ImsApis
  val imspageCalls5 = new ImsUtil5().ImsApis

  private val preRequisiteScenario =
    scenario("Pre Requisite Script")
      .feed(csvfeeder)
      .exec(
        imspageCalls.imsblockcall,
        imspageCalls2.imsblockcall,
        imspageCalls3.imsblockcall,
        imspageCalls4.imsblockcall,
        imspageCalls5.imsblockcall
      )

  // noOfUsers*5 is due to 5 requests being executed in parallel in one txn
  setUp(scenarios map (e => e.inject(constantUsersPerSec(noOfUsers) during (duration))) toList).throttle(
    reachRps(noOfUsers * 5 / 2) in ((duration * 10) / 100),
    holdFor((duration * 10) / 100),
    reachRps(noOfUsers * 5) in ((duration * 20) / 100),
    holdFor((duration * 60) / 100)
  ).protocols(imsBaseUrl)
}