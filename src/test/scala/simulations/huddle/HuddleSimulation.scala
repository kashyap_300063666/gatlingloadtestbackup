package simulations.huddle

import com.github.phisgr.gatling.grpc.Predef.{$, grpc}
import io.gatling.core.Predef._
import simulations.BaseSimulation
import io.grpc.{ManagedChannelBuilder, Metadata}
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import huddle.{ClusteredServiceabilityGrpc, TatFilterRequest}
import com.github.phisgr.gatling.pb.{EpxrLens, value2ExprUpdatable}
import io.gatling.core.Predef.scenario
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.session.Expression

class HuddleSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "lms"
  /* Name of File to be downloaded */
  val fileNamePincode = "huddle_pincode.csv"
  val filePathPincode: String = feederUtil.getFileDownloadedPath(containerName, fileNamePincode)
  val fileNameTat = "huddle_tat.csv"
  val filePathTat: String = feederUtil.getFileDownloadedPath(containerName, fileNameTat)

  val grpcConf = grpc(ManagedChannelBuilder
    .forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.HUDDLE.toString,""), grpcPortNum)
    .usePlaintext())

  val dynamicPayload: Expression[TatFilterRequest] = TatFilterRequest(
    requestedTat=Option("1DD") ,
    pincode = Option(110001)
  )
    .updateExpr(_.pincode :~ $("pincode"), (_.requestedTat :~ $("requestedTat")))

  val pincodeFeeder = csv(filePathPincode).circular.convert {
    case ("pincode", string) => string.toInt
  }

  val requestedTatFeeder = csv(filePathTat).circular


  val s1 = scenario("Huddle-LT")
    .feed(pincodeFeeder)
    .feed(requestedTatFeeder)
    .exec(grpc("Huddle-LT")
      .rpc(ClusteredServiceabilityGrpc.METHOD_GET_TAT_FILTER)
      .payload(dynamicPayload)
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
    )

  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}