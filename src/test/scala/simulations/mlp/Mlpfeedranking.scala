package simulations.mlp

import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Mlpfeedranking extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "platform"
  /* Name of File to be downloaded */
  val fileName = "mlp_feedranking5k.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  val httpConf = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MLPFEEDRANKING.toString)).disableWarmUp
 //val csvFile = "test-data/mlp_feedranking5k.csv"
  val customSeparatorFeeder = separatedValues(filePath, '#').circular


  val req = scenario("mlpfeedranking")
    .feed(customSeparatorFeeder)
    .exec(http("all requests")
      .post("/feedranking/predict")
      .body(StringBody("""{"data":{"uidx":"${uidx}","posts":${posts}}}""")).asJson
      .check(status.is(200))
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpConf)
}
