package simulations.mynsta

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import simulations.BaseSimulation
import com.github.phisgr.gatling.grpc.Predef._
import io.grpc.ManagedChannelBuilder
import com.github.phisgr.gatling.pb._
import memento.momento._
import io.gatling.core.session.Expression
import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.commons.ServiceType
import com.myntra.mynsta.mynsta.{OptionSelectRequest, PollQuizServiceGrpc}
import com.myntra.mynsta.mynsta.PollQuizServiceGrpc.PollQuizService

class MynstaPollGrpcSimulation extends BaseSimulation {
  private val url = System.getProperty("url", BaseUrlConstructor.getBaseUrl(ServiceType.MYNSTA.toString, ""))
  val grpcConf = grpc(ManagedChannelBuilder.forAddress(url, grpcPortNum).usePlaintext())

  //  val testNameFeeder = csv("test-data/testname.csv").circular

  val s1 = scenario("Poll grpc")
    .exec(grpc("PollQuizService_CastPollVote")
      .rpc(PollQuizServiceGrpc.METHOD_CAST_POLL_VOTE)
      .payload(OptionSelectRequest(
        postId = 2006,
        questionId = 2,
        optionId = 1
      ))
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}
