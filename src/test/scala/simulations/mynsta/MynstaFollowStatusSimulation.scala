package simulations.mynsta

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import simulations.BaseSimulation
import com.github.phisgr.gatling.grpc.Predef._
import io.grpc.{ManagedChannelBuilder, Metadata}
import com.github.phisgr.gatling.pb._
import memento.momento._
import io.gatling.core.session.Expression
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import com.myntra.mynsta.mynsta.{AuthorServiceGrpc, FeedServiceGrpc, FetchQuizQuestionResponse, GenericStringUidxRequest, OptionSelectRequest, PollQuizServiceGrpc, QuizQuestionRequest}

class MynstaFollowStatusGrpcSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "mynstaUsers.csv"

  private val url = System.getProperty("url", BaseUrlConstructor.getBaseUrl(ServiceType.MYNSTA.toString, ""))
  val grpcConf = grpc(ManagedChannelBuilder.forAddress(url, grpcPortNum).usePlaintext())

  //val uidxFeeder = csv("test-data/mynstaUsers.csv").circular
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val uidxFeeder = csv(filePath).circular


  val authorsUidx: Expression[GenericStringUidxRequest] = GenericStringUidxRequest(uidx = "b792fbb6.8df2.48be.ab8e.28b218487622QQh8lgEngt")

  val s1 = scenario("Get author follow status grpc")
    .feed(uidxFeeder)
    .exec(grpc("AuthorService_GetAuthorFolllowStatus")
      .rpc(AuthorServiceGrpc.METHOD_GET_AUTHOR_FOLLOW_STATUS)
      .payload(authorsUidx)
      .header(Metadata.Key.of("m-uidx", Metadata.ASCII_STRING_MARSHALLER))("${m-uidx}")
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}