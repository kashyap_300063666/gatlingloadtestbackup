package simulations.mynsta

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import simulations.BaseSimulation
import io.grpc.{ManagedChannelBuilder, Metadata}
import io.gatling.core.session.Expression
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import com.myntra.mynsta.mynsta.UserServiceGrpc.UserService
import com.myntra.mynsta.mynsta.{AuthorServiceGrpc, FeedRequest, FeedServiceGrpc, GenericStringIdRequest, GenericStringRequest, GenericStringUidxRequest, Pagination, PostByIdRequest, PostServiceGrpc, TagServiceGrpc, TopicServiceGrpc, UserServiceGrpc}

class MynstaCommonGrpcSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "mynstaUsers.csv"

  private val url = System.getProperty("url", BaseUrlConstructor.getBaseUrl(ServiceType.MYNSTA.toString, ""))
  val grpcConf = grpc(ManagedChannelBuilder.forAddress(url, grpcPortNum).usePlaintext())


  val authorsUidx: Expression[GenericStringUidxRequest] = GenericStringUidxRequest(uidx = "c3a05e83.d8be.4d3d.bf7c.36a690a33baa6XzpXliK6U")
  //val uidxFeeder = csv("test-data/mynstaUsers.csv").circular
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val uidxFeeder = csv(filePath).circular

  val s1 = scenario("Get tag by id")
    .feed(uidxFeeder)
    .exec(grpc("TagService_GetTagById")
      .rpc(TagServiceGrpc.METHOD_GET_TAG_BY_ID)
      .payload(GenericStringIdRequest(
        id = "5e3d3df829bdfb88a94ae2b4"
      ))
      .header(Metadata.Key.of("m-uidx", Metadata.ASCII_STRING_MARSHALLER))("${m-uidx}")
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
      )
    .exec(grpc("AuthorService_GetAuthorByHandle")
      .rpc(AuthorServiceGrpc.METHOD_GET_AUTHOR_BY_HANDLE)
      .payload(GenericStringRequest(
        value = "PlayOnStudio"
      ))
      .header(Metadata.Key.of("m-uidx", Metadata.ASCII_STRING_MARSHALLER))("${m-uidx}")
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
    )
    .exec(grpc("TopicService_GetTopicById")
      .rpc(TopicServiceGrpc.METHOD_GET_TOPIC_BY_ID)
      .payload(GenericStringIdRequest(
        id = "5e3d12fb6c63697fc3335b78"
      ))
      .header(Metadata.Key.of("m-uidx", Metadata.ASCII_STRING_MARSHALLER))("${m-uidx}")
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
    )
    .exec(grpc("PostService_FilteredPosts")
      .rpc(PostServiceGrpc.METHOD_GET_POST_BY_ID)
      .payload(PostByIdRequest(
        id = 10049
      ))
      .header(Metadata.Key.of("m-uidx", Metadata.ASCII_STRING_MARSHALLER))("${m-uidx}")
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}
