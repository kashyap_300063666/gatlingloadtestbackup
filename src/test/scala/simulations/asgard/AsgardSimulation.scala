package simulations.asgard

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import simulations.BaseSimulation
import io.gatling.http.Predef.http
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class AsgardSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "cartpayments"
  /* Name of File to be downloaded */
  val fileName = "giftcardIDs.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ASGARD.toString)
  //private val csvFeeder = csv("test-data/giftcardIDs.csv").random
  private val csvFeeder = csv(filePath).random

  private val balanceHeaders = Map("cache-control" -> "no-cache",
    "client" -> "cart",
    "version" -> "1.0",
    "x-mynt-ctx" -> "storeid=2297")

  val asgardBaseUrl = System.getProperty("baseUrl", defaultUrl)
  val httpProtocol = http
    .baseUrl(asgardBaseUrl)
    .headers(balanceHeaders)

  private val balance =
    scenario("giftcard balance")
        .feed(csvFeeder)
        .exec(http("Get user's giftcard balance")
        .get("/giftcard-service/v3/user/giftcard/balance/${uidx}")
        .headers(balanceHeaders)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("18002"))
        .check(jsonPath("$.status.statusType").is("SUCCESS"))
      )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}
