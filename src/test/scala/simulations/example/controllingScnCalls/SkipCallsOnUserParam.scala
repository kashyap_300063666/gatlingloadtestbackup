package simulations.example.controllingScnCalls

import com.myntra.commons.{FormIdeaUserJson, ServiceType}
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil, HLTUtility, RedisUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

import scala.util.Random

class SkipCallsOnUserParam extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "systems"
  /* Name of File to be downloaded */
  val fileNameUidxFileLocation = "uidxlist1.csv"

  val util = new HLTUtility()

  val setApiDisableEnable: Iterator[Product2[String, String]] = List("bool" -> "true").iterator
  util.redisUtil.hmset("ApiEnableDisable", setApiDisableEnable.toIterable)


  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  val namespace = "personify"
  val uidxFileLocation: String = System.getProperty("uidxFileLocation", fileNameUidxFileLocation)
  val filePathUidxFileLocation: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.SEGMENT_SERVICE.toString)
  private val segmentServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  val r: Random.type = scala.util.Random
  val lines: Seq[String] = readFile(filePathUidxFileLocation)
  println(util.redisUtil.hget("ApiEnableDisable", "home").toBoolean)

  //This called is blocked by user input
  private val api1 = scenario("Segment service GET segments test")
    .exec(session => session.set("uidx", lines(r.nextInt(10000))))
  .doIf(session => util.redisUtil.hget("ApiEnableDisable", "home").toBoolean) {
    exec(http("Segment service GET segments test")
      .post("/api/v1/user_segments/get/${uidx}")
      .body(StringBody("""{"namespace":["personify", "cohort"]}""")).asJson
      .headers(headers)
      .check(status.is(200))
    )
      .exec(sess => {
        println(util.redisUtil.hget("ApiEnableDisable", "home").toBoolean)
        sess
      })
  }
  private val api2 = scenario("Segment service GET METADATA test")
    .exec(session => session.set("metadataId", lines(r.nextInt(200))))
    .exec(http("Segment service GET METADATA test")
      .get("/api/v1/get/metadata/${metadataId}")
      .headers(headers)
      .check(status.is(200))
    ).exitHereIfFailed


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(segmentServiceBaseUrl)

}

/*
  Command for the simulation.

  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -Dfilter.scenarios="segment" -DuidxFileLocation="/Users/11151/Downloads/uidxlist.txt" -DsegmentValuesFileLocation="/Users/11151/Downloads/segmentValues.txt" -jar inbound-performance-test-all.jar -s simulations.platform.SegmentSimulation  -df . -rf .
  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -Dfilter.scenarios="segment" -DuidxFileLocation="/home/guest/perf/uidx.txt" -DsegmentValuesFileLocation="/home/guest/perf/segmentValues.txt" -jar inbound-performance-test-all.jar -s simulations.platform.SegmentSimulation  -df . -rf .
 */