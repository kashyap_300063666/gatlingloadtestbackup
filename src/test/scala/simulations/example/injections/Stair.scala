package simulations.example.injections

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class Stair extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "systems"
  /* Name of File to be downloaded */
  val fileName = "address.csv"

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ADDRESS_SERVICE.toString)
  private val addressServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  /*val csvFile = System.getProperty("addressUidxFile", "test-data/address.csv")

  val csvfeeder = csv(csvFile).circular*/
  val csvFile = System.getProperty("addressUidxFile", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  val csvfeeder = csv(filePath).circular

  //reference for feeder stratergies-https://gatling.io/docs/current/session/feeder
  val pTestDuration = getIntProperty("staircase.duration")
  val pRampUpTime =getIntProperty("staircase.rampUpTime")
  val pNumSteps =getIntProperty("staircase.steps")
  val pPacing =getIntProperty("staircase.pacing")
  val pVusers = getIntProperty("staircase.rampUser")

  val pSteadyStateTime = pTestDuration - (pRampUpTime * pNumSteps)
  val pStepTime = pSteadyStateTime/pNumSteps

  def ApiLoad(step: Int, pacing: Int, duration: Int) =
    scenario(s"API Scenario $step").during(duration) {
        pace(pacing)
        .feed(csvfeeder)
        .exec(http("get all address test")
          .get("/myntra-address-service/v3/address")
          .headers(headers)
          .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
          .check(status.is(200))
          .check(jsonPath("$.status.statusCode").is("15002"))
        )
    }

  val stepsParams = 1 to pNumSteps map { i => {
    ApiLoad(i, pPacing, ((pNumSteps - i + 1) * (pRampUpTime + pStepTime))).inject(
      nothingFor((i-1) * (pRampUpTime + pStepTime) ),
      rampUsers(pVusers) during (pRampUpTime ),
      nothingFor(pStepTime )
    )
  }
  }

  setUp(stepsParams:_*)
    .protocols(addressServiceBaseUrl)




}
