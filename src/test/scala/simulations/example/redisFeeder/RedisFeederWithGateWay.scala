package simulations.example.redisFeeder

import com.myntra.commons.{FormGenTokenAndLoginUser, ServiceType}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.myntra.commons.util.{BaseUrlConstructor, HLTUtility, RedisUtil}

class RedisFeederWithGateWay extends BaseSimulation {

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)
  private val apiServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  private val counterKeyName = System.getProperty("counter.key", "counter")
  private val waitTimeToSetCounterVal = System.getProperty("wait.time", "5").toInt
  val util = new HLTUtility()

  before {
    util.redisUtil.set(counterKeyName, "0")
  }

  private val loginIdea =
    scenario("Get User Token and login")
      .exec(session => {
        session.set("emailId", util.redisUtil.increamentCounter(counterKeyName) + "@myntra360.com")
      })
      .exec(http("knuthToken")
        .get("/auth/v1/token")
        .header("clientid", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
        .header("x-myntra-app", "appFamily=MyntraRetailAndroid")
        .check(status.is(200))
        .check(header("At").exists.saveAs("at"))
        .check(header("Xid").exists.saveAs("xid"))
      )
    .doIf(session => session.contains("at") && session.contains("xid")) {
      exec(http("login using gen token ")
        .post("/auth/v1/login")
        .header("at", "${at}")
        .header("xid", "${xid}")
        .header("x-myntra-app", "appFamily=MyntraRetailAndroid")
        .body(StringBody("""{"userId":"${emailId}","accessKey":"${emailId}"}""")).asJson
        .check(status.is(200))
        .check(header("Rt").exists.saveAs("rt"))
        .check(header("At").exists.saveAs("at"))
        .check(header("Sxid").exists.saveAs("sxid"))
        .check(jsonPath("$.uidx").exists.saveAs("uidx"))
      )
    }
      .doIf(session => session.contains("at") && session.contains("xid") &&
        session.contains("rt") && session.contains("sxid") && session.contains("uidx")) {

        exec(sess => {
          util.redisUtil.set(sess.apply("emailId").as[String], FormGenTokenAndLoginUser.formJson(
            sess.apply("at").as[String],
            sess.apply("rt").as[String],
            sess.apply("uidx").as[String]))
          sess
        })
    }

  setUp(loginIdea.inject(
    nothingFor(waitTimeToSetCounterVal),
    constantUsersPerSec(noOfUsers) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  ).protocols(apiServiceBaseUrl)
  ).maxDuration(maxDurationInSec)
}