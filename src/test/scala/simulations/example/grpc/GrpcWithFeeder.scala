package simulations.example.grpc

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import simulations.BaseSimulation
import com.github.phisgr.gatling.grpc.Predef._
import io.grpc.ManagedChannelBuilder
import com.github.phisgr.gatling.pb._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import memento.momento._
import io.gatling.core.session.Expression

class GrpcWithFeeder extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "systems"
  /* Name of File to be downloaded */
  val fileName = "testname.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  val grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.GROWTH.toString,""), grpcPortNum).usePlaintext())

  val dynamicPayload: Expression[SaveFilterRequest] = SaveFilterRequest(name = "Saved_Filter_Test",
    data = "{\"f\" : \"Categories:Accessory Gift Set::Content:Tie and Pocket Square\", " +
      "\"q\":\"Red\",\"rf\":\"Price:100.0_1100.0_100.0 TO 1100.0::Discount Range:20.0_100.0_20.0 TO 100.0\"}",
    collection = "saved",
    uidx = "648154ac.3f53.4e7b.aa68.483340156986OhmRPUOtGz")
    .updateExpr(
      _.name :~ $("testname")
    )
  val testNameFeeder = csv(filePath).circular

  val s1 = scenario("Memento grpc")
    .feed(testNameFeeder)
    .exec(grpc("growthHack")
      .rpc(mementoServiceGrpc.METHOD_SAVE_FILTER)
      .payload(dynamicPayload)
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)
}

