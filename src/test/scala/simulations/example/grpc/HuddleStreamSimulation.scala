package simulations.example.grpc

import com.github.phisgr.gatling.generic.SessionCombiner
import com.github.phisgr.gatling.grpc.Predef._
import com.github.phisgr.gatling.grpc.protocol.GrpcProtocol
import com.github.phisgr.gatling.grpc.stream.TimestampExtractor
import com.github.phisgr.gatling.pb._
import com.google.protobuf.empty.Empty
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import huddle.ClusteredServiceabilityGrpc
import io.gatling.commons.validation.{Failure, Validation}
import io.gatling.core.Predef._
import io.gatling.core.check.Matcher
import io.gatling.core.session.Expression
import io.grpc.{CallOptions, Status}
import simulations.BaseSimulation

import scala.concurrent.duration.DurationInt


class HuddleStreamSimulation extends BaseSimulation {

  val grpcConf = grpc(managedChannelBuilder(target = BaseUrlConstructor.getBaseUrl(ServiceType.HUDDLE.toString, "") + ":" + grpcPortNum).usePlaintext()).shareChannel

  val listenCall = grpc("Listen")
    .serverStream(streamName = "listener")



  val listener = scenario("Listener")
    .exec(
      listenCall
        .start(ClusteredServiceabilityGrpc.METHOD_STREAM_TAT_FILTER)(Empty.defaultInstance)
        .timestampExtractor { (_, _, streamStartTime) => streamStartTime}
        .extract(_.some)(_ saveAs "previous")
        .sessionCombiner(SessionCombiner.pick("previous"))
        .endCheck(statusCode is Status.Code.OK)
    )
    .repeat(2) {
      pause(60.seconds)
        .exec(listenCall.copy(requestName = "Reconciliate").reconciliate)
    }



    setUp(scenarios map (e => e.inject(step)) toList)
       .maxDuration(maxDurationInSec)
      .protocols(grpcConf)

}