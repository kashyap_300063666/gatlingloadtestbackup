package simulations.coupon

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class ApplyCouponWithFalse2 extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "ApplyCoupon2.csv"
  val fileName1 = "ApplyCoupon2.json"

  val applyCouponData = System.getProperty("applyCouponData", fileName).toString
  val filePath1 = feederUtil.getFileDownloadedPath(containerName, fileName1)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, applyCouponData)
  //val couponpdpFeeder: BatchableFeederBuilder[String] = feederUtil.getFeeder(containerName, applyCouponData).circular
  val couponpdpFeeder = csv(filePath).circular

  val couponBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PP_COUPON_SERVICE.toString)).disableWarmUp
  val couponheaders = Map ("accept" -> "application/json", "Content-Type" -> "application/json", "x-mynt-ctx" -> "storeid=2297")
  /*val applyCouponData = System.getProperty("applyCouponData", "ApplyCoupon2.csv").toString
  val couponpdpFeeder = csv("test-data/"+applyCouponData).circular*/

  val clickforoffer = scenario("Apply coupon")
    .feed(couponpdpFeeder)
    .exec(http("Apply Coupon With FetchBestCombination False for 2 styles")
      .put("/coupons/myntcoupon/apply")
      .headers(couponheaders)
      .body(ElFileBody(filePath1)).asJson
      .check(status.is(200))
    )
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(couponBaseUrl)


}

