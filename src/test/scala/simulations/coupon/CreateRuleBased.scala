package simulations.coupon

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random


class CreateRuleBased extends  BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "RuleBased.json"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  val couponBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PP_COUPON_INTERNAL.toString)).disableWarmUp
  val couponheaders = Map ("accept" -> "application/json", "Content-Type" -> "application/json", "x-mynt-ctx" -> "storeid=2297")

  val ruleBasedFeeder =Iterator.continually(Map("userId" -> ("automation"+Random.alphanumeric.take(11).mkString)));

  val clickforoffer = scenario("Create rule based coupon")
    .feed(ruleBasedFeeder)
    .exec(http("Create rule based coupon")
      .post("/coupons/couponEngine")
      .headers(couponheaders)
      .body(ElFileBody(filePath)).asJson
      .check(status.is(200))
    )
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(couponBaseUrl)




}
