package simulations.coupon

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class ApplyCoupon2StylesCombined extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "ApplyCoupon2.csv"
  val fileNameCouponUsers = "applyCouponsUsers.csv"
  val fileName1 = "ApplyCoupon2.json"

  val applyCouponData = System.getProperty("applyCouponData", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, applyCouponData)
  val filePath1: String = feederUtil.getFileDownloadedPath(containerName, fileName1)
  val couponpdpFeeder: BatchableFeederBuilder[String] = csv(filePath).circular
  //val couponpdpFeeder: BatchableFeederBuilder[String] = feederUtil.getFeeder(containerName, applyCouponData).circular

  val couponBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PP_COUPON_SERVICE.toString)).disableWarmUp
  /*val applyCouponData = System.getProperty("applyCouponData", "ApplyCoupon2.csv").toString
  val couponpdpFeeder = csv("test-data/"+applyCouponData).circular*/
  //val applyCouponsUidxfeeder = csv("test-data/applyCouponsUsers.csv").circular
  val filePathCouponUsers: String = feederUtil.getFileDownloadedPath(containerName, fileNameCouponUsers)
  val applyCouponsUidxfeeder = csv(filePathCouponUsers).circular

  val clickforoffer = scenario("Apply coupon combined")
    .feed(couponpdpFeeder)
    .feed(applyCouponsUidxfeeder)
    .exec(http("Apply Coupon combined for 2 styles")
      .put("/coupons/myntcoupon/apply")
      .header("accept", "application/json")
      .header("Content-Type", "application/json")
      .header("fetchBestCombination", "${fetchBestCombination}")
      .header("x-mynt-ctx","storeid=2297;uidx=${uidx};")
      .body(ElFileBody(filePath1)).asJson
      .check(status.is(200))
    )
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(couponBaseUrl)


}
