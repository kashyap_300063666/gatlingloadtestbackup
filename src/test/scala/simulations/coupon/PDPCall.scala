package simulations.coupon

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class PDPCall extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "PDPCall.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val fileNamePdpOffers = "PDPOffersUidx.csv"
  val filePathPdpOffers: String = feederUtil.getFileDownloadedPath(containerName, fileNamePdpOffers)

  val couponBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PP_COUPON_SERVICE.toString)).disableWarmUp
  val couponheaders = Map ("accept" -> "application/json", "Content-Type" -> "application/json")
  val couponpdpFeeder = csv(filePath).random
  val xMyntCtxfeeder = csv(filePathPdpOffers).random

  val clickforoffer = scenario("Click For Offer")
    .feed(couponpdpFeeder)
    .feed(xMyntCtxfeeder)
    .exec(http("click for offer")
      .get("/coupons/pdp/${styleId}/${sellerId}")
      .header("x-mynt-ctx" , "${x-mynt-ctx}")
      .headers(couponheaders)
      .check(status.is(200))
    )
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(couponBaseUrl)

}
