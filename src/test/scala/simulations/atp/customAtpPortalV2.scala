package simulations.atp

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.http
import simulations.BaseSimulation

class customAtpPortalV2 extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "wms"
  /* Name of File to be downloaded */
  val fileName = "atpData2.csv"

  private val atpBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.ATP2.toString)).disableWarmUp

  //val csvFile = System.getProperty("atpdataprovider", "test-data/atpData2.csv")
  val csvFile: String = System.getProperty("atpdataprovider", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  //val csvfeeder = csv(csvFile).circular
  val csvfeeder = csv(filePath).circular
  val atppageCalls = new AtpUtil().AtpApis
  val atppageCalls2 = new AtpUtil2().AtpApis
  val atppageCalls3 = new AtpUtil3().AtpApis
  val atppageCalls4 = new AtpUtil4().AtpApis
  val atppageCalls5 = new AtpUtil5().AtpApis

  private val preRequisiteScenario =
    scenario("Pre Requisite Script")
      .feed(csvfeeder)
      .exec(
        atppageCalls.portalInventory,
        atppageCalls2.portalInventory,
        atppageCalls3.portalInventory,
        atppageCalls4.portalInventory,
        atppageCalls5.portalInventory
      )

  setUp(scenarios map (e => e.inject(constantUsersPerSec(noOfUsers) during (duration))) toList).throttle(
    reachRps(noOfUsers * 5 / 2) in ((duration * 10) / 100),
    holdFor((duration * 10) / 100),
    reachRps(noOfUsers * 5) in ((duration * 20) / 100),
    holdFor((duration * 60) / 100)
  ).protocols(atpBaseUrl)
}