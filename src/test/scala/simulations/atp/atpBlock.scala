package simulations.atp
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class atpBlock extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "wms"
  /* Name of File to be downloaded */
  val fileName = "atpData.csv"

  //private val imsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.IMSSYNC.toString)).disableWarmUp
  private val atpBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.ATP.toString)).disableWarmUp
  //private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ADDRESS_SERVICE.toString)




  //val csvFile = System.getProperty("atpdataprovider", "test-data/atpData.csv")
  val csvFile: String = System.getProperty("atpdataprovider", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  //val csvfeeder = csv(csvFile).circular
  val csvfeeder = csv(filePath).circular
  val atppageCalls = new AtpUtil().AtpApis
  val atppageCalls2 = new AtpUtil2().AtpApis
  val atppageCalls3 = new AtpUtil3().AtpApis
  val atppageCalls4 = new AtpUtil4().AtpApis
  val atppageCalls5 = new AtpUtil5().AtpApis

  private val preRequisiteScenario =
    scenario("Pre Requisite Script")
      .feed(csvfeeder)
      .exec(
        atppageCalls.blockInventory,
        atppageCalls2.blockInventory,
        atppageCalls3.blockInventory,
        atppageCalls4.blockInventory,
        atppageCalls5.blockInventory
      )


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(atpBaseUrl)




}