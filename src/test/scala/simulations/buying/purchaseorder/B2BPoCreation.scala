package simulations.buying.purchaseorder

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.http.Predef._
import io.gatling.core.Predef._

import simulations.BaseSimulation
  /* Created by 300000929 on 16/05/18.
  */
class B2BPoCreation extends BaseSimulation {

  private val purchaseOrderBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PURCHASE_ORDER.toString)).disableWarmUp

  private val poheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")

  private val createB2BPO = scenario("CreateB2BPo")
      .exec(http("create b2b po")
      .post("/purchase-order-service/po/b2c/v2")
      .headers(poheaders)
      .body(StringBody("""{"vendorId": "5561", "vendorWarehouseId":"363",  "buyerId":"3974","buyerWarehouseId":"363","commercialType":"JIT_MARKETPLACE","description":"Load Testing Dummy POs","sourceReferenceId":121,"sourcePartnerId":"3974","b2COrderSKUEntries":[{"skuId":"32379559","quantity": 1, "mrp":101.0}]}""")).asJson
          .check(status.is(200)))


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(purchaseOrderBaseURL)

}
