package simulations.buying.purchaseorder

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

/**
  * Updated by 300067190 on 30/11/19.
  */
class LmcPOCreation extends BaseSimulation {

  private val purchaseOrderBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PURCHASE_ORDER.toString)).disableWarmUp

  private val poheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")

  private val createLMCPO = scenario("CreateLmcPO")
    .exec(http("create lmc po")
      .post("/purchase-order-service/po/lmc/")
      .headers(poheaders)
      .body(StringBody("""{"vendorId": "5561", "vendorAddressId":"12766",  "buyerId":"3974","buyerWarehouseId":"363","commercialType":"OUTRIGHT","description":"Load Testing Dummy POs","sourceReferenceId":"DummyTest","marketPlaceOrderSKUEntries":[{"skuId":"32379559","quantity": 1, "mrp":101.0}]}""")).asJson
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(purchaseOrderBaseURL)
}
