package simulations.buying.purchaseorder

import com.myntra.commons.util.{CSVUtils, FeederUtil}
import io.gatling.http.Predef._
import simulations.BaseSimulation
import io.gatling.core.Predef._

import scala.util.Random

class SingleServerExecution extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "buying_pos.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  /**
    * Created by 300067190 on 04/12/19
    */
  //Using pas-purchaseorder0 {10.162.161.155:11301}

  private val purchaseOrderBaseURL = http.baseUrl(getProperty("buyingBaseUrlForSingleMachine")).disableWarmUp

  private val poheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")

  var myntraPoCodesFeeder = Iterator.continually(
    Map("poIds" ->  Random.shuffle(CSVUtils.readCSV2(filePath, false).map(e => e.apply(0)).toList).take(900).mkString(",")))

  private val bulkFetchByIds_singleServer = scenario("bulkFetchByIds_singleServer")
    .feed(myntraPoCodesFeeder)
    .exec(http("BulkFetchByIds")
      .post("/purchase-order-service/po/bulk-fetch-by-ids/")
      .headers(poheaders)
      .body(StringBody("""{"ids":[${poIds}]}""")).asJson
      .check(status.is(200)))

  private val createB2BPO_singleServer = scenario("CreateB2BPo_singleServer")
    .exec(http("create b2b po")
      .post("/purchase-order-service/po/b2c/v2")
      .headers(poheaders)
      .body(StringBody("""{"vendorId": "5561", "vendorWarehouseId":"36",  "buyerId":"3974","buyerWarehouseId":"363 ","commercialType":"JIT_MARKETPLACE","description":"Load Testing Dummy POs","sourceReferenceId":121,"b2COrderSKUEntries":[{"skuId":"32379559","quantity":1, "mrp":101.0}]}""")).asJson
      .check(status.is(200)))

  private val createFBMPO_singleServer = scenario("createFbmPo_singleServer")
    .exec(http("create fbm po")
      .post("/purchase-order-service/po/fbm")
      .headers(poheaders)
      .body(StringBody("""{"enabled":"true","vendorId": "6320", "warehouseId":"363",  "buyerId":"6320","buyerWarehouseId":"363","commercialType":"OUTRIGHT","description":"Load Testing Dummy POs","sourceReferenceId":121, "totalQuantity":1,"purchaseOrderSKUEntries":[{"skuId":"32445641","quantity":1, "listPrice":101.0,"mrp":100,"landedPrice":100,"createdBy":"WOMRS","createdOn":1575463796000,"lastModifiedOn":1575463796000}]}""")).asJson
      .check(status.is(200)))

  private val createLMCPO_singleServer = scenario("CreateLmcPO_singleServer")
    .exec(http("create lmc po")
      .post("/purchase-order-service/po/lmc/")
      .headers(poheaders)
      .body(StringBody("""{"vendorId": "5561", "vendorAddressId":3781,  "buyerId":"3974","buyerWarehouseId":"363","commercialType":"SOR","description":"Load Testing Dummy POs","sourceReferenceId":"DummyTest","marketPlaceOrderSKUEntries":[{"skuId":"32379559","quantity": 1, "mrp":101.0}]}""")).asJson
      .check(status.is(200)))

  private val fetchPoWithSKUs_singleServer = scenario("fetchPOsWithSKUs_singleServer")
    .feed(myntraPoCodesFeeder)
    .exec(http("BulkFetchByIds")
      .get("/purchase-order-service/po/fetch-po-with-skus/${poIds}")
      .headers(poheaders)
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(purchaseOrderBaseURL)

}
