package simulations.buying.purchaseorder

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.http.Predef._
import simulations.BaseSimulation
import io.gatling.core.Predef._

import scala.util.Random

/**
  * Created by 300067190 on 26/11/19.
  */
class BulkFetchBySkuIds extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileNamePo = "buying_pos.csv"
  val fileNameSku = "buying_skus.csv"

  val filePathPo: String = feederUtil.getFileDownloadedPath(containerName, fileNamePo)
  val filePathSku: String = feederUtil.getFileDownloadedPath(containerName, fileNameSku)

  private val purchaseOrderBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PURCHASE_ORDER.toString)).disableWarmUp

  private val poheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")

  var myntraSkuCodesFeeder = Iterator.continually(
    Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePathSku, false).map(e => e.apply(0)).toList).take(100).mkString(",")))

  var myntraPoCodesFeeder = Iterator.continually(
    Map("poId" ->  Random.shuffle(CSVUtils.readCSV2(filePathPo, false).map(e => e.apply(0)).toList).take(1).mkString("")))

  private val bulkFetchPOsBySKUIds = scenario("bulkFetchPOsBySKUIds")
    .feed(myntraPoCodesFeeder)
    .feed(myntraSkuCodesFeeder)
    .exec(http("BulkFetchBySKUIds")
      .post("/purchase-order-service/po-sku/bulk-fetch-by-sku-ids/")
      .headers(poheaders)
      .body(StringBody("""{"id":${poId},"ids":[${skuIds}]}""")).asJson
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(purchaseOrderBaseURL)

}
