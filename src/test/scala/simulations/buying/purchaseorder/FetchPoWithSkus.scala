package simulations.buying.purchaseorder

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.http.Predef._
import simulations.BaseSimulation
import io.gatling.core.Predef._

import scala.util.Random

/**
  * Created by 300067190 on 30/11/19.
  */
class FetchPoWithSkus extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "buying_pos.csv"

  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val purchaseOrderBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PURCHASE_ORDER.toString)).disableWarmUp

  private val poheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")

  var myntraPoCodesFeeder = Iterator.continually(
    Map("poIds" ->  Random.shuffle(CSVUtils.readCSV2(filePath, false).map(e => e.apply(0)).toList).take(1).mkString(",")))

  private val fetchPoWithSKUs = scenario("fetchPOsWithSKUs")
      .feed(myntraPoCodesFeeder)
      .exec(http("BulkFetchByIds")
        .get("/purchase-order-service/po/fetch-po-with-skus/${poIds}")
        .headers(poheaders)
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
      .protocols(purchaseOrderBaseURL)

    }