package simulations.buying.purchaseorder

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class PoSearch extends BaseSimulation {

  private val purchaseOrderBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PURCHASE_ORDER.toString)).disableWarmUp

  private val poheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")

  private val searchPOs = scenario("SearchPOs")
    .exec(http("search POs")
      .get("/purchase-order-service/po/search/?q=status.in%3AAPPROVED%2CDECLINED%2CCOMPLETED%2CCLOSED%2CWITHDRAWN___barcode.in%3ATAUR201218-2&fetchSize=10000&start=0")
      .headers(poheaders)
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec)
    .protocols(purchaseOrderBaseURL)

}
