package simulations.coreservice.lego

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Lgp extends BaseSimulation {

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.LEGO.toString)

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid; appVersion=4.2011.2; appBuild=80110371; deviceCategory=Phone; osVersion=9; sdkVersion=28; deviceID=97e8344a8f7f4afb; installationID=dff75a93-546f-321a-abf3-10321ea19077; sessionID=4f21dbe1-88f7-4dcb-999a-54744cfe41a7-97e8344a8f7f4afb; customerID=8c5c408a.fddf.4648.8247.67045c0eafbaXXho9Mx4DC; advertisingID=1209cf68-8b90-4d0e-87f4-5f8e8d54f5ad; advertisingIDKeySpace=GAID; bundleVersion=1.0.2;",
    "x-location-context" -> "pincode=600001; source=IP")

  val httpProtocol = http
    .baseUrl(defaultUrl)

  val randomUidxFeeder = Array("8c5c408a.fddf.4648.8247.67045c0eafbaXXho9Mx4DC",
    "7ff8d2b0.9ba2.43f6.8884.0f6a5a50090cOStsv53rA9",
    "c531378f.0a76.4637.892e.4970e36f84a0yqSSyurEb0",
    "b1d02b92.53f2.4e62.82cd.724b06c124e45OBpYpCDRM").map(e => Map("uidx" -> e)).array.random

  val randomPageNameFeeder = Array("home", "sstest").map(e => Map("pageName" -> e)).array.random

  private val layoutFeedApiWithMultiPage =
      scenario("layoutFeedApiWithMultiPage")
      .feed(randomUidxFeeder)
      .feed(randomPageNameFeeder)
      .exec(http("layoutFeedApiWithMultiPage")
      .post("/v2/layout/feed/${pageName}")
      .headers(defaultHeader)
      .header("x-mynt-ctx", "storeid=2297; uidx=$uidx")
      .body(StringBody("""{"pageContext":{"widgetHash": {
                         |"19:5fa3ea398883c8aa55271b82":"79a002d961c54326741aab28a5b8adb1",
                         |"19:5fb3b101b7ed329884408ad2":"b7fed20a04a7138ec047a4aaa3db33ac",
                         |"19:5fbd3526349d183c7c6eeef9" : "77d8648ab1b666ecef643efee8c6c1de",
                         |"19:5fbd3526349d183c7c6eeef8" : "4b83f81db62b34b4214281b83f5a66e2"}}}""".stripMargin)).asJson
      .check(status.is(200))
      )

  private val layoutFeedApiWithMultiPageAndOneUidx =
    scenario("layoutFeedApiWithMultiPageAndOneUidx")
      .feed(randomPageNameFeeder)
      .exec(http("layoutFeedApiWithMultiPageAndOneUidx")
        .post("/v2/layout/feed/${pageName}")
        .headers(defaultHeader)
        .header("x-mynt-ctx", "storeid=2297; uidx=7ff8d2b0.9ba2.43f6.8884.0f6a5a50090cOStsv53rA9")
        .body(StringBody("""{"pageContext":{"widgetHash": {
                           |"19:5fa3ea398883c8aa55271b82":"79a002d961c54326741aab28a5b8adb1",
                           |"19:5fb3b101b7ed329884408ad2":"b7fed20a04a7138ec047a4aaa3db33ac",
                           |"19:5fbd3526349d183c7c6eeef9" : "77d8648ab1b666ecef643efee8c6c1de",
                           |"19:5fbd3526349d183c7c6eeef8" : "4b83f81db62b34b4214281b83f5a66e2"}}}""".stripMargin)).asJson
        .check(status.is(200))
      )

  private val layoutFeedApiWithMultiPageWithoutUidx =
    scenario("layoutFeedApiWithMultiPageWithoutUidx")
      .feed(randomPageNameFeeder)
      .exec(http("layoutFeedApiWithMultiPageWithoutUidx")
        .post("/v2/layout/feed/${pageName}")
        .headers(defaultHeader)
        .header("x-mynt-ctx", "storeid=2297;")
        .body(StringBody("""{"pageContext":{"widgetHash": {
                           |"19:5fa3ea398883c8aa55271b82":"79a002d961c54326741aab28a5b8adb1",
                           |"19:5fb3b101b7ed329884408ad2":"b7fed20a04a7138ec047a4aaa3db33ac",
                           |"19:5fbd3526349d183c7c6eeef9" : "77d8648ab1b666ecef643efee8c6c1de",
                           |"19:5fbd3526349d183c7c6eeef8" : "4b83f81db62b34b4214281b83f5a66e2"}}}""".stripMargin)).asJson
        .check(status.is(200))
      )

  private val layoutFeedApiWithHomePageAndOneUidx =
    scenario("layoutFeedApiWithHomePageAndOneUidx")
      .feed(randomPageNameFeeder)
      .exec(http("layoutFeedApiWithHomePageAndOneUidx")
        .post("/v2/layout/feed/home")
        .headers(defaultHeader)
        .header("x-mynt-ctx", "storeid=2297; uidx=7ff8d2b0.9ba2.43f6.8884.0f6a5a50090cOStsv53rA9")
        .body(StringBody("""{"pageContext":{"widgetHash": {
                           |"19:5fa3ea398883c8aa55271b82":"79a002d961c54326741aab28a5b8adb1",
                           |"19:5fb3b101b7ed329884408ad2":"b7fed20a04a7138ec047a4aaa3db33ac",
                           |"19:5fbd3526349d183c7c6eeef9" : "77d8648ab1b666ecef643efee8c6c1de",
                           |"19:5fbd3526349d183c7c6eeef8" : "4b83f81db62b34b4214281b83f5a66e2"}}}""".stripMargin)).asJson
        .check(status.is(200))
      )

  private val layoutFeedApiHomePageWithUidxAndGzip =
    scenario("layoutFeedApiHomePageWithUidxAndGzip")
      .feed(randomPageNameFeeder)
      .exec(http("layoutFeedApiHomePageWithUidxAndGzip")
        .post("/v2/layout/feed/home")
        .headers(defaultHeader)
        .header("x-mynt-ctx", "storeid=2297; uidx=7ff8d2b0.9ba2.43f6.8884.0f6a5a50090cOStsv53rA9")
        .header("accept-encoding", "gzip")
        .body(StringBody("""{"pageContext":{"widgetHash": {
                           |"19:5fa3ea398883c8aa55271b82":"79a002d961c54326741aab28a5b8adb1",
                           |"19:5fb3b101b7ed329884408ad2":"b7fed20a04a7138ec047a4aaa3db33ac",
                           |"19:5fbd3526349d183c7c6eeef9" : "77d8648ab1b666ecef643efee8c6c1de",
                           |"19:5fbd3526349d183c7c6eeef8" : "4b83f81db62b34b4214281b83f5a66e2"}}}""".stripMargin)).asJson
        .check(status.is(200))
      )

  private val layoutFeedApiWithHomePageWithoutUidx =
    scenario("layoutFeedApiWithHomePageWithoutUidx")
      .feed(randomPageNameFeeder)
      .exec(http("layoutFeedApiWithHomePageWithoutUidx")
        .post("/v2/layout/feed/home")
        .headers(defaultHeader)
        .header("x-mynt-ctx", "storeid=2297;")
        .body(StringBody("""{"pageContext":{"widgetHash": {
                           |"19:5fa3ea398883c8aa55271b82":"79a002d961c54326741aab28a5b8adb1",
                           |"19:5fb3b101b7ed329884408ad2":"b7fed20a04a7138ec047a4aaa3db33ac",
                           |"19:5fbd3526349d183c7c6eeef9" : "77d8648ab1b666ecef643efee8c6c1de",
                           |"19:5fbd3526349d183c7c6eeef8" : "4b83f81db62b34b4214281b83f5a66e2"}}}""".stripMargin)).asJson
        .check(status.is(200))
      )


  private val layoutFeedApiWithHomePage =
    scenario("layoutFeedApiWithHomePage")
      .feed(randomUidxFeeder)
      .exec(http("layoutFeedApiWithHomePage")
        .post("/v2/layout/feed/home")
        .headers(defaultHeader)
        .header("x-mynt-ctx", "storeid=2297; uidx=$uidx")
        .body(StringBody("""{"pageContext":{"widgetHash": {
                           |"19:5fa3ea398883c8aa55271b82":"79a002d961c54326741aab28a5b8adb1",
                           |"19:5fb3b101b7ed329884408ad2":"b7fed20a04a7138ec047a4aaa3db33ac",
                           |"19:5fbd3526349d183c7c6eeef9" : "77d8648ab1b666ecef643efee8c6c1de",
                           |"19:5fbd3526349d183c7c6eeef8" : "4b83f81db62b34b4214281b83f5a66e2"}}}""".stripMargin)).asJson
        .check(status.is(200))
      )

  private val layoutFeedApiWithSsTestPage =
    scenario("layoutFeedApiWithSsTestPage")
      .feed(randomUidxFeeder)
      .exec(http("layoutFeedApiWithSsTestPage")
        .post("/v2/layout/feed/sstest")
        .headers(defaultHeader)
        .header("x-mynt-ctx", "storeid=2297; uidx=7ff8d2b0.9ba2.43f6.8884.0f6a5a50090cOStsv53rA9")
        .header("x-myntra-abtest", "lgp.smartstore.sstest=default")
        .body(StringBody("""{"pageContext":{"widgetHash": {
                           |"19:5fa3ea398883c8aa55271b82":"79a002d961c54326741aab28a5b8adb1",
                           |"19:5fb3b101b7ed329884408ad2":"b7fed20a04a7138ec047a4aaa3db33ac",
                           |"19:5fbd3526349d183c7c6eeef9" : "77d8648ab1b666ecef643efee8c6c1de",
                           |"19:5fbd3526349d183c7c6eeef8" : "4b83f81db62b34b4214281b83f5a66e2"}}}""".stripMargin)).asJson
        .check(status.is(200))
      )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpProtocol)
}
