package simulations.coreservice.lego

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Pdp extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "apifydevapi"
  /* Name of File to be downloaded */
  val fileName = "styleids.csv"
  val fileNameV3 = "serviceabilityV3.json"
  val filePathV3: String = feederUtil.getFileDownloadedPath(containerName, fileNameV3)

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.LEGO.toString)
  val legoBaseUrl = System.getProperty("baseUrl", defaultUrl)
  val httpProtocol = http.baseUrl(legoBaseUrl)

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "x-mynt-ctx" -> "storeid=2297;uidx=8c5c408a.fddf.4648.8247.67045c0eafbaXXho9Mx4DC")


  val csvFile = System.getProperty("testData", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  val styleData = csv(filePath).circular

  private val legoPdpApis =
    scenario("legoPdpApis")
    .feed(styleData)
    .exec(http("LegoPdp")
    .get("/layout/v2/product/${styleId}")
    .headers(defaultHeader)
    .check(status.is(200)))

    .exec(http("LegoRelated")
      .get("/layout/product/${styleId}/recommendation/related?colors=true")
      .headers(defaultHeader)
      .check(status.is(200)))

    .exec(http("LegoCrossSell")
      .get("/layout/product/${styleId}/recommendation/cross-sell")
      .headers(defaultHeader)
      .check(status.is(200)))

    /*.exec(http("LegoOffers")
      .get("/layout/product/${styleId}/offers/${sellerPartnerId}")
      .headers(defaultHeader)
      .check(status.is(200)))*/

    .exec(http("LegoServiceability")
      .post("/layout/serviceability/check")
      .headers(defaultHeader)
      .body(ElFileBody(filePathV3)).asJson
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpProtocol)
}