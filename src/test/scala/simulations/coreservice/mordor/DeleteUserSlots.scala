package simulations.coreservice.mordor

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class DeleteUserSlots extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "apifydevapi"
  /* Name of File to be downloaded */
  val fileName = "mordorData.csv"


  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MORDOR.toString)

  //val csvFile = System.getProperty("testData", "test-data/mordorData.csv").toString
  val csvFile = System.getProperty("testData", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  val mordorBaseUrl = System.getProperty("baseUrl", defaultUrl)


  val httpProtocol = http
    .baseUrl(mordorBaseUrl)

  //val mordorDataReader = csv(csvFile).circular
  val mordorDataReader = csv(filePath).circular


  var deleteUserSlot =
    scenario("deleteUserSlot")
      .feed(mordorDataReader)
      .exec(http("deleteUserSlot")
      .delete("/admission/user/${uidx}/all")
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpProtocol)
}