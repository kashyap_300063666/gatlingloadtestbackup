package simulations.coreservice.gateway

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Ratings extends BaseSimulation{
  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "at"-> "${at}",
    "m-uidx"-> "${uidx}",
    "x-mynt-ctx"-> "storeid=2297;uidx=${uidx};nidx=${xid}")

  object ratingApis {
    val getStyleRating: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    ) {
      exec(http("GetStyleRating")
        .get("/v1/ratings/${styleId}")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }
  }
}
