package simulations.coreservice.gateway

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class QuestionAnswer extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "apifydevapi"
  /* Name of File to be downloaded */
  val fileName = "gatewayStyleId.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val gateWayBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.GATEWAY.toString)).disableWarmUp
  private val gatewayHeader = Map("x-mynt-ctx" -> "storeid=2297")


  //val styleIdfeeder = csv("test-data/gatewayStyleId.csv").circular
  val styleIdfeeder = csv(filePath).circular

  private val reviews =
    scenario("reviews-and-questions-answer")
      .feed(styleIdfeeder)
      .exec(http("reviews-and-questions-answer")
        .get("/v1/reviews-and-questions-answer/${styleId}")
        .headers(gatewayHeader)
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(gateWayBaseUrl)

}
