package simulations.coreservice.gateway

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Auth extends BaseSimulation {

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;appVersion=4.2009.2",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "x-forwarded-for" -> "10.10.10.10")

  object authApis {
    val guestTokenApi: ChainBuilder = exec(http("token")
      .get("/auth/v1/token")
      .headers(defaultHeader)
      .check(status.is(200))
      .check(header("At").exists.saveAs("at"))
    )

    val loggedUserApi: ChainBuilder = doIf(
      session =>
        session.contains("at")  && session.contains("emailId") || session.contains("xid")
    ) {
      exec(http("login ")
        .post("/v1/auth/login")
        .header("at", "${at}")
        .headers(defaultHeader)
        .header("x-mynt-ctx", "storeid=2297")
        .body(StringBody("""{"userId":"${emailId}","accessKey":"${emailId}"}""")).asJson
        .check(status.is(200))
        .check(header("Rt").exists.saveAs("rt"))
        .check(header("At").exists.saveAs("at"))
        .check(jsonPath("$.uidx").exists.saveAs("uidx"))
      )
    }
  }
}
