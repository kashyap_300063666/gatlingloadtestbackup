package com.myntra.hlt

import com.myntra.commons.util.{PropertyLoader, RedisUtil}

class HLTOrdersUtil extends PropertyLoader {
  private val redisHost: String = System.getProperty("redis.Host", "localhost")
  private val redisPort: Int = System.getProperty("redis.port", "6379").toInt

  private val codOrdersFrom: String = getProperty("COD_ORDERS_FROM", OrderEnums.COD_ORDERS.toString)
  private val codOrdersRetry: String = getProperty("COD_ORDERS_RETRY", OrderEnums.COD_RETRY.toString)

  private val redisUtil = new RedisUtil(redisHost, redisPort)

  def saveOrderToList(orderId: String, uidx: String, listName: String) {
    val orderList: List[String] = List(uidx + ":" + orderId)
    redisUtil.rp(listName, orderList)
  }

  def saveCODOrder(orderId: String, uidx: String) {
    saveOrderToList(orderId, uidx, OrderEnums.COD_ORDERS.toString)
  }

  def saveCancelledCODOrder(orderId: String, uidx: String): Unit = {
    saveOrderToList(orderId, uidx, OrderEnums.COD_CANCELLED.toString)
  }

  def saveWalletOrder(orderId: String, uidx: String) {
    saveOrderToList(orderId, uidx, OrderEnums.WALLET_ORDERS.toString)
  }

  def saveOnlineOrder(orderId: String, uidx: String) {
    saveOrderToList(orderId, uidx, OrderEnums.ONLINE_ORDERS.toString)
  }

  def getCODOrder(): String = {

    redisUtil.lpop(codOrdersFrom).toString
  }

  def saveToCODRetry(orderId: String, uidx: String) {
    saveOrderToList(orderId, uidx, codOrdersRetry)
  }
}
