package com.myntra.hlt

import com.myntra.commons.util.PropertyLoader
import com.myntra.hltScenarioParams

class LoadParams extends PropertyLoader{

  /* Time to rampUp to max load */
  val rampUpDuration: Float = getFloatProperty(hltScenarioParams.rampUpDuration)

  /* Time to hold Max Load */
  val maxLoadHoldDuration: Float = getFloatProperty(hltScenarioParams.maxLoadHoldDuration)

  /* Time to rampUp to max load */
  val peakLoadDuration: Float = getFloatProperty(hltScenarioParams.peakLoadDuration)

  /* Time to hold Max Load */
  val peakLoadAsTimesOfMaxLoad: Float = getFloatProperty(hltScenarioParams.peakLoadAsTimesOfMaxLoad)
  
  /**
    * Class to calculate all User loads and durations of a scenario
    * @param propertyName : Property name related to Scenario
    */
  class ScenarioParams(propertyName: String) {
    private val maxLoad: Int = getIntProperty(propertyName)
    val rampUpMaxLoad: Int = if (rampUpDuration == 0) 0 else maxLoad
    val constUsersMaxLoad: Int = if (maxLoadHoldDuration == 0) 0 else maxLoad
    var scenarioPeakDuration: Float = peakLoadDuration
    val constUsersPeakLoad: Int = if (scenarioPeakDuration == 0) 0 else (peakLoadAsTimesOfMaxLoad * maxLoad).toInt

    /* If peakLoadDuration is non-Zero but load is 0, then make peakLoadDuration as 0 */
    if (scenarioPeakDuration != 0 && constUsersPeakLoad == 0) {
      scenarioPeakDuration = 0
    }
  }
  
  
  /**
    * Class to calculate all ratio loads and durations of a scenario
    * @param propertyName : Property name related to Scenario
    */
  class RatioParams(propertyName: String, defValue : String) {
    private val maxLoad: Int = getIntProperty("users.count", "1")
    val ratioValue: Float =  getFloatProperty(propertyName, defValue)
    val rampUpMaxLoad: Int = if (rampUpDuration == 0) 0 else ((maxLoad * ratioValue)/100).toInt
    val constUsersMaxLoad: Int = if (maxLoadHoldDuration == 0) 0 else  ((maxLoad * ratioValue)/100).toInt
    var scenarioPeakDuration: Float = peakLoadDuration
    val constUsersPeakLoad: Int = if (scenarioPeakDuration == 0) 0 else (peakLoadAsTimesOfMaxLoad * constUsersMaxLoad).toInt

    /* If peakLoadDuration is non-Zero but load is 0, then make peakLoadDuration as 0 */
    if (scenarioPeakDuration != 0 && constUsersPeakLoad == 0) {
      scenarioPeakDuration = 0
    }
    
  }
  
}
